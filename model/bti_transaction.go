package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
)

// 中心钱包帐变(预投注) BTI使用
func walletReserveTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, createAt int64) memberTransaction {

	id := helper.GenId()

	afterAmount := balance.Sub(amount)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       "reserve",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	if amount.Cmp(decimal.Zero) > 0 {
		fAddValue, _ := amount.Float64()
		myredis.AddUserFieldValueByFloat64(mb.Uid, myConfig.G_tbl_member_balance+"brl", -fAddValue)
	}

	return trans
}

// 中心钱包帐变(注单资讯指令) BTI使用
func walletDebitReserveTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) memberTransaction {

	afterAmount := balance
	id := helper.GenId()
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionDebitReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()

	myredis.UpdateSqlFieldToRedis(query)

	return trans
}

// 中心钱包帐变(取消投注) BTI使用
func walletCancelReserveTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, createAt int64) memberTransaction {

	id := helper.GenId()
	afterAmount := balance.Add(amount)
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionCancelReserve,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	// 取消投注，把预投注订单改为取消状态
	ex := g.Ex{
		"bill_no":   billNo,
		"cash_type": []int{helper.TransactionReserve, helper.TransactionDebitReserve},
	}
	record := g.Record{
		"remark": "cancelReserve",
	}
	query, _, _ = dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	if amount.Cmp(decimal.Zero) > 0 {
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
	}

	return trans
}

// 中心钱包帐变(确认投注) BTI使用
func walletCommitReserveTransaction(mb myUserHelp.TblMemberBase, balance, reserveAmount, betAmount decimal.Decimal, platformID, reserveID string, createAt int64) memberTransaction {

	id := helper.GenId()
	var trans memberTransaction
	// 预投注金额未完全使用
	if reserveAmount.GreaterThan(betAmount) {
		// 剩余未使用金额
		leftAmount := reserveAmount.Sub(betAmount)
		afterAmount := balance.Add(leftAmount)
		// 预投注，投注金额可以为0
		trans = memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       reserveID,
			CashType:     helper.TransactionCommitReserve,
			Amount:       leftAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  "",
			CreatedAt:    createAt,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", leftAmount)
	}

	// 确认投注，把预投注订单改为投注成功状态
	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	record := g.Record{
		"remark": "running",
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	return trans
}

// 中心钱包帐变(结算) BTI使用
func walletCreditCustomerTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) memberTransaction {

	id := helper.GenId()
	afterAmount := balance.Add(amount)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionCreditCustomer,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	if amount.Cmp(decimal.Zero) > 0 {
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
	}

	return trans
}

// 中心钱包帐变(重新结算) BTI使用
func walletDebitCustomerTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, createAt int64) memberTransaction {

	afterAmount := balance.Sub(amount)
	id := helper.GenId()
	// 预投注，投注金额可以为0
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionDebitCustomer,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  afterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    createAt,
		Remark:       remark,
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
	return trans
}
