package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"sort"
	"strings"
	"time"
)

type plat_pp_t struct{}

func (plat_pp_t) Reg(args map[string]string) error {
	return nil
}

// 场馆启动
func (plat_pp_t) Launch(args map[string]string) (string, error) {
	dst := "fullwin" + "_" + args["puid"]
	ops := helper.ThinkEncrypt(dst, "pplay", 0)
	pid := args["pid"]
	gameCode := args["gamecode"]
	var requestURI string
	var req map[string]string
	if args["tester"] == "1" || args["tester"] == "3" {
		requestURI = fmt.Sprintf("%s/game/url", meta.PlatCfg.PP.API)
		req = map[string]string{
			"secureLogin": meta.PlatCfg.PP.SecureLogin,
			"symbol":      gameCode,
			"language":    meta.PlatCfg.PP.Lang,
			"token":       ops,
			"currency":    meta.PlatCfg.PP.Currency,
			"technology":  "H5",
			"lobbyUrl":    meta.H5URL,
			"platform":    "fullwin",
			"url":         requestURI,
			"AccountID":   args["puid"],
			"key":         meta.PlatCfg.PP.SecretKey,
		}
	} else if args["tester"] == "2" {
		requestURI = fmt.Sprintf("%s/game/url", meta.PlatCfg.PPTest.API)
		req = map[string]string{
			"secureLogin": meta.PlatCfg.PPTest.SecureLogin,
			"symbol":      gameCode,
			"language":    meta.PlatCfg.PPTest.Lang,
			"token":       ops,
			"currency":    meta.PlatCfg.PPTest.Currency,
			"technology":  "H5",
			"lobbyUrl":    meta.H5URL,
			"platform":    "fullwin",
			"url":         requestURI,
			"AccountID":   args["puid"],
			"key":         meta.PlatCfg.PPTest.SecretKey,
		}
	}

	if args["deviceType"] == "1" {
		req["lobbyUrl"] = meta.WebURL
	}
	//hash := ppHash(req)
	//req["hash"] = hash
	requestBody := ParamEncode(req)
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	statusCode, body, err := HttpPostHeaderWithPushLog([]byte(requestBody), args["username"], ppPlatform[pid], "http://177.71.145.60/pplay/index/createuser", header)
	fmt.Println("statusCode===", statusCode)
	fmt.Println("body===", string(body))
	fmt.Println("err===", err)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := PPLoginRsp{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		fmt.Println("evo:Launch:format", string(body), err)
		return "", errors.New(helper.FormatErr)
	}

	return rsp.GameURL, nil
}

func ppHash(req map[string]string, tester int) string {

	var (
		ks     []string
		sorted string
	)
	for k := range req {
		ks = append(ks, k)
	}
	//fmt.Println("before sort", ks)
	sort.Strings(ks)
	//fmt.Println("after sort", ks)
	for k, v := range ks {
		if k == 0 {
			sorted += fmt.Sprintf("%s=%s", v, req[v])
		} else {
			sorted += fmt.Sprintf("&%s=%s", v, req[v])
		}
	}
	if tester == 1 || tester == 3 {
		sorted += meta.PlatCfg.PP.SecretKey
	} else if tester == 2 {
		sorted += meta.PlatCfg.PPTest.SecretKey
	}

	//fmt.Println(sorted)
	return MD5Hash(sorted)
}

func PPAuthenticate(ctx *fasthttp.RequestCtx) PPAuthRsp {

	//fmt.Println(ctx.PostArgs().String())
	hash := string(ctx.PostArgs().Peek("hash"))
	token := string(ctx.PostArgs().Peek("token"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))       //可选
	ipAddress := string(ctx.PostArgs().Peek("ipAddress")) //可选

	rsp := PPAuthRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
		Country:     meta.PlatCfg.PP.Country,
	}
	req := map[string]string{
		"providerId": providerId,
		"token":      token,
	}
	if gameId != "" {
		req["gameId"] = gameId
	}
	if ipAddress != "" {
		req["ipAddress"] = ipAddress
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if token == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	uid := myUserHelp.GetUidFromToken(token)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}

	rsp.UserId = meta.Prefix + mb.Uid
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	rsp.Bonus = 0

	return rsp
}

func PPBalance(ctx *fasthttp.RequestCtx) PPBalanceRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	token := string(ctx.PostArgs().Peek("token")) //可选

	rsp := PPBalanceRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
	}
	req := map[string]string{
		"providerId": providerId,
		"userId":     userId,
	}
	if token != "" {
		req["token"] = token
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	rsp.Bonus = 0

	return rsp
}

func PPBet(ctx *fasthttp.RequestCtx) PPBetRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))
	roundId := string(ctx.PostArgs().Peek("roundId"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	reference := string(ctx.PostArgs().Peek("reference"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	roundDetails := string(ctx.PostArgs().Peek("roundDetails"))
	bonusCode := string(ctx.PostArgs().Peek("bonusCode"))                     //可选
	platform := string(ctx.PostArgs().Peek("platform"))                       //可选
	language := string(ctx.PostArgs().Peek("language"))                       //可选
	jackpotContribution := string(ctx.PostArgs().Peek("jackpotContribution")) //可选
	jackpotId := string(ctx.PostArgs().Peek("jackpotId"))                     //可选
	jackpotDetails := string(ctx.PostArgs().Peek("jackpotDetails"))           //可选
	token := string(ctx.PostArgs().Peek("token"))                             //可选
	ipAddress := string(ctx.PostArgs().Peek("ipAddress"))                     //可选

	rsp := PPBetRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
	}
	req := map[string]string{
		"userId":       userId,
		"gameId":       gameId,
		"roundId":      roundId,
		"amount":       sAmount,
		"reference":    reference,
		"providerId":   providerId,
		"timestamp":    timestamp,
		"roundDetails": roundDetails,
	}
	if bonusCode != "" {
		req["bonusCode"] = bonusCode
	}
	if platform != "" {
		req["platform"] = platform
	}
	if language != "" {
		req["language"] = language
	}
	if jackpotContribution != "" {
		req["jackpotContribution"] = jackpotContribution
	}
	if jackpotId != "" {
		req["jackpotId"] = jackpotId
	}
	if jackpotDetails != "" {
		req["jackpotDetails"] = jackpotDetails
	}
	if token != "" {
		req["token"] = token
	}
	if ipAddress != "" {
		req["ipAddress"] = ipAddress
	}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	if amount.GreaterThan(balance) {
		rsp.Error = 1
		rsp.Description = "Lack Of Balance"
		return rsp
	}

	ex := g.Ex{
		"bill_no":      roundId,
		"operation_no": reference,
		"cash_type":    helper.TransactionBet,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.TransactionId = roundId
		rsp.Cash, _ = balance.Float64()
		rsp.Bonus = 0
		rsp.UsedPromo = 0
		return rsp
	}

	var (
		pid      = helper.PPCasino
		apiName  = "PP Casino"
		gameType = gameTypeCasino
		rowID    = fmt.Sprintf("ppcasino%s%s", roundId, reference)
	)
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
		apiName = "PP Slot"
		gameType = gameTypeSlot
		rowID = fmt.Sprintf("ppslot%s%s", roundId, reference)
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
		apiName = "VIVER Sport"
		gameType = gameTypeSport
		rowID = fmt.Sprintf("viversport%s%s", roundId, reference)
	}

	//投注
	walletBetTransaction(mb, roundId, reference, pid, balance, amount.Abs(), ctx.Time())

	gm := GameRecord{
		BillNo:         roundId,
		Result:         "roundDetails:" + roundDetails,
		GameName:       ppGameName[gameId],
		GameCode:       gameId,
		PlayType:       gameId,
		RowId:          rowID,
		ApiBillNo:      roundId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      time.Now().UnixMilli(),
		UpdatedAt:      time.Now().UnixMilli(),
		BetTime:        time.Now().UnixMilli(),
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        apiName,
		ValidBetAmount: 0,
		NetAmount:      0,
	}
	gm.BetAmount, _ = amount.Abs().Float64()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	rsp.Bonus = 0
	rsp.UsedPromo = 0

	return rsp
}

func PPResult(ctx *fasthttp.RequestCtx) PPResultRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))
	roundId := string(ctx.PostArgs().Peek("roundId"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	reference := string(ctx.PostArgs().Peek("reference"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	roundDetails := string(ctx.PostArgs().Peek("roundDetails"))
	bonusCode := string(ctx.PostArgs().Peek("bonusCode"))                 //可选
	platform := string(ctx.PostArgs().Peek("platform"))                   //可选
	token := string(ctx.PostArgs().Peek("token"))                         //可选
	sPromoWinAmount := string(ctx.PostArgs().Peek("promoWinAmount"))      //可选
	promoWinReference := string(ctx.PostArgs().Peek("promoWinReference")) //可选
	promoCampaignID := string(ctx.PostArgs().Peek("promoCampaignID"))     //可选
	promoCampaignType := string(ctx.PostArgs().Peek("promoCampaignType")) //可选

	rsp := PPResultRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
	}
	req := map[string]string{
		"userId":       userId,
		"gameId":       gameId,
		"roundId":      roundId,
		"amount":       sAmount,
		"reference":    reference,
		"providerId":   providerId,
		"timestamp":    timestamp,
		"roundDetails": roundDetails,
	}
	if bonusCode != "" {
		req["bonusCode"] = bonusCode
	}
	if platform != "" {
		req["platform"] = platform
	}
	if token != "" {
		req["token"] = token
	}
	if sPromoWinAmount != "" {
		req["promoWinAmount"] = sPromoWinAmount
	}
	if promoWinReference != "" {
		req["promoWinReference"] = promoWinReference
	}
	if promoCampaignID != "" {
		req["promoCampaignID"] = promoCampaignID
	}
	if promoCampaignType != "" {
		req["promoCampaignType"] = promoCampaignType
	}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	settleAmount := amount
	if sPromoWinAmount != "" {
		promoWinAmount, err := decimal.NewFromString(sPromoWinAmount)
		if err != nil {
			rsp.Error = 100
			rsp.Description = "System Error"
			return rsp
		}

		if promoWinAmount.GreaterThan(decimal.Zero) {
			settleAmount = settleAmount.Add(promoWinAmount)
		}
	}

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionBet,
	}

	bTrx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.Error = 0
		rsp.Description = "NOT FOUND TRANSACTION"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	exPayout := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionPayout,
	}

	trx, bTransExsit := walletTransactionFindOne(exPayout)

	if bTransExsit {
		rsp.TransactionId = trx.ID
		rsp.Cash, _ = balance.Float64()
		rsp.Bonus = 0
		return rsp
	}

	pid := helper.PPCasino
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
	}
	// 派彩

	trans := walletSettleTransaction(mb, roundId, "", pid, balance, settleAmount.Abs(), ctx.Time())
	rsp.TransactionId = trans.ID
	SetTransactionToRedis(ex, trans)
	SetTransactionToRedis(exPayout, trans)
	ex = g.Ex{
		"bill_no": roundId,
		"uid":     strings.TrimPrefix(userId, meta.Prefix),
	}
	betAmount, _ := decimal.NewFromString(bTrx.Amount)
	netAmount := amount.Sub(betAmount)
	gm := g.Record{
		"flag":             "1",
		"valid_bet_amount": betAmount.String(),
		"net_amount":       netAmount.String(),
		"updated_at":       time.Now().UnixMilli(),
		"settle_time":      time.Now().UnixMilli(),
		"bet_time":         time.Now().UnixMilli(),
	}
	if betAmount.GreaterThan(netAmount.Abs()) {
		gm["valid_bet_amount"] = netAmount.Abs().String()
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(ex).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	rsp.Bonus = 0

	return rsp
}

func PPBonusWin(ctx *fasthttp.RequestCtx) PPBonusWinRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	reference := string(ctx.PostArgs().Peek("reference"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	bonusCode := string(ctx.PostArgs().Peek("bonusCode"))       //可选
	roundId := string(ctx.PostArgs().Peek("roundId"))           //可选
	gameId := string(ctx.PostArgs().Peek("gameId"))             //可选
	token := string(ctx.PostArgs().Peek("token"))               //可选
	requestId := string(ctx.PostArgs().Peek("requestId"))       //可选
	remainAmount := string(ctx.PostArgs().Peek("remainAmount")) //可选

	rsp := PPBonusWinRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
		Cash:        0,
		Bonus:       0,
	}
	req := map[string]string{
		"userId":     userId,
		"amount":     sAmount,
		"reference":  reference,
		"providerId": providerId,
		"timestamp":  timestamp,
	}
	if bonusCode != "" {
		req["bonusCode"] = bonusCode
	}
	if roundId != "" {
		req["roundId"] = roundId
	}
	if gameId != "" {
		req["gameId"] = gameId
	}
	if token != "" {
		req["token"] = token
	}
	if requestId != "" {
		req["requestId"] = requestId
	}
	if remainAmount != "" {
		req["remainAmount"] = remainAmount
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	return rsp
}

func PPJackpotWin(ctx *fasthttp.RequestCtx) PPJackpotWinRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	userId := string(ctx.PostArgs().Peek("userId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))
	roundId := string(ctx.PostArgs().Peek("roundId"))
	jackpotId := string(ctx.PostArgs().Peek("jackpotId"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	reference := string(ctx.PostArgs().Peek("reference"))
	jackpotDetails := string(ctx.PostArgs().Peek("jackpotDetails")) //可选
	platform := string(ctx.PostArgs().Peek("platform"))             //可选
	token := string(ctx.PostArgs().Peek("token"))                   //可选

	rsp := PPJackpotWinRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
		Cash:        0,
		Bonus:       0,
	}
	req := map[string]string{
		"providerId": providerId,
		"userId":     userId,
		"amount":     sAmount,
		"gameId":     gameId,
		"roundId":    roundId,
		"timestamp":  timestamp,
		"jackpotId":  jackpotId,
		"reference":  reference,
	}
	if jackpotDetails != "" {
		req["jackpotDetails"] = jackpotDetails
	}
	if platform != "" {
		req["platform"] = platform
	}
	if token != "" {
		req["token"] = token
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionJackPotWin,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)

	if !bTransExsit {
		rsp.TransactionId = trx.ID
		return rsp
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	var (
		pid      = helper.PPCasino
		apiName  = "PP Casino"
		gameType = gameTypeCasino
		rowID    = fmt.Sprintf("ppcasino%s%s", roundId, reference)
	)
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
		apiName = "PP Slot"
		gameType = gameTypeSlot
		rowID = fmt.Sprintf("ppslot%s%s", roundId, reference)
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
		apiName = "VIVER Sport"
		gameType = gameTypeSport
		rowID = fmt.Sprintf("viversport%s%s", roundId, reference)
	}
	//免费旋转奖金
	rsp.TransactionId, err = walletJackpotWinTransaction(tx, mb, roundId, jackpotId, pid, balance, amount.Abs(), ctx.Time())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.TransactionId = trx.ID
			return rsp
		}

		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	gm := GameRecord{
		BillNo:         roundId,
		Result:         "jackpotId:" + jackpotId,
		GameName:       ppGameName[gameId],
		GameCode:       gameId,
		PlayType:       gameId,
		RowId:          rowID,
		ApiBillNo:      roundId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      time.Now().UnixMilli(),
		UpdatedAt:      time.Now().UnixMilli(),
		BetTime:        time.Now().UnixMilli(),
		SettleTime:     time.Now().UnixMilli(),
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        apiName,
		ValidBetAmount: 0,
		NetAmount:      0,
	}
	gm.NetAmount, _ = amount.Abs().Float64()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)
	err = tx.Commit()
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	return rsp
}

func PPEndRound(ctx *fasthttp.RequestCtx) PPEndRoundRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))
	roundId := string(ctx.PostArgs().Peek("roundId"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	bonusCode := string(ctx.PostArgs().Peek("bonusCode"))       //可选
	platform := string(ctx.PostArgs().Peek("platform"))         //可选
	token := string(ctx.PostArgs().Peek("token"))               //可选
	roundDetails := string(ctx.PostArgs().Peek("roundDetails")) //可选
	win := string(ctx.PostArgs().Peek("win"))                   //可选

	rsp := PPEndRoundRsp{
		Error:       0,
		Description: "Success",
	}
	req := map[string]string{
		"providerId": providerId,
		"gameId":     gameId,
		"roundId":    roundId,
		"userId":     userId,
	}
	if bonusCode != "" {
		req["bonusCode"] = bonusCode
	}
	if platform != "" {
		req["platform"] = platform
	}
	if token != "" {
		req["token"] = token
	}
	if roundDetails != "" {
		req["roundDetails"] = roundDetails
	}
	if win != "" {
		req["win"] = win
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	rsp.Bonus = 0

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionBet,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.Error = 0
		rsp.Description = "NOT FOUND TRANSACTION"
		return rsp
	}

	if trx.Remark == "settled" {
		return rsp
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	walletSettleTransaction(mb, roundId, "", trx.PlatformID, balance, decimal.Zero, ctx.Time())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Cash, _ = balance.Float64()
			rsp.Bonus = 0
			return rsp
		}

		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	ex = g.Ex{
		"bill_no": roundId,
		"uid":     strings.TrimPrefix(userId, meta.Prefix),
	}
	betAmount, _ := decimal.NewFromString(trx.Amount)
	gm := g.Record{
		"flag":             "1",
		"valid_bet_amount": betAmount.String(),
		"net_amount":       decimal.Zero.Sub(betAmount).String(),
		"updated_at":       time.Now().UnixMilli(),
		"settle_time":      time.Now().UnixMilli(),
		"bet_time":         time.Now().UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(ex).ToSQL()
	myredis.AddLogToRedis(query)

	err = tx.Commit()
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	return rsp
}

func PPRefund(ctx *fasthttp.RequestCtx) PPRefundRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	reference := string(ctx.PostArgs().Peek("reference"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	platform := string(ctx.PostArgs().Peek("platform"))         //可选
	sAmount := string(ctx.PostArgs().Peek("amount"))            //可选
	gameId := string(ctx.PostArgs().Peek("gameId"))             //可选
	roundId := string(ctx.PostArgs().Peek("roundId"))           //可选
	timestamp := string(ctx.PostArgs().Peek("timestamp"))       //可选
	roundDetails := string(ctx.PostArgs().Peek("roundDetails")) //可选
	bonusCode := string(ctx.PostArgs().Peek("bonusCode"))       //可选
	token := string(ctx.PostArgs().Peek("token"))               //可选

	rsp := PPRefundRsp{
		Error:       0,
		Description: "Success",
	}
	req := map[string]string{
		"userId":     userId,
		"reference":  reference,
		"providerId": providerId,
	}
	if platform != "" {
		req["platform"] = platform
	}
	if sAmount != "" {
		req["amount"] = sAmount
	}
	if gameId != "" {
		req["gameId"] = gameId
	}
	if roundId != "" {
		req["roundId"] = roundId
	}
	if timestamp != "" {
		req["timestamp"] = timestamp
	}
	if roundDetails != "" {
		req["roundDetails"] = roundDetails
	}
	if bonusCode != "" {
		req["bonusCode"] = bonusCode
	}
	if token != "" {
		req["token"] = token
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionBet,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.Error = 0
		rsp.Description = "NOT FOUND TRANSACTION"
		return rsp
	}

	ex = g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionBetCancel,
	}

	trx, bTransBetCancelExsit := walletTransactionFindOne(ex)
	if bTransBetCancelExsit {
		rsp.TransactionId = trx.ID
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	pid := helper.PPCasino
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
	}
	//投注
	trans := walletCancelTransaction(mb, roundId, "", pid, balance, amount.Abs(), ctx.Time())
	rsp.TransactionId = trans.ID
	ex = g.Ex{
		"bill_no": roundId,
		"uid":     strings.TrimPrefix(userId, meta.Prefix),
	}
	gm := g.Record{
		"flag":             "2",
		"valid_bet_amount": "0",
		"net_amount":       "0",
		"updated_at":       time.Now().UnixMilli(),
		"bet_time":         time.Now().UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(ex).ToSQL()
	myredis.AddLogToRedis(query)
	return rsp
}

func PPPromoWin(ctx *fasthttp.RequestCtx) PPPromoWinRsp {

	hash := string(ctx.PostArgs().Peek("hash"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	userId := string(ctx.PostArgs().Peek("userId"))
	campaignId := string(ctx.PostArgs().Peek("campaignId"))
	campaignType := string(ctx.PostArgs().Peek("campaignType"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	currency := string(ctx.PostArgs().Peek("currency"))
	reference := string(ctx.PostArgs().Peek("reference"))
	roundId := string(ctx.PostArgs().Peek("roundId"))   //可选
	gameId := string(ctx.PostArgs().Peek("gameId"))     //可选
	dataType := string(ctx.PostArgs().Peek("dataType")) //可选

	rsp := PPPromoWinRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
		Cash:        0,
		Bonus:       0,
	}
	req := map[string]string{
		"providerId":   providerId,
		"userId":       userId,
		"amount":       sAmount,
		"timestamp":    timestamp,
		"campaignId":   campaignId,
		"campaignType": campaignType,
		"currency":     currency,
		"reference":    reference,
	}
	if roundId != "" {
		req["roundId"] = roundId
	}
	if gameId != "" {
		req["gameId"] = gameId
	}
	if dataType != "" {
		req["dataType"] = dataType
	}

	//if providerId != meta.PlatCfg.PP.ProviderID {
	//	rsp.Error = 7
	//	rsp.Description = "Invalid Parameter"
	//	return rsp
	//}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": helper.TransactionPromoWin,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.TransactionId = roundId
		return rsp
	}

	var (
		pid      = helper.PPCasino
		apiName  = "PP Casino"
		gameType = gameTypeCasino
		rowID    = fmt.Sprintf("ppcasino%s%s", roundId, reference)
	)
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
		apiName = "PP Slot"
		gameType = gameTypeSlot
		rowID = fmt.Sprintf("ppslot%s%s", roundId, reference)
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
		apiName = "VIVER Sport"
		gameType = gameTypeSport
		rowID = fmt.Sprintf("viversport%s%s", roundId, reference)
	}
	//免费旋转奖金
	rsp.TransactionId = walletPromoWinTransaction(mb, roundId, reference, pid, balance, amount.Abs(), ctx.Time())
	gm := GameRecord{
		BillNo:         roundId,
		Result:         "campaignId:" + campaignId,
		GameName:       ppGameName[gameId],
		GameCode:       gameId,
		PlayType:       gameId,
		RowId:          rowID,
		ApiBillNo:      roundId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      time.Now().UnixMilli(),
		UpdatedAt:      time.Now().UnixMilli(),
		BetTime:        time.Now().UnixMilli(),
		SettleTime:     time.Now().UnixMilli(),
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        apiName,
		BetAmount:      0,
		ValidBetAmount: 0,
		NetAmount:      0,
	}
	gm.NetAmount, _ = amount.Abs().Float64()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	return rsp
}

func PPAdjustment(ctx *fasthttp.RequestCtx) interface{} {

	hash := string(ctx.PostArgs().Peek("hash"))
	userId := string(ctx.PostArgs().Peek("userId"))
	gameId := string(ctx.PostArgs().Peek("gameId"))
	roundId := string(ctx.PostArgs().Peek("roundId"))
	sAmount := string(ctx.PostArgs().Peek("amount"))
	reference := string(ctx.PostArgs().Peek("reference"))
	providerId := string(ctx.PostArgs().Peek("providerId"))
	timestamp := string(ctx.PostArgs().Peek("timestamp"))
	token := string(ctx.PostArgs().Peek("token"))                   //可选
	validBetAmount := string(ctx.PostArgs().Peek("validBetAmount")) //可选

	rsp := PPAdjustmentRsp{
		Error:       0,
		Description: "Success",
		Currency:    meta.PlatCfg.PP.Currency,
		Cash:        0,
		Bonus:       0,
	}
	req := map[string]string{
		"providerId": providerId,
		"userId":     userId,
		"amount":     sAmount,
		"gameId":     gameId,
		"roundId":    roundId,
		"timestamp":  timestamp,
		"reference":  reference,
	}
	if token != "" {
		req["token"] = token
	}
	if validBetAmount != "" {
		req["validBetAmount"] = validBetAmount
	}

	if userId == "" {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.Error = 2
		rsp.Description = "Invalid User"
		return rsp
	}

	if ppHash(req, mb.Tester) != hash {
		rsp.Error = 5
		rsp.Description = "Invalid Hash"
		return rsp
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	cashType := helper.TransactionAdjustPlus
	if decimal.Zero.GreaterThan(amount) {
		cashType = helper.TransactionAdjustDiv
	}

	ex := g.Ex{
		"bill_no":   roundId,
		"cash_type": cashType,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.TransactionId = roundId
		return rsp
	}

	if decimal.Zero.GreaterThan(balance.Add(amount)) {
		return PPInsufficientBalanceRsp{
			Error:       1,
			Description: "Insufficient balance",
		}
	}

	var (
		pid      = helper.PPCasino
		apiName  = "PP Casino"
		gameType = gameTypeCasino
		rowID    = fmt.Sprintf("ppcasino%s%s", roundId, reference)
	)
	if strings.HasPrefix(gameId, "vs") || strings.HasPrefix(gameId, "sc") ||
		strings.HasPrefix(gameId, "cs") || strings.HasPrefix(gameId, "bjm") ||
		strings.HasPrefix(gameId, "bn") || gameId == "rla" || gameId == "bca" {
		pid = helper.PP
		apiName = "PP Slot"
		gameType = gameTypeSlot
		rowID = fmt.Sprintf("ppslot%s%s", roundId, reference)
	} else if strings.HasPrefix(gameId, "vp") {
		pid = helper.VIVER
		apiName = "VIVER Sport"
		gameType = gameTypeSport
		rowID = fmt.Sprintf("viversport%s%s", roundId, reference)
	}
	//免费旋转奖金
	trans := walletAdjustTransaction(mb, roundId, reference, pid, balance, amount.Abs(), cashType, ctx.Time())
	SetTransactionToRedis(ex, trans)
	rsp.TransactionId = trans.ID
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.TransactionId = roundId
			return rsp
		}

		rsp.Error = 100
		rsp.Description = "System Error"
		return rsp
	}

	gm := GameRecord{
		BillNo:         roundId,
		Result:         "",
		GameName:       ppGameName[gameId],
		GameCode:       gameId,
		PlayType:       gameId,
		RowId:          rowID,
		ApiBillNo:      roundId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      time.Now().UnixMilli(),
		UpdatedAt:      time.Now().UnixMilli(),
		BetTime:        time.Now().UnixMilli(),
		SettleTime:     time.Now().UnixMilli(),
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        apiName,
		BetAmount:      0,
		ValidBetAmount: 0,
		NetAmount:      0,
	}
	gm.NetAmount, _ = amount.Abs().Float64()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Cash, _ = balance.Float64()
	return rsp
}
