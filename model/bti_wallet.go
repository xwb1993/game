package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"fmt"
	"game/contrib/helper"
	"game/contrib/validator"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"time"
)

type BTIBalanceRsp struct {
	Status  string `json:"status"`
	Token   string `json:"token"`
	Message string `json:"message"`
	Balance string `json:"balance"`
}

type BTIValidateTokenRsp struct {
	ErrorCode    int
	ErrorMessage string
	CustId       string
	CustLogin    string
	Balance      string
	City         string
	Country      string
	CurrencyCode string
}

type BTIRsp struct {
	ErrorCode    int
	ErrorMessage string
	Balance      string
	TrxID        string
}

type plat_bti_t struct{}

func (plat_bti_t) Reg(args map[string]string) error {
	return nil
}

func (plat_bti_t) Launch(args map[string]string) (string, error) {
	return fmt.Sprintf("%s?operatorToken=%s", meta.PlatCfg.BTI.API, args["puid"]), nil
}

func BTIBalance(ctx *fasthttp.RequestCtx) BTIBalanceRsp {

	token := string(ctx.QueryArgs().Peek("token"))
	rsp := BTIBalanceRsp{
		Status:  "failure",
		Balance: "0",
		Message: "",
		Token:   token,
	}
	if token == "" {
		rsp.Message = "fail"
		return rsp
	}

	uid := myUserHelp.GetUidFromToken(token)
	if uid == "" {
		rsp.Message = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		helper.Print(ctx, false, helper.UserNotExist)
		rsp.Message = "UserNotExist"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Status = "success"
	rsp.Balance = balance.String()
	return rsp
}

// token s9Y9Y7Adfx6ePLqdnhx+Ow==
func BTIValidateToken(ctx *fasthttp.RequestCtx) BTIValidateTokenRsp {

	fmt.Println(ctx.QueryArgs().String())
	authToken := string(ctx.QueryArgs().Peek("auth_token"))
	rsp := BTIValidateTokenRsp{
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
		CurrencyCode: "BRL",
	}
	if !validator.CtypeDigit(authToken) {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	uid := myUserHelp.GetUidFromToken(authToken)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.CustId = meta.Prefix + mb.Pid
	rsp.CustLogin = meta.Prefix + mb.Pid
	rsp.Balance = balance.String()
	rsp.City = "Brasília"
	rsp.Country = "BR" //ISO 3166-1
	return rsp
}

func BTIReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err := BetLock(mb.Username)
	if err != nil {
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	req := BTIReserveReq{}
	err = helper.XMLUnmarshal(body, &req)
	if err != nil {
		_ = pushError(err, helper.FormatErr)
		rsp.ErrorCode = -1
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.TrxID = reserveID
		rsp.Balance = balance.String()
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	if amount.Cmp(balance) > 0 {
		rsp.ErrorCode = -4
		rsp.ErrorMessage = "Insufficient Amount"
		return rsp
	}

	trans := walletReserveTransaction(mb, reserveID, "", helper.BTI, balance, amount, ctx.Time().UnixMilli())
	SetTransactionToRedis(ex, trans)
	rsp.TrxID = trans.ID
	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()

	return rsp
}

func BTIDebitReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	reserve, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.ErrorMessage = "Reserve ID Error"
		return rsp
	}

	balance, _ := decimal.NewFromString(reserve.AfterAmount)
	rsp.Balance = balance.String()
	req := BTIDebitReserveReq{}
	err := helper.XMLUnmarshal(body, &req)
	if err != nil {
		_ = pushError(err, helper.FormatErr)
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	ex = g.Ex{
		"bill_no":      reserveID,
		"operation_no": purchaseID,
		"cash_type":    helper.TransactionDebitReserve,
	}
	trans, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit { //重复请求
		rsp.TrxID = trans.ID
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	trans = walletDebitReserveTransaction(mb, purchaseID, reqID, helper.BTI, reserveID, balance, amount, ctx.Time().UnixMilli())
	SetTransactionToRedis(ex, trans)

	rowID := "bti" + purchaseID
	ts := time.Now()
	result := fmt.Sprintf("BetTypeName : %s,BranchName : %s,LeagueName : %s,HomeTeam : %s,AwayTeam : %s,YourBet : %s,OddsDec : %s",
		req.Bet.BetTypeName, req.Bet.BranchName, req.Bet.LeagueName, req.Bet.HomeTeam, req.Bet.AwayTeam, req.Bet.YourBet, req.Bet.OddsDec)
	odds, _ := decimal.NewFromString(req.Bet.OddsDec)
	gm := GameRecord{
		BillNo:         purchaseID,
		Result:         result,
		GameName:       req.Bet.BranchName,
		GameCode:       req.Bet.LeagueID,
		PlayType:       req.Bet.LeagueID,
		RowId:          rowID,
		ApiBillNo:      purchaseID,
		MainBillNo:     purchaseID,
		HandicapType:   req.Bet.BetTypeName,
		Handicap:       req.Bet.BranchName,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		Uid:            mb.Uid,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      ts.UnixMilli(),
		UpdatedAt:      ts.UnixMilli(),
		PlayerName:     mb.Username,
		GameType:       gameTypeSport,
		ApiType:        helper.BTI,
		ApiName:        "Esporte BTI",
	}
	gm.Odds, _ = odds.Float64()
	gm.BetAmount, _ = amount.Float64()
	gm.Flag = 3
	gm.BetTime = ts.UnixMilli()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)

	rsp.TrxID = trans.ID
	err = tx.Commit()
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
	}

	return rsp
}

func BTICancelReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}
	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err := BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	trans, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.ErrorMessage = "Reserve ID Error"
		return rsp
	}

	ex = g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionCancelReserve,
	}
	_, bTransExsit = walletTransactionFindOne(ex)

	if bTransExsit { //重复请求
		return rsp
	}

	amount, _ := decimal.NewFromString(trans.Amount)
	trans = walletCancelReserveTransaction(mb, reserveID, "", helper.BTI, balance, amount, ctx.Time().UnixMilli())
	SetTransactionToRedis(ex, trans)
	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTICommitReserve(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reserveID := string(ctx.QueryArgs().Peek("reserve_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	ex := g.Ex{
		"bill_no":   reserveID,
		"cash_type": helper.TransactionReserve,
	}
	trx, bTransReserveExsit := walletTransactionFindOne(ex)
	if !bTransReserveExsit {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	var amount sql.NullFloat64
	exDebitReserve := g.Ex{
		"remark":    reserveID,
		"cash_type": helper.TransactionDebitReserve,
	}

	trx, bTransDebitReserveExsit := walletTransactionFindOne(exDebitReserve)

	//此处有问题 获取的是总和 后期需要修改
	/*
		query, _, _ := dialect.From("tbl_balance_transaction").Select(g.SUM("amount")).Where(ex).Limit(1).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&amount, query)
	*/

	if !bTransDebitReserveExsit {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	// 预扣除金额
	reserveAmount, _ := decimal.NewFromString(trx.Amount)
	// 实际投注金额
	betAmount := decimal.NewFromFloat(amount.Float64)
	err := BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	ts := ctx.Time()
	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()

	trans := walletCommitReserveTransaction(mb, balance, reserveAmount, betAmount, helper.BTI, reserveID, ts.UnixMilli())
	rsp.TrxID = trans.ID
	SetTransactionToRedis(ex, trans)
	SetTransactionToRedis(exDebitReserve, trans)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	rowID := "bti" + purchaseID
	gm := g.Record{
		"flag":       "0",
		"updated_at": ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTICreditCustomer(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err := BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	ex := g.Ex{
		"bill_no":   purchaseID,
		"cash_type": helper.TransactionDebitReserve,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	betAmount, _ := decimal.NewFromString(trx.Amount)
	amount, _ := decimal.NewFromString(sAmount)
	ts := ctx.Time()
	trans := walletCreditCustomerTransaction(mb, purchaseID, reqID, helper.BTI, trx.Remark, balance, amount.Abs(), ts.UnixMilli())
	SetTransactionToRedis(ex, trans)
	rowID := "bti" + purchaseID
	netAmount := amount.Sub(betAmount)
	validBetAmount := betAmount
	if betAmount.GreaterThan(netAmount.Abs()) {
		validBetAmount = netAmount.Abs()
	}
	gm := g.Record{
		"flag":             "1",
		"valid_bet_amount": validBetAmount.String(),
		"settle_amount":    amount.String(),
		"net_amount":       netAmount.String(),
		"settle_time":      ts.UnixMilli(),
		"updated_at":       ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}

func BTIDebitCustomer(ctx *fasthttp.RequestCtx) BTIRsp {

	fmt.Println(ctx.QueryArgs().String())
	body := ctx.PostBody()
	fmt.Println(string(body))
	//agentID := string(ctx.QueryArgs().Peek("agent_id"))
	//customerID := string(ctx.QueryArgs().Peek("customer_id"))
	custID := string(ctx.QueryArgs().Peek("cust_id"))
	reqID := string(ctx.QueryArgs().Peek("req_id"))
	purchaseID := string(ctx.QueryArgs().Peek("purchase_id"))
	sAmount := string(ctx.QueryArgs().Peek("amount"))

	rsp := BTIRsp{
		TrxID:        helper.GenId(),
		Balance:      "0",
		ErrorCode:    0,
		ErrorMessage: "No error",
	}

	uid := myUserHelp.GetUidFromToken(custID)
	if uid == "" {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "AccessTokenExpires"
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.ErrorCode = -2
		rsp.ErrorMessage = "Member Not exist"
		return rsp
	}

	err := BetLock(mb.Username)
	if err != nil {
		rsp.ErrorMessage = "Generic Error"
		return rsp
	}

	defer BetUnlock(mb.Username)

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()

	ex := g.Ex{
		"bill_no":      purchaseID,
		"operation_no": reqID,
		"cash_type":    helper.TransactionDebitCustomer,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.TrxID = purchaseID
		rsp.Balance = balance.String()
		return rsp
	}

	amount, _ := decimal.NewFromString(sAmount)
	ts := ctx.Time()

	trans := walletDebitCustomerTransaction(mb, purchaseID, reqID, helper.BTI, "", balance, amount.Abs(), ts.UnixMilli())
	rsp.TrxID = trans.ID
	SetTransactionToRedis(ex, trans)
	rowID := "bti" + purchaseID
	gm := g.Record{
		"net_amount":  g.L(fmt.Sprintf("net_amount-%s", amount.Abs().String())),
		"settle_time": ts.UnixMilli(),
		"updated_at":  ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)
	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance = balance.String()
	return rsp
}
