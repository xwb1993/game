package model

const (
	defaultRedisKeyPrefix = "rlock:"
)

var (
	Devices = map[int]string{
		DeviceTypeWeb:            "1",
		DeviceTypeH5:             "2",
		DeviceTypeAndroidFlutter: "3",
		DeviceTypeIOSFlutter:     "4",
	}
)

// 设备端类型
const (
	DeviceTypeWeb            = 24 //web
	DeviceTypeH5             = 25 //h5
	DeviceTypeAndroidFlutter = 35 //android_flutter
	DeviceTypeIOSFlutter     = 36 //ios_flutter
)

const (
	OperateAdd = "+"
	OperateSub = "-"
)

const (
	gameTypeCasino = "1"
	gameTypeFish   = "2"
	gameTypeSlot   = "3"
	gameTypeSport  = "4"
	gameTypePoker  = "5"
	gameTypeESport = "6"
)
