package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"encoding/json"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
	"time"
)

// 投注
func walletBetTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount decimal.Decimal, ts time.Time) memberTransaction {

	id := helper.GenId()
	betAfterAmount := balance.Sub(betAmount)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBet,
		Amount:       betAmount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "running",
		Tester:       mb.Tester,
	}

	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	if betAmount.GreaterThan(decimal.Zero) {
		myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", betAmount)
	}

	return trans
}

// 投注结算
func walletBetAndSettleTransaction(mb myUserHelp.TblMemberBase, trans TransJson, operationNo, platformID string, balance, amount decimal.Decimal, ts time.Time) (winMoney, validAmount float64, err error) {
	record := g.Record{
		"remark": "settled",
	}
	bx := g.Ex{
		"id": trans.ID,
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(bx).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	var value float64
	var discount, settleAmount, validBetAmount decimal.Decimal

	if platformID == helper.PG {
		if amount.Sub(trans.Amount).Cmp(decimal.Zero) > 0 {
			discount = myConfig.GetGameCfgDecimal(myConfig.CfgPGFEE)
			fmt.Println("discount===", discount)
			//扣税金额
			settleAmount = amount.Sub(trans.Amount).Mul(discount)
		}
	}

	//扣完税后赢的钱
	profitAmount := amount.Sub(settleAmount)
	//扣完税和本金后赢的钱
	winAmount := profitAmount.Sub(trans.Amount)
	fmt.Println("settleAmount===", settleAmount)
	fmt.Println("profitAmount===", profitAmount)
	fmt.Println("winAmount===", winAmount)

	if profitAmount.Cmp(decimal.Zero) > 0 {
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", profitAmount)
		settleAfterAmount := balance.Add(profitAmount)
		trans := memberTransaction{
			ID:           helper.GenId(),
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       trans.TransactionRefId,
			CashType:     helper.TransactionPayout,
			Amount:       profitAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  settleAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.Add(1 * time.Millisecond).UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	if trans.Amount.Cmp(decimal.Zero) > 0 {
		if winAmount.Cmp(decimal.Zero) > 0 {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.WinFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount)
			} else {
				discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalWinFlowMultiple)
				validBetAmount = winAmount.Mul(discount)
			}
		} else {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.LoseFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount).Abs()
			} else {
				discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalLoseFlowMultiple)
				validBetAmount = winAmount.Mul(discount).Abs()
			}
		}
	}

	winMoney, _ = profitAmount.Float64()
	validAmount, _ = validBetAmount.Float64()
	if validAmount > 0 {
		err = FlowBonusTransaction(mb, validAmount, platformID)
		if err != nil {
			return winMoney, validAmount, err
		}
	}

	return winMoney, validAmount, nil
}

/*
billNo 主单号
operationNo 子单号（一个）
*/
// 结算，一次下注，一次结算
func walletSettleTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, settleAmount decimal.Decimal, ts time.Time) memberTransaction {

	id := helper.GenId()
	record := g.Record{
		"remark": "settled",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}

	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	var trans memberTransaction
	if settleAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(settleAmount)
		trans = memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       settleAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", settleAmount)
	}

	return trans
}

/*
billNo 主单号
operationNos 子单号（多个）
*/
// 结算，多次下注，一次结算
func walletSettleTransactions(mb myUserHelp.TblMemberBase, billNo, platformID string, operationNos []string, balance, settleAmount decimal.Decimal, ts time.Time) string {

	id := helper.GenId()
	record := g.Record{
		"remark": "settled",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if len(operationNos) > 0 {
		ex["operation_no"] = operationNos
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	if settleAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(settleAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       settleAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			CreatedAt:    ts.UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", settleAmount)
	}

	return id
}

// 取消投注 投注一笔，取消一笔
func walletCancelTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount decimal.Decimal, ts time.Time) memberTransaction {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	var trans memberTransaction
	if betAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(betAmount)
		trans = memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionBetCancel,
			Amount:       betAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", betAmount)
	}

	return trans
}

// 取消投注（已结算为赢的注单）
func walletCancelSettledWinTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, winAmount decimal.Decimal, ts time.Time) string {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if operationNo != "" {
		ex["operation_no"] = operationNo
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	if winAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Sub(winAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionSettledBetCancel,
			Amount:       winAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)

		myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
	}

	return id
}

// 取消投注 投注多笔，取消多笔
func walletCancelTransactions(mb myUserHelp.TblMemberBase, billNo, platformID string, operationNos []string, balance, betAmount decimal.Decimal, ts time.Time) string {

	id := helper.GenId()
	record := g.Record{
		"remark": "void",
	}
	ex := g.Ex{
		"bill_no": billNo,
	}
	if len(operationNos) > 0 {
		ex["operation_no"] = operationNos
	}
	query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)

	if betAmount.GreaterThan(decimal.Zero) {
		betAfterAmount := balance.Add(betAmount)
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionBetCancel,
			Amount:       betAmount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  betAfterAmount.String(),
			CreatedAt:    ts.UnixMilli(),
			Remark:       "void",
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)

		myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", betAmount)
	}

	return id
}

// 免费旋转奖金
func walletBonusWinTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) string {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBonusWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "bonus",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", bonus)
	return id
}

// 累计奖池赢奖
func walletJackpotWinTransaction(tx *sql.Tx, mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) (string, error) {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionJackPotWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "jackpot",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	fmt.Println(query)
	_, err := tx.Exec(query)
	if err != nil {
		fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
		_ = tx.Rollback()
		//记录日志
		if strings.HasPrefix(err.Error(), "Error 1062") {
			PushLog(nil, "", "duplicate", "", 200, nil, err)
		}

		return id, err
	}

	myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", bonus)
	return id, nil
}

// 促销赢奖
func walletPromoWinTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, bonus decimal.Decimal, ts time.Time) string {

	id := helper.GenId()
	betAfterAmount := balance.Add(bonus)
	trans := memberTransaction{
		ID:           id,
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionPromoWin,
		Amount:       bonus.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "jackpot",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", bonus)
	return id
}

func walletAdjustTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, amount decimal.Decimal, cashType int, ts time.Time) memberTransaction {

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionAdjustDiv {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	var trans memberTransaction
	if amount.Cmp(decimal.Zero) > 0 {
		trans = memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.UnixMilli(),
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
		operate := "+"
		if cashType == helper.TransactionAdjustDiv {
			operate = "-"
		}

		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else if operate == "-" {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return trans
}

func TransRedis(transId string) (TransJson, error) {

	trans := TransJson{}
	k := fmt.Sprintf("evolution_debit_amount:%s", transId)
	cmd := meta.MerchantRedis.Get(Ctx, k)
	fmt.Println(cmd.String())
	res, err := cmd.Result()
	if err != nil {
		if err == redis.Nil {
			return trans, nil
		}
		return trans, err
	}

	err = json.Unmarshal([]byte(res), &trans)
	if err != nil {
		return trans, err
	}

	return trans, nil
}
