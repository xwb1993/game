package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const EVO string = "EVO"

type plat_evo_t struct{}

func (plat_evo_t) Reg(args map[string]string) error {
	return nil
}

func (plat_evo_t) Launch(args map[string]string) (string, error) {

	var (
		isMobile bool
		isApp    bool
	)
	d, ok := args["deviceType"]
	if ok && (d == "1" || d == "2") {
		isMobile = true
	}
	if ok && (d == "3" || d == "4") {
		isApp = true
	}
	var request *evoLoginRequest
	if args["tester"] == "1" || args["tester"] == "3" {
		uid, _ := uuid.NewUUID()
		requestURI := fmt.Sprintf("%s/ua/v1/%s/%s", meta.PlatCfg.EVO.API, meta.PlatCfg.EVO.CasinoKey, meta.PlatCfg.EVO.APIToken)
		request = &evoLoginRequest{
			Uuid: uid.String(),
			Player: evoPlayer{
				Id:       "fullwin" + "_" + meta.Prefix + args["puid"],
				Update:   false,
				Country:  meta.PlatCfg.EVO.Country,
				Language: meta.PlatCfg.EVO.Language,
				Currency: meta.PlatCfg.EVO.Currency,
				Session: evoSession{
					Id: args["puid"],
					Ip: args["ip"],
				},
				FirstName: meta.Prefix,
				LastName:  args["username"],
				Nickname:  args["username"],
			},
			Config: evoLoginConfig{
				Channel: evoChannel{
					Mobile:  isMobile,
					Wrapped: isApp,
				},
				Brand: evoBrand{
					Id:   "1",
					Skin: "1",
				},
				Game: evoGameId{
					Category:  args["gamecode"],
					Interface: "view1",
				},
			},
			Url: requestURI,
		}
	} else if args["tester"] == "2" {
		uid, _ := uuid.NewUUID()
		requestURI := fmt.Sprintf("%s/ua/v1/%s/%s", meta.PlatCfg.EVOTest.API, meta.PlatCfg.EVOTest.CasinoKey, meta.PlatCfg.EVOTest.APIToken)
		request = &evoLoginRequest{
			Uuid: uid.String(),
			Player: evoPlayer{
				Id:       "fullwin" + "_" + meta.Prefix + args["puid"],
				Update:   false,
				Country:  meta.PlatCfg.EVOTest.Country,
				Language: meta.PlatCfg.EVOTest.Language,
				Currency: meta.PlatCfg.EVOTest.Currency,
				Session: evoSession{
					Id: args["puid"],
					Ip: args["ip"],
				},
				FirstName: meta.Prefix,
				LastName:  args["username"],
				Nickname:  args["username"],
			},
			Config: evoLoginConfig{
				Channel: evoChannel{
					Mobile:  isMobile,
					Wrapped: isApp,
				},
				Brand: evoBrand{
					Id:   "1",
					Skin: "1",
				},
				Game: evoGameId{
					Category:  args["gamecode"],
					Interface: "view1",
				},
			},
			Url: requestURI,
		}
	}

	// 进入指定的桌子
	//gameCode, ok := args["gamecode"]
	//if ok && gameCode != "" {
	//	request.Config.Game = evoGame{
	//		Table: evoGameTable{
	//			ID: gameCode,
	//		},
	//	}
	//}

	headers := map[string]string{
		"Content-Type": "application/json",
	}
	requestBody, _ := helper.JsonMarshal(request)
	statusCode, body, err := HttpPostHeaderWithPushLog(requestBody, args["username"], EVO, "http://177.71.145.60/evolution/index/createuser", headers)
	fmt.Println("requestBody===", string(requestBody))
	fmt.Println("statusCode===", statusCode)
	fmt.Println("body===", string(body))
	fmt.Println("err===", err)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := evoLoginRsp{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		fmt.Println("evo:Launch:format", string(body), err)
		return "", errors.New(helper.FormatErr)
	}

	return rsp.Entry, nil
}

func EVOCheck(token, userId string) EvoResponse {

	uid, _ := uuid.NewUUID()
	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
		Uuid:    uid.String(),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Tester == 1 || mb.Tester == 3 {
		if token != meta.PlatCfg.EVO.APIToken {
			res.Status = "INVALID_TOKEN_ID"
		}
	} else if mb.Tester == 2 {
		if token != meta.PlatCfg.EVOTest.APIToken {
			res.Status = "INVALID_TOKEN_ID"
		}
	}

	return res
}

// EVOSid 描述：用于 REST API 测试自动化的 sid 生成（不依赖于测试 UI 可用性）。返回的“sid”将用于“check”中的身份验证。
func EVOSid(body []byte) EVOSidResponse {

	ud, _ := uuid.NewUUID()
	res := EVOSidResponse{
		Status: "OK",
		Sid:    "",
		Uuid:   ud.String(),
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		return res
	}

	req := EvoCheckUserReq{
		AuthToken: values.Get("authToken"),
		Sid:       values.Get("sid"),
		UserId:    values.Get("userId"),
		Uuid:      values.Get("uuid"),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	res.Status = "OK"
	res.Sid = mb.Pid
	return res
}

// EVOCheckUser 检查玩家
func EVOCheckUser(body []byte) EvoCheckResponse {

	res := EvoCheckResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "ACCOUNT_LOCKED"
		return res
	}

	req := EvoCheckUserReq{
		AuthToken: values.Get("authToken"),
		Sid:       values.Get("sid"),
		UserId:    values.Get("userId"),
		Uuid:      values.Get("uuid"),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	res.Status = "OK"
	res.Sid = mb.Pid
	return res
}

// EVOBalance 余额
func EVOBalance(body []byte) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	req := EvoBalanceReq{
		Sid:    values.Get("sid"),
		UserId: values.Get("userId"),
		Uuid:   values.Get("uuid"),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0
	return res
}

// EVODebit 扣除投注金额
func EVODebit(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	req := EvoDebitReq{
		Sid:      values.Get("sid"),
		UserId:   values.Get("userId"),
		Uuid:     values.Get("uuid"),
		Currency: values.Get("currency"),

		Transaction: EvoTransaction{
			Id:    values.Get("transaction[id]"),
			RefId: values.Get("transaction[refId]"),
		},
	}
	transAmount, err := strconv.ParseFloat(values.Get("transaction[amount]"), 64)
	req.Transaction.Amount = transAmount
	fmt.Println("string(body)", string(body))
	fmt.Println("req", req)
	fmt.Println("Transaction.Id", req.Transaction.Id)
	fmt.Println("amount", transAmount)
	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	err = BetLock(mb.Username)
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	defer BetUnlock(mb.Username)

	amount := decimal.NewFromFloat(req.Transaction.Amount)
	status, _, betAmount, err := walletBetStatus(transactionRefId, transactionId, helper.EVO, "deduct")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "BET_ALREADY_EXIST"
		return res
	} else if err == nil { //此处说明已经有该注单下注帐变了
		if status == "void" {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}
		if status == "settled" || status == "running" {
			res.Status = "BET_ALREADY_EXIST"
			return res
		}

		amount = amount.Sub(betAmount)
		// 第二次扣款金额小于等于上一次扣款
		if amount.Cmp(decimal.Zero) <= 0 {
			res.Status = "INSUFFICIENT_FUNDS"
			return res
		}
	}

	// 不存在投注单，判断是否有取消注单请求帐变
	ex := g.Ex{
		"bill_no":      transactionRefId,
		"operation_no": transactionId,
		"cash_type":    helper.TransactionBetCancel,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		//先取消再投注
		trans := walletLateBetTransaction(mb, transactionRefId, transactionId, helper.EVO, amount.Abs(), t.UnixMilli())
		SetTransactionToRedis(ex, trans)
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	trans := walletBetTransaction(mb, transactionRefId, transactionId, helper.EVO, balance, amount, t)
	SetTransactionToRedis(ex, trans)
	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	var data TransJson
	data.ID = trans.ID
	data.Amount = betAmount
	data.TransactionRefId = transactionRefId
	b, _ := json.Marshal(data)
	k := fmt.Sprintf("evolution_debit_amount:%s", transactionRefId)
	fmt.Println("key===", k)
	pipe.Set(Ctx, k, string(b), 20)
	_, err = pipe.Exec(Ctx)
	if err != nil {
		res.Status = "RedisErr"
		return res
	}

	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0
	return res
}

// EVOCredit 派彩结算
func EVOCredit(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	req := EvoCreditReq{
		Sid:      values.Get("sid"),
		UserId:   values.Get("userId"),
		Uuid:     values.Get("uuid"),
		Currency: values.Get("currency"),

		Transaction: EvoTransaction{
			Id:    values.Get("transaction[id]"),
			RefId: values.Get("transaction[refId]"),
		},
	}
	transAmount, err := strconv.ParseFloat(values.Get("transaction[amount]"), 64)
	req.Transaction.Amount = transAmount
	fmt.Println("string(body)", string(body))
	fmt.Println("req", req)
	fmt.Println("Transaction.Id", req.Transaction.Id)
	fmt.Println("amount", transAmount)

	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	winLoss := decimal.NewFromFloat(req.Transaction.Amount)
	status, _, _, err := walletBetStatus(transactionRefId, "", helper.EVO, "settle")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	if err == sql.ErrNoRows {
		res.Status = "BET_DOES_NOT_EXIST"
		return res
	}

	if status == "settled" {
		res.Balance, _ = balance.Float64()
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	if status == "cancel" {
		res.Balance, _ = balance.Float64()
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	// evo可能同时出现同会员多笔派彩
	evoSettleLockKey := fmt.Sprintf("evo:settle:%s", mb.Username)
	err = Lock(evoSettleLockKey)
	if err != nil {
		time.Sleep(1 * time.Second)
	}

	defer Unlock(evoSettleLockKey)

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	trans, err := TransRedis(transactionRefId)
	fmt.Println("trans===", trans)
	if err != nil {
		res.Status = "transactionRefId error"
		return res
	}
	winMoney, validAmount, err := walletBetAndSettleTransaction(mb, trans, transactionId, helper.EVO, balance, winLoss, t)
	//_, err = walletSettleTransaction(tx, mb, transactionRefId, transactionId, helper.EVO, balance, winLoss, t)
	if err != nil {
		fmt.Println(err.Error())
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	money, _ := trans.Amount.Float64()
	if winLoss.Cmp(trans.Amount) == 0 || trans.Amount.Cmp(decimal.Zero) == 0 {
		validAmount = 0
	}
	if trans.Amount.Cmp(decimal.Zero) > 0 || winLoss.Cmp(decimal.Zero) > 0 {
		record := GameRecord{
			Uid:            mb.Uid,
			GameType:       "1",
			ApiName:        "EVO",
			Flag:           1,
			ApiType:        helper.EVO,
			CreatedAt:      time.Now().UnixMilli(),
			PlayerName:     mb.Username,
			Name:           mb.Username,
			BetAmount:      money,
			BetTime:        t.UnixMilli(),
			ApiBetTime:     t.UnixMilli(),
			SettleTime:     t.UnixMilli(),
			ApiSettleTime:  t.UnixMilli(),
			ValidBetAmount: validAmount,
			ApiBillNo:      transactionRefId,
			BillNo:         transactionRefId,
			Resettle:       0,
			Presettle:      0,
			RowId:          "evo" + transactionRefId,
			MainBillNo:     transactionRefId,
			SettleAmount:   winMoney,
			RebateAmount:   0,
			Prefix:         meta.Prefix,
			Tester:         "1",
		}

		record.ParentUid = mb.ParentID
		record.ParentName = mb.ParentName
		record.GrandID = mb.GrandID
		record.GrandName = mb.GrandName
		record.GreatGrandID = mb.GreatGrandID
		record.GreatGrandName = mb.GreatGrandName
		record.TopUid = mb.TopID
		record.TopName = mb.TopName
		record.NetAmount, _ = winLoss.Sub(trans.Amount).Float64()
		if mb.Uid == "" {
			record.Uid = "0"
		}
		if mb.GrandID == "0" {
			record.GrandID = "0"
			record.GrandName = "0"
		}
		if mb.GreatGrandID == "0" {
			record.GreatGrandID = "0"
			record.GreatGrandName = "0"
		}
		if mb.TopID == "0" {
			record.TopUid = "0"
			record.TopName = "0"
		}
		query, _, _ := dialect.Insert("tbl_game_record").Rows(record).ToSQL()
		myredis.AddLogToRedis(query)
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	// 参加了活动
	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()
	res.Bonus = 0

	return res
}

// EVOBonus 红利
func EVOBonus(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	req := EvoPromoReq{
		Sid:      values.Get("sid"),
		UserId:   values.Get("userId"),
		Uuid:     values.Get("uuid"),
		Currency: values.Get("currency"),

		PromoTransaction: EvoPromoTransaction{
			Id:        values.Get("promoTransaction[id]"),
			VoucherId: values.Get("promoTransaction[voucherId]"),
		},
	}
	transAmount, err := strconv.ParseFloat(values.Get("promoTransaction[amount]"), 64)
	req.PromoTransaction.Amount = transAmount
	transactionId := req.PromoTransaction.Id
	transactionRefId := req.PromoTransaction.VoucherId
	username := req.UserId
	if username == "" {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	amount := decimal.NewFromFloat(req.PromoTransaction.Amount)
	if amount.Cmp(decimal.Zero) < 1 {
		res.Status = "OK"
		res.Balance, _ = balance.Truncate(3).Float64()

		return res
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = walletEVOPrizeTransaction(mb, transactionRefId, transactionId, helper.EVO, balance, amount.Abs(), t)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	err = tx.Commit()
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	res.Status = "OK"
	res.Balance, _ = balance.Truncate(3).Float64()

	return res
}

func EVOCancel(body []byte, t time.Time) EvoResponse {

	res := EvoResponse{
		Status:  "OK",
		Balance: 0,
	}

	values, err := url.ParseQuery(string(body))
	if err != nil {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	req := EvoCancelReq{
		Sid:      values.Get("sid"),
		UserId:   values.Get("userId"),
		Uuid:     values.Get("uuid"),
		Currency: values.Get("currency"),

		Transaction: EvoTransaction{
			Id:    values.Get("transaction[id]"),
			RefId: values.Get("transaction[refId]"),
		},
	}
	transAmount, err := strconv.ParseFloat(values.Get("transaction[amount]"), 64)
	req.Transaction.Amount = transAmount
	transactionId := req.Transaction.Id
	transactionRefId := req.Transaction.RefId

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.UserId)
	if !ret {
		res.Status = "INVALID_PARAMETER"
		return res
	}

	if mb.Pid != req.Sid {
		res.Status = "INVALID_SID"
		return res
	}

	status, _, amount, err := walletBetStatus(transactionRefId, transactionId, helper.EVO, "cancel")
	if err != nil && err != sql.ErrNoRows {
		res.Status = "FINAL_ERROR_ACTION_FAILED"
		return res
	}

	if err == sql.ErrNoRows {
		//先取消再投注
		err = walletAdvanceCancelTransaction(mb, transactionRefId, transactionId, helper.EVO, decimal.Zero, t.UnixMilli())
		if err != nil {
			fmt.Println(err)
			if strings.HasPrefix(err.Error(), "Error 1062") {
				res.Status = "FINAL_ERROR_ACTION_FAILED"
				return res
			}

			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}
	}

	if status == "void" {
		res.Status = "BET_ALREADY_SETTLED"
		return res
	}

	if status == "settled" {
		payoutAmount, err := walletSettledBetStatus(transactionRefId, helper.EVO, "cancel")
		if err != nil && err != sql.ErrNoRows {
			res.Status = "FINAL_ERROR_ACTION_FAILED"
			return res
		}

		// 结算金额减去投注金额，为需要扣除的金额
		amount = amount.Sub(payoutAmount)
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	walletCancelTransaction(mb, transactionRefId, transactionId, helper.EVO, balance, amount.Abs(), t)
	res.Balance, _ = balance.Truncate(3).Float64()
	return res
}
