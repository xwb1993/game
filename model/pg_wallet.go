package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const PG string = "PG"

type plat_pg_t struct{}

// PGReg 注册/登陆 不存在账号会自动创建
func (plat_pg_t) Reg(args map[string]string) error {

	//playerName := meta.Prefix + args["puid"]
	//requestBody := url.Values{}
	//requestBody.Set("operator_token", meta.PlatCfg.PG.OperatorToken)
	//requestBody.Set("secret_key", meta.PlatCfg.PG.SecretKey)
	//requestBody.Set("player_name", playerName)
	//requestBody.Set("currency", meta.PlatCfg.PG.Currency)
	//requestBody.Set("nickname", playerName)
	//
	//headers := map[string]string{
	//	"Content-Type": "application/x-www-form-urlencoded",
	//}
	//requestURI := fmt.Sprintf("%s/Player/v1/Create?trace_id=%s", meta.PlatCfg.PG.API, UUID())
	//statusCode, body, err := HttpPostWithPushLog([]byte(requestBody.Encode()), args["username"], PG, requestURI, headers)
	//if err != nil {
	//	return errors.New(helper.PlatformRegErr)
	//}
	//
	//if statusCode != fasthttp.StatusOK {
	//	return errors.New(helper.PlatformRegErr)
	//}
	//
	//rsp := pgRegRsp{}
	//err = helper.JsonUnmarshal(body, &rsp)
	//if err != nil {
	//	fmt.Println("pg:Reg:format", string(body), err)
	//	return errors.New(helper.FormatErr)
	//}
	//
	//// 注册成功，重复注册
	//if rsp.Data.ActionResult == 1 || rsp.Error.Code == "1305" {
	//	return nil
	//}
	//
	//return errors.New(helper.PlatformRegErr)
	return nil
}

func (plat_pg_t) Launch(args map[string]string) (string, error) {
	dst := "fullwin" + "_" + args["puid"]
	ops := helper.ThinkEncrypt(dst, "pggme", 0)
	var launchURL string
	var req *http.Request
	var err error
	if args["tester"] == "1" || args["tester"] == "3" {
		// 创建表单数据
		formData := url.Values{
			"operator_token": {meta.PlatCfg.PG.OperatorToken},
			"path":           {fmt.Sprintf("/%s/index.html", args["gamecode"])},
			"url_type":       {"game-entry"},
			"client_ip":      {args["ip"]},
			"ops":            {ops},
			"l":              {meta.PlatCfg.PG.Language},
			"url":            {meta.PlatCfg.PG.API},
		}

		// 创建请求
		req, err = http.NewRequest("POST", meta.PlatCfg.PG.LaunchURL+"/pggame/index/index", strings.NewReader(formData.Encode()))
		if err != nil {
			return launchURL, errors.New(helper.PlatformLoginErr)
		}
	} else if args["tester"] == "2" {
		// 创建表单数据
		formData := url.Values{
			"operator_token": {meta.PlatCfg.PGTest.OperatorToken},
			"path":           {fmt.Sprintf("/%s/index.html", args["gamecode"])},
			"url_type":       {"game-entry"},
			"client_ip":      {args["ip"]},
			"ops":            {ops},
			"l":              {meta.PlatCfg.PGTest.Language},
			"url":            {meta.PlatCfg.PGTest.API},
		}

		// 创建请求
		req, err = http.NewRequest("POST", meta.PlatCfg.PGTest.LaunchURL+"/pggame/index/index", strings.NewReader(formData.Encode()))
		if err != nil {
			return launchURL, errors.New(helper.PlatformLoginErr)
		}

	}

	// 设置请求头
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Cache-Control", "no-cache, no-store, must-revalidate")

	// 创建客户端
	client := &http.Client{}

	// 发送请求
	resp, err := client.Do(req)
	if err != nil {
		return launchURL, errors.New(helper.PlatformLoginErr)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return launchURL, errors.New(helper.PlatformLoginErr)
	}
	fmt.Println(resp.Status)
	fmt.Println(string(body))
	launchURL = string(body)

	return launchURL, nil
}

func PGVerifySession(ctx *fasthttp.RequestCtx) PGResponse {
	return pgCheckToken(ctx, true)
}

// token检查
func pgCheckToken(ctx *fasthttp.RequestCtx, checkSession bool) PGResponse {

	token := string(ctx.PostArgs().Peek("operator_token"))
	secretKey := string(ctx.PostArgs().Peek("secret_key"))

	rsp := PGResponse{
		Error: nil,
	}

	if checkSession {
		session := string(ctx.PostArgs().Peek("operator_player_session"))

		fmt.Printf("pgCheckToken session : %s\n", session)

		if strings.Contains(session, "Invalid") {
			rsp.Error = PGError{
				Code:    "1034",
				Message: "token err",
			}
			return rsp
		}

		dst := helper.ThinkDecrypt(session, "pggme")

		mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
		if !ret {
			rsp.Error = PGError{
				Code:    "1034",
				Message: "token err",
			}
			return rsp
		}

		if mb.Tester == 1 || mb.Tester == 3 {
			if token != meta.PlatCfg.PG.OperatorToken || secretKey != meta.PlatCfg.PG.SecretKey {
				fmt.Printf("pgCheckToken token : %s, operatorToken : %s, key : %s, secretKey = %s, salt = %s\n",
					token, meta.PlatCfg.PG.OperatorToken, secretKey, meta.PlatCfg.PG.SecretKey, meta.PlatCfg.PG.Salt)

				rsp.Error = PGError{
					Code:    "1034",
					Message: "token err",
				}
				return rsp
			}
		} else if mb.Tester == 2 {
			if token != meta.PlatCfg.PGTest.OperatorToken || secretKey != meta.PlatCfg.PGTest.SecretKey {
				fmt.Printf("pgCheckToken token : %s, operatorToken : %s, key : %s, secretKey = %s, salt = %s\n",
					token, meta.PlatCfg.PGTest.OperatorToken, secretKey, meta.PlatCfg.PGTest.SecretKey, meta.PlatCfg.PGTest.Salt)

				rsp.Error = PGError{
					Code:    "1034",
					Message: "token err",
				}
				return rsp
			}
		}

		rsp.Data = PGVerifyData{
			PlayerName: meta.Prefix + mb.Uid,
			Nickname:   meta.Prefix + mb.Uid,
			Currency:   meta.PlatCfg.PG.Currency,
		}
	}

	return rsp
}

// PGCashGet 获取余额
func PGCashGet(ctx *fasthttp.RequestCtx) PGResponse {

	playerName := string(ctx.PostArgs().Peek("player_name"))
	rsp := pgCheckToken(ctx, false)
	if rsp.Error != nil {
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(playerName)
	if !ret {
		rsp.Error = PGError{
			Code:    "1034",
			Message: "playerName err",
		}
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	fBalance, _ := balance.Float64()
	rsp.Data = PGBalanceData{
		CurrencyCode:  meta.PlatCfg.PG.Currency,
		BalanceAmount: fBalance,
		UpdatedTime:   time.Now().UnixMilli(),
	}

	return rsp
}

/*
operator_token=32b7ddd99f36c0d1b3263033aa52cfa3&secret_key=4a80a1f30df8cf464fc4d2b127adcec2&operator_player_session=7824c795561d65b786409f4b779e722d&game_id=100&parent_bet_id=1548251938284908545&bet_id=1548251953808027648&player_name=w88test2241&currency_code=VND&transfer_amount=0.00&wallet_type=C&bet_type=1&transaction_id=1548251953808027648-1548251938284908545-106-0&updated_time=1657967005565&is_feature=True&is_minus_count=False&is_end_round=True&bet_amount=0.00&win_amount=0.00&platform=2&create_time=1657967005565&is_parent_zero_stake=False&jackpot_rtp_contribution_amount=0&jackpot_win_amount=0&jackpot_pool_id=0&jackpot_type=0
*/

// PGCashTransferInOut 投注/派彩
func PGCashTransferInOut(ctx *fasthttp.RequestCtx) PGResponse {
	fmt.Println("投注===", string(ctx.PostBody()))
	updatedTime := string(ctx.PostArgs().Peek("updated_time"))
	playerName := string(ctx.PostArgs().Peek("player_name"))
	transactionID := string(ctx.PostArgs().Peek("transaction_id"))
	currencyCode := string(ctx.PostArgs().Peek("currency_code"))
	betID := string(ctx.PostArgs().Peek("bet_id"))
	//sTransferAmount := string(ctx.PostArgs().Peek("transfer_amount"))
	sBetAmount := string(ctx.PostArgs().Peek("bet_amount"))
	sWinAmount := string(ctx.PostArgs().Peek("win_amount"))
	isValidateBet := string(ctx.PostArgs().Peek("is_validate_bet"))
	isAdjustment := string(ctx.PostArgs().Peek("is_adjustment"))
	//isEndRound := string(ctx.PostArgs().Peek("is_end_round"))
	fmt.Println("sBetAmount===", sBetAmount)
	fmt.Println("sWinAmount1===", sWinAmount)
	checkSession := false
	if isValidateBet == "True" || isAdjustment == "True" {
		checkSession = true
	}
	rsp := pgCheckToken(ctx, checkSession)
	if rsp.Error != nil {
		return rsp
	}

	if currencyCode != meta.PlatCfg.PG.Currency {
		rsp.Error = PGError{
			Code:    "1034",
			Message: "currency_code err",
		}
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(playerName)
	if !ret {
		rsp.Error = PGError{
			Code:    "3004",
			Message: "playerName err",
		}
		return rsp
	}

	var balance, betAmount, winAmount decimal.Decimal
	var fBalance, winMoney, validAmount float64
	var ut int64
	// 投注金额
	var err error
	betAmount, err = decimal.NewFromString(sBetAmount)
	//派彩金额
	winAmount, err = decimal.NewFromString(sWinAmount)
	if betAmount.Cmp(decimal.Zero) > 0 || winAmount.Cmp(decimal.Zero) > 0 {
		if meta.WalletMode == "1" {
			balance, _ = myUserHelp.GetBalance(mb, 2)
			fBalance, _ = balance.Truncate(3).Float64()
			ut, _ = strconv.ParseInt(updatedTime, 10, 64)
			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "amount error",
				}
				return rsp
			}

			// 输赢金额加余额是否大于0
			if balance.LessThan(betAmount.Abs()) {
				rsp.Error = PGError{
					Code:    "3202",
					Message: "player balance not enoug",
				}
				return rsp
			}

			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "amount error",
				}
				return rsp
			}

			_, _, _, err = walletBetStatus(transactionID, betID, helper.PG, "bet")
			if err != nil && err != sql.ErrNoRows {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}

			if err == nil {
				rsp.Data = PGBalanceData{
					CurrencyCode:  meta.PlatCfg.PG.Currency,
					BalanceAmount: fBalance,
					UpdatedTime:   ut,
				}
				return rsp
			}

			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}

			//投注
			winMoney, validAmount, _, err = walletBetNSettleTransaction(mb, transactionID, betID, helper.PG, balance, betAmount.Abs(), winAmount.Abs(), ctx.Time())
			if err != nil {
				if err.Error() == helper.BalanceErr {
					rsp.Error = PGError{
						Code:    "3005",
						Message: "balance error",
					}
					return rsp
				}

				if strings.HasPrefix(err.Error(), "Error 1062") {
					rsp.Data = PGBalanceData{
						CurrencyCode:  meta.PlatCfg.PG.Currency,
						BalanceAmount: fBalance,
						UpdatedTime:   ut,
					}
					return rsp
				}

				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}
		} else if meta.WalletMode == "2" {
			memBalance, ret := myUserHelp.GetMemberBalanceInfo(mb.Uid)
			if !ret {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "amount error",
				}
				return rsp
			}
			bal := decimal.NewFromFloat(memBalance.Brl)
			balance = bal.Truncate(2)
			fBalance, _ = balance.Truncate(3).Float64()
			ut, _ = strconv.ParseInt(updatedTime, 10, 64)

			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "amount error",
				}
				return rsp
			}
			unlockAmount := decimal.NewFromFloat(memBalance.UnlockAmount)
			depositLockAmount := decimal.NewFromFloat(memBalance.DepositLockAmount)
			agencyAmount := decimal.NewFromFloat(memBalance.AgencyAmount)
			wallet := &Wallet{
				UnlockAmount:      unlockAmount,
				DepositLockAmount: depositLockAmount,
				AgencyAmount:      agencyAmount,
				IsValid:           false,
			}

			memWallet, err := PlayGame(betAmount, wallet)
			fmt.Println("err===", err)
			if err != nil {
				rsp.Error = PGError{
					Code:    "3202",
					Message: "player balance not enough",
				}
				return rsp
			}
			fmt.Println("memWallet===", memWallet)
			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "amount error",
				}
				return rsp
			}

			_, _, _, err = walletBetStatus(transactionID, betID, helper.PG, "bet")
			if err != nil && err != sql.ErrNoRows {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}

			if err == nil {
				rsp.Data = PGBalanceData{
					CurrencyCode:  meta.PlatCfg.PG.Currency,
					BalanceAmount: fBalance,
					UpdatedTime:   ut,
				}
				return rsp
			}

			if err != nil {
				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}

			//投注
			winMoney, validAmount, err = walletBetSettleTransaction(mb, transactionID, betID, helper.PG, balance, betAmount.Abs(), winAmount.Abs(), ctx.Time(), memWallet)
			if err != nil {
				if err.Error() == helper.BalanceErr {
					rsp.Error = PGError{
						Code:    "3005",
						Message: "balance error",
					}
					return rsp
				}

				if strings.HasPrefix(err.Error(), "Error 1062") {
					rsp.Data = PGBalanceData{
						CurrencyCode:  meta.PlatCfg.PG.Currency,
						BalanceAmount: fBalance,
						UpdatedTime:   ut,
					}
					return rsp
				}

				rsp.Error = PGError{
					Code:    "3033",
					Message: "transaction error",
				}
				return rsp
			}

		}

		money, _ := betAmount.Float64()
		if winAmount.Cmp(betAmount) == 0 || betAmount.Cmp(decimal.Zero) == 0 {
			validAmount = 0
		}
		//if betAmount.Cmp(decimal.Zero) > 0 || winAmount.Cmp(decimal.Zero) > 0 {
		record := GameRecord{
			Uid:            mb.Uid,
			GameType:       "3",
			ApiName:        "PG",
			Flag:           1,
			ApiType:        helper.PG,
			CreatedAt:      time.Now().UnixMilli(),
			PlayerName:     mb.Username,
			Name:           mb.Username,
			BetAmount:      money,
			BetTime:        ctx.Time().UnixMilli(),
			ApiBetTime:     ctx.Time().UnixMilli(),
			SettleTime:     ctx.Time().UnixMilli(),
			ApiSettleTime:  ctx.Time().UnixMilli(),
			ValidBetAmount: validAmount,
			ApiBillNo:      transactionID,
			BillNo:         transactionID,
			Resettle:       0,
			Presettle:      0,
			RowId:          "pgdy" + transactionID,
			MainBillNo:     transactionID,
			SettleAmount:   winMoney,
			RebateAmount:   0,
			Prefix:         meta.Prefix,
			Tester:         "1",
		}

		record.ParentUid = mb.ParentID
		record.ParentName = mb.ParentName
		record.GrandID = mb.GrandID
		record.GrandName = mb.GrandName
		record.GreatGrandID = mb.GreatGrandID
		record.GreatGrandName = mb.GreatGrandName
		record.TopUid = mb.TopID
		record.TopName = mb.TopName
		record.NetAmount, _ = winAmount.Sub(betAmount).Float64()
		if mb.Uid == "" {
			record.Uid = "0"
		}
		if mb.GrandID == "0" {
			record.GrandID = "0"
			record.GrandName = "0"
		}
		if mb.GreatGrandID == "0" {
			record.GreatGrandID = "0"
			record.GreatGrandName = "0"
		}
		if mb.TopID == "0" {
			record.TopUid = "0"
			record.TopName = "0"
		}
		query, _, _ := dialect.Insert("tbl_game_record").Rows(record).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}
	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		rsp.Error = PGError{
			Code:    "3005",
			Message: "balance error",
		}
		return rsp
	}

	fBalance, _ = balance.Truncate(3).Float64()
	rsp.Data = PGBalanceData{
		CurrencyCode:  meta.PlatCfg.PG.Currency,
		BalanceAmount: fBalance,
		UpdatedTime:   ut,
	}
	//}

	return rsp
}

// PGCashAdjustment 余额调整
func PGCashAdjustment(ctx *fasthttp.RequestCtx) PGResponse {

	playerName := string(ctx.PostArgs().Peek("player_name"))
	currencyCode := string(ctx.PostArgs().Peek("currency_code"))
	adjustmentID := string(ctx.PostArgs().Peek("adjustment_id"))
	transactionID := string(ctx.PostArgs().Peek("adjustment_transaction_id"))
	transferAmount := string(ctx.PostArgs().Peek("transfer_amount"))
	isValidateBet := string(ctx.PostArgs().Peek("is_validate_bet"))
	isAdjustment := string(ctx.PostArgs().Peek("is_adjustment"))

	checkSession := false
	if isValidateBet == "True" || isAdjustment == "True" {
		checkSession = true
	}
	rsp := pgCheckToken(ctx, checkSession)
	if rsp.Error != nil {
		return rsp
	}

	if currencyCode != meta.PlatCfg.PG.Currency {
		rsp.Error = PGError{
			Code:    "1034",
			Message: "currency_code error",
		}
		return rsp
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(playerName)
	if !ret {
		rsp.Error = PGError{
			Code:    "3004",
			Message: "playerName error",
		}
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	amount, err := decimal.NewFromString(transferAmount)
	if err != nil {
		rsp.Error = PGError{
			Code:    "3033",
			Message: "amount error",
		}
		return rsp
	}

	if amount.Equals(decimal.Zero) {
		rsp.Error = nil
		return rsp
	}

	_, _, _, err = walletBetStatus(transactionID, "", helper.PG, "status")
	if err != nil && err != sql.ErrNoRows {
		rsp.Error = PGError{
			Code:    "3033",
			Message: "transaction error",
		}
		return rsp
	}

	if err == nil {
		rsp.Error = nil
		return rsp
	}

	ts := ctx.Time()
	cashType := helper.TransactionAdjustPlus
	if amount.IsNegative() {
		cashType = helper.TransactionAdjustDiv
	}
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.Error = PGError{
			Code:    "3033",
			Message: "transaction error",
		}
		return rsp
	}

	err = walletBetNSettleAdjustTransaction(mb, transactionID, "", helper.PG, adjustmentID, amount.Abs(), cashType, ts.UnixMilli())
	if err != nil {
		if err.Error() == helper.BalanceErr {
			rsp.Error = PGError{
				Code:    "3005",
				Message: "balance error",
			}
			return rsp
		}

		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Error = nil
			return rsp
		}

		rsp.Error = PGError{
			Code:    "3033",
			Message: "transaction error",
		}
		return rsp
	}

	err = tx.Commit()
	if err != nil {
		rsp.Error = PGError{
			Code:    "3033",
			Message: "transaction error",
		}
		return rsp
	}

	data := PGAdjustData{
		UpdatedTime: ts.UnixMilli(),
	}
	data.BalanceBefore, _ = balance.Float64()
	data.BalanceAfter, _ = balance.Add(amount).Float64()
	data.AdjustAmount, _ = amount.Float64()
	rsp.Data = data

	return rsp
}

type Wallet struct {
	UnlockAmount      decimal.Decimal
	DepositLockAmount decimal.Decimal
	AgencyAmount      decimal.Decimal
	IsValid           bool
}

func PlayGame(bet decimal.Decimal, wallet *Wallet) (Wallet, error) {
	var rsp Wallet

	max := myConfig.GetGameCfgDecimal(myConfig.CfgMaxBetRealAmount)
	//如果真金钱包未消耗为彩金能投注的最高真金金额以下,那就永远不能用彩金进行游戏
	if wallet.UnlockAmount.Cmp(max) > 0 {
		if bet.LessThanOrEqual(wallet.UnlockAmount) {
			wallet.UnlockAmount = wallet.UnlockAmount.Sub(bet)
			bet = decimal.Zero
		} else {
			return rsp, errors.New("betting amount is greater than the available unlock amount")
		}
	}
	//真金消耗完开始消耗首存彩金
	if wallet.UnlockAmount.Cmp(max) <= 0 {
		if bet.LessThanOrEqual(wallet.UnlockAmount) && wallet.UnlockAmount.GreaterThan(decimal.Zero) {
			wallet.UnlockAmount = wallet.UnlockAmount.Sub(bet)
			bet = decimal.Zero
		} else if bet.LessThanOrEqual(wallet.UnlockAmount.Add(wallet.DepositLockAmount)) {
			bet = bet.Sub(wallet.UnlockAmount)
			wallet.DepositLockAmount = wallet.DepositLockAmount.Sub(bet)
			wallet.UnlockAmount = decimal.Zero
			bet = decimal.Zero
			wallet.IsValid = true
		} else {
			bet = bet.Sub(wallet.UnlockAmount).Sub(wallet.DepositLockAmount)
			wallet.UnlockAmount = decimal.Zero
			wallet.DepositLockAmount = decimal.Zero
			wallet.IsValid = true
		}
	}
	//首存彩金消耗完开始消耗手动彩金
	if bet.GreaterThan(decimal.Zero) {
		if bet.LessThanOrEqual(wallet.AgencyAmount) {
			wallet.AgencyAmount = wallet.AgencyAmount.Sub(bet)
			bet = decimal.Zero
		} else {
			wallet.IsValid = false
			return rsp, errors.New("betting amount is greater than all the available amount")
		}
	}
	rsp.UnlockAmount = wallet.UnlockAmount
	rsp.DepositLockAmount = wallet.DepositLockAmount
	rsp.AgencyAmount = wallet.AgencyAmount
	rsp.IsValid = wallet.IsValid
	return rsp, nil
}
