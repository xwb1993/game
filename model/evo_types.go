package model

type evoLoginRequest struct {
	Uuid   string         `json:"uuid"` //Unique request id, that identifies concrete user authentication request (attempt).
	Player evoPlayer      `json:"player"`
	Config evoLoginConfig `json:"config"`
	Url    string         `json:"url"`
}

type evoLoginConfig struct {
	Channel evoChannel `json:"channel"`
	Brand   evoBrand   `json:"brand"`
	Game    evoGameId  `json:"game"`
	//Game    interface{} `json:"game"`
}

type evoGame struct {
	Table evoGameTable `json:"table"`
}

type evoGameTable struct {
	ID string `json:"id"`
}

type evoGameId struct {
	Category  string `json:"category"`  //Specifies if client is wrapped or standalone. Should only be sent as true for standalone native or mobile apps.
	Interface string `json:"interface"` //Specifies if game is launched using mobile device.
}

type evoBrand struct {
	Id   string `json:"id"`   //Specifies if client is wrapped or standalone. Should only be sent as true for standalone native or mobile apps.
	Skin string `json:"skin"` //Specifies if game is launched using mobile device.
}

type evoChannel struct {
	Wrapped bool `json:"wrapped"` //Specifies if client is wrapped or standalone. Should only be sent as true for standalone native or mobile apps.
	Mobile  bool `json:"mobile"`  //Specifies if game is launched using mobile device.
}

type evoPlayer struct {
	Id        string     `json:"id"`       //Player's ID. Unique identifier of a player, assigned by Licensee.
	Update    bool       `json:"update"`   //Indicates if player details should be updated. True if system is asked to update player records. False if player data is relevant for current session only. Updates firstName, lastName, nickname, country, language values.
	Country   string     `json:"country"`  //Player's country code (ISO 3166, 2 letter code). See Country Code Table.
	Language  string     `json:"language"` //Player's preferred language (ISO 639-1, 2 letter code). See Language Code Table
	Currency  string     `json:"currency"` //Player's currency (ISO 4217, 3 letter code). See Currency Code Table.
	Session   evoSession `json:"session"`
	FirstName string     `json:"firstName"`
	LastName  string     `json:"lastName"`
	Nickname  string     `json:"nickname"` //昵称
}

type evoLoginRsp struct {
	Entry         string `json:"entry"`
	EntryEmbedded string `json:"entryEmbedded"`
}

type evoSession struct {
	Id string `json:"id"` //Player's session ID, assigned by Licensee.
	Ip string `json:"ip"`
}

type EvoResponse struct {
	Status  string  `json:"status"`
	Balance float64 `json:"balance"`
	Bonus   float64 `json:"bonus"`
	Uuid    string  `json:"uuid"`
}

type EvoCheckResponse struct {
	Status  string  `json:"status"`
	Balance float64 `json:"balance"`
	Bonus   float64 `json:"bonus"`
	Uuid    string  `json:"uuid"`
	Sid     string  `json:"sid"`
}

type EvoErrResponse struct {
	Status string `json:"status"`
	Uuid   string `json:"uuid"`
}

type EVOSidReq struct {
	Sid     string `json:"sid"`
	UserId  string `json:"userId"`
	Channel struct {
		Type string `json:"type"`
	} `json:"channel"`
	Uuid string `json:"uuid"`
}

type EVOSidResponse struct {
	Status string `json:"status"`
	Sid    string `json:"sid"`
	Uuid   string `json:"uuid"`
}

type EvoCreditReq struct {
	Sid         string         `json:"sid"`
	UserId      string         `json:"userId"`
	Currency    string         `json:"currency"`
	Game        EvoGame        `json:"game"`
	Transaction EvoTransaction `json:"transaction"`
	Uuid        string         `json:"uuid"`
}

type EvoCheckUserReq struct {
	Channel struct {
		Type string `json:"type"`
	} `json:"channel"`
	Sid       string `json:"sid"`
	UserId    string `json:"userId"`
	Uuid      string `json:"uuid"`
	AuthToken string `json:"authToken"`
}

type EvoBalanceReq struct {
	UserId   string  `json:"userId"`
	Sid      string  `json:"sid"`
	Currency string  `json:"currency"`
	Game     EvoGame `json:"game"`
	Uuid     string  `json:"uuid"`
}

type EvoGame struct {
	Id      string        `json:"id"`
	Type    string        `json:"type"`
	Details EvoGameDetail `json:"details"`
}

type EvoGameDetail struct {
	Table EvoTable `json:"table"`
}

type EvoTable struct {
	Id  string `json:"id"`
	Vid string `json:"vid"`
}

type EvoDebitReq struct {
	Sid         string         `json:"sid"`
	UserId      string         `json:"userId"`
	Currency    string         `json:"currency"`
	Game        EvoGame        `json:"game"`
	Transaction EvoTransaction `json:"transaction"`
	Uuid        string         `json:"uuid"`
}
type EvoTransaction struct {
	Id     string  `json:"id"`
	RefId  string  `json:"refId"`
	Amount float64 `json:"amount"`
}

type EvoCancelReq struct {
	Transaction EvoTransaction `json:"transaction"`
	Sid         string         `json:"sid"`
	UserId      string         `json:"userId"`
	Uuid        string         `json:"uuid"`
	Currency    string         `json:"currency"`
	Game        EvoGame        `json:"game"`
}

type EvoPromoReq struct {
	Sid              string              `json:"sid"`
	UserId           string              `json:"userId"`
	Currency         string              `json:"currency"`
	Game             EvoGame             `json:"game"`
	PromoTransaction EvoPromoTransaction `json:"promoTransaction"`
	Uuid             string              `json:"uuid"`
}

type EvoPromoTransaction struct {
	Type            string  `json:"type"`
	Id              string  `json:"id"`
	Amount          float64 `json:"amount"`
	VoucherId       string  `json:"voucherId"`
	RemainingRounds int     `json:"remainingRounds"`
}
