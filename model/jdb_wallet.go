package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"strings"
	"time"
)

const JDB = "JDB"

type plat_jdb_t struct{}

func (plat_jdb_t) Reg(args map[string]string) error {
	return nil
}

func (plat_jdb_t) Launch(args map[string]string) (string, error) {

	gameCode := args["gamecode"]
	gType, ok := gTypes[gameCode]
	if !ok {
		return "", errors.New(helper.PlatformLoginErr)
	}
	memberBalance, ret := myUserHelp.GetMemberBalanceInfo(args["uid"])
	if !ret {
		return "", errors.New(helper.PlatformLoginErr)
	}

	req := map[string]interface{}{
		"action":  21,
		"ts":      time.Now().In(usEasternLoc).UnixMilli(),
		"parent":  meta.PlatCfg.JDB.Agent,
		"uid":     "fullwin" + "abc" + args["puid"],
		"balance": memberBalance.Brl,
		"gType":   gType,
		"mType":   gameCode,
		"lang":    meta.PlatCfg.JDB.Lang,
	}
	jsonStr, _ := helper.JsonMarshal(req)
	fmt.Println(string(jsonStr), meta.PlatCfg.JDB.Key, meta.PlatCfg.JDB.Iv)
	x, err := JDBAesEncrypt(jsonStr, []byte(meta.PlatCfg.JDB.Key), []byte(meta.PlatCfg.JDB.Iv))
	if err != nil {
		fmt.Println(err)
		return "", errors.New(helper.PlatformLoginErr)
	}

	//requestBody := url.QueryEscape(fmt.Sprintf("dc=%s&x=%s", meta.PlatCfg.JDB.Dc, x))
	requestURI := fmt.Sprintf("%s/apiRequest.do?dc=%s&x=%s", meta.PlatCfg.JDB.API, meta.PlatCfg.JDB.Dc, x)
	headers := map[string]string{
		"Content-Type":  "application/x-www-form-urlencoded",
		"cache-control": "no-cache",
	}
	postParam := map[string]string{"url": requestURI}
	requestBody := ParamEncode(postParam)
	statusCode, body, err := HttpPostHeaderWithPushLog([]byte(requestBody), args["username"], JDB, "http://177.71.145.60/jdbspr/index/createuser", headers)
	fmt.Println("requestBody===", requestBody)
	fmt.Println("statusCode===", statusCode)
	fmt.Println("body===", string(body))
	fmt.Println("err===", err)
	if err != nil {
		fmt.Println(err)
		return "", errors.New(helper.PlatformLoginErr)
	}

	fmt.Println(requestURI)
	fmt.Println(string(jsonStr))

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := spribeRsp{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		return "", errors.New(helper.FormatErr)
	}

	if rsp.Status == "0000" {
		return rsp.Path, nil
	}

	return "", errors.New(helper.PlatformLoginErr)
}

func jdbUnpack(x []byte) (*fastjson.Value, []byte, error) {

	//fmt.Println("jdbUnpack ", string(x))
	var p fastjson.Parser
	body, err := JDBAesDecrypt(string(x), []byte(meta.PlatCfg.JDB.Key), []byte(meta.PlatCfg.JDB.Iv))
	if err != nil {
		fmt.Println("JDBAesCBCDecrypt ", err)
		return nil, nil, err
	}

	s := strings.Trim(string(body), "\x00")
	fmt.Println("jdb", s)
	v, err := p.ParseBytes([]byte(s))
	if err != nil {
		fmt.Println("ParseBytes ", err)
		return nil, nil, err
	}

	return v, []byte(s), nil
}

/*
{"uid":"w88test2241","currency":"VN","ts":1658145132206,"action":6}
{"jackpotWin":0.0,"jackpotContribute":-1.5E-4,"hasFreeGame":0,"hasGamble":0,"action":8,"transferId":938034,"gameSeqNo":5250205891353,"uid":"w88test2241","gType":0,"mType":14063,"reportDate":"18-07-2022","gameDate":"18-07-2022 07:51:05","bet":-0.03,"win":0.04,"netWin":0.01,"currency":"VN","denom":0.01,"lastModifyTime":"18-07-2022 07:51:05","ipAddress":"49.157.15.129","clientType":"WEB","systemTakeWin":0,"ts":1658145065118}
*/

func Jdb(ctx *fasthttp.RequestCtx) (JDBResponse, []byte) {

	fmt.Println("Jdb", ctx.PostArgs().String())

	rsp := JDBResponse{}
	x := ctx.PostArgs().Peek("x")
	v, body, err := jdbUnpack(x)
	if err != nil {
		rsp.Status = "9006"
		rsp.ErrText = "Failed to extract the SAML parameters from the encrypted data."
		return rsp, body
	}

	action := v.GetInt("action")
	switch action {
	case 6: //查询余额
		return jdbGetBalance(body), body
	case 4: //取消投付
		return jdbCancelBetNSettle(body, ctx.Time()), body
	case 8: //投付
		gType := v.GetInt("gType")
		return jdbBetNSettle(body, gType, ctx.Time()), body
	case 9: //投注
		return jdbBet(body, ctx.Time()), body
	case 10: //结算
		return jdbSettle(body, ctx.Time()), body
	case 11: //取消投注
		return jdbCancelBet(body, ctx.Time()), body
	default:
	}

	rsp.Status = "9007"
	rsp.ErrText = "Unknown action."
	return rsp, body
}

/*
{"action":6,"ts":1447452951820,"uid":"testpl01","currency":"RB"}
*/
// jdbGetBalance 获取余额
func jdbGetBalance(body []byte) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}
	req := JDBBalance{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	pid := strings.Replace(req.Uid, "fullwinabc", "", -1)

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(pid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	rsp.Balance, _ = balance.Truncate(3).Float64()
	rsp.Status = "0000"
	return rsp
}

/*
{"action":9,"ts":1664358643847,"transferId":251916,"uid":"testpl","currency":"RM","amount":5000,"gameRoundSeqNo":1975826710,"mType":18024}
*/
//jdbBet 投注
func jdbBet(body []byte, ts time.Time) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}
	req := JDBBet{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}
	pid := strings.Replace(req.Uid, "fullwinabc", "", -1)

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(pid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	ex := g.Ex{
		"bill_no":      req.TransferId,
		"operation_no": req.GameRoundSeqNo,
		"cash_type":    helper.TransactionBet,
	}
	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.Balance, _ = balance.Truncate(3).Float64()
		return rsp
	}

	amount := decimal.NewFromFloat(req.Amount)
	// 输赢金额加余额是否大于0
	if balance.LessThan(amount.Abs()) {
		rsp.Status = "6006"
		rsp.ErrText = "Your Cash Balance not enough."
		return rsp
	}

	transferId := fmt.Sprintf("%d", req.TransferId)
	gameRoundSeqNo := fmt.Sprintf("%d", req.GameRoundSeqNo)
	//投注
	trans := walletBetTransaction(mb, gameRoundSeqNo, transferId, helper.JDBSpribe, balance, amount.Abs(), ts)
	SetTransactionToRedis(ex, trans)

	rowID := fmt.Sprintf("jdb%d%s", req.GameRoundSeqNo, transferId)
	mType := fmt.Sprintf("%d", req.MType)
	gm := GameRecord{
		BillNo:         transferId,
		Result:         fmt.Sprintf("Hand number:%d", req.GameRoundSeqNo),
		GameName:       JDBGmap[mType],
		GameCode:       mType,
		PlayType:       mType,
		RowId:          rowID,
		ApiBillNo:      transferId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		Uid:            mb.Uid,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      ts.UnixMilli(),
		UpdatedAt:      ts.UnixMilli(),
		PlayerName:     mb.Username,
		GameType:       gameTypeSlot,
		ApiType:        helper.JDBSpribe,
		ApiName:        "JDB Spribe",
	}
	gm.BetAmount = req.Amount
	gm.Flag = 0
	gm.BetTime = ts.UnixMilli()
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 3)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

/*
{"action":10,"ts":1664521584150,"transferId":251917,"uid":"testpl","currency":"RM","amount":5285,"refTransferIds":[252925],"gameRoundSeqNo":2050123475,"gameSeqNo":5250227089174,"gType":18,"mType":18024,"reportDate":"30-09-2022","gameDate":"30-09-2022 15:06:23","lastModifyTime":"30-09-2022 15:06:23","win":300,"bet":300,"validBet":285,"netWin":285,"tax":15,"sessionNo":"6bab11891b3634afb6a2e6d8a9821f98646387a5"}
*/
//jdbSettle 结算
func jdbSettle(body []byte, ts time.Time) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}
	req := JDBSettle{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	pid := strings.Replace(req.Uid, "fullwinabc", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(pid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	ex := g.Ex{
		"bill_no":   req.GameRoundSeqNo,
		"cash_type": helper.TransactionPayout,
	}
	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.Balance, _ = balance.Truncate(3).Float64()
		return rsp
	}

	netAmount := decimal.NewFromFloat(req.NetWin)
	winAmount := decimal.NewFromFloat(req.Win)
	validBetAmount := decimal.NewFromFloat(req.ValidBet)
	gameRoundSeqNo := fmt.Sprintf("%d", req.GameRoundSeqNo)

	var (
		refTransferIds []string
		rowIDs         []string
		rowID          string
	)
	for k, v := range req.RefTransferIds {
		if k == 0 {
			rowID = fmt.Sprintf("jdb%d%d", req.GameRoundSeqNo, v)
		} else {
			rowIDs = append(rowIDs, fmt.Sprintf("jdb%d%d", req.GameRoundSeqNo, v))
		}
		refTransferIds = append(refTransferIds, fmt.Sprintf("%d", v))
	}
	//投注

	walletSettleTransactions(mb, gameRoundSeqNo, helper.JDBSpribe, refTransferIds, balance, winAmount.Abs(), ts)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Balance, _ = balance.Truncate(3).Float64()
			return rsp
		}

		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	gm := g.Record{
		"flag":             "1",
		"valid_bet_amount": validBetAmount.String(),
		"net_amount":       netAmount.String(),
		"settle_time":      ts.UnixMilli(),
		"updated_at":       ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	if len(rowIDs) > 0 {
		query, _, _ = dialect.Update("tbl_game_record").Set(g.Record{"flag": "1"}).Where(g.Ex{"row_id": rowIDs}).ToSQL()
		myredis.AddLogToRedis(query)
	}

	balance, _ = myUserHelp.GetBalance(mb, 3)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

/*
{"action":11,"ts":1664522589182,"transferId":251919,"uid":"testpl","currency":"RM","amount":5000,"refTransferIds":[251916],"gameRoundSeqNo":1975826710}
*/
//jdbCancelBet 取消投注
func jdbCancelBet(body []byte, ts time.Time) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}
	req := JDBCancelBet{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	pid := strings.Replace(req.Uid, "fullwinabc", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(pid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	ex := g.Ex{
		"bill_no":   req.GameRoundSeqNo,
		"cash_type": helper.TransactionBetCancel,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.Balance, _ = balance.Truncate(3).Float64()
		return rsp
	}

	amount := decimal.NewFromFloat(req.Amount)
	gameRoundSeqNo := fmt.Sprintf("%d", req.GameRoundSeqNo)
	var (
		refTransferIds []string
		rowIDs         []string
	)
	for _, v := range req.RefTransferIds {
		rowIDs = append(rowIDs, fmt.Sprintf("jdb%d%d", req.GameRoundSeqNo, v))
		refTransferIds = append(refTransferIds, fmt.Sprintf("%d", v))
	}
	//取消投注
	walletCancelTransactions(mb, gameRoundSeqNo, helper.JDBSpribe, refTransferIds, balance, amount.Abs(), ts)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Balance, _ = balance.Truncate(3).Float64()
			return rsp
		}

		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	gm := g.Record{
		"flag":             "2",
		"valid_bet_amount": "0",
		"net_amount":       "0",
		"updated_at":       ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowIDs}).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 3)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

// 投付
func jdbBetNSettle(body []byte, gType int, ts time.Time) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}

	var (
		betAmount  decimal.Decimal
		winAmount  decimal.Decimal
		netAmount  decimal.Decimal
		mType      string
		transferId string
		gameSeqNo  string
		username   string
		//gameDate       string
		//lastModifyTime string
		pid          string
		platformName string
		gameType     string
		ipAddress    string
	)
	switch gType {
	case 0: //slot
		req := JDBBetNSettleSlot{}
		err := helper.JsonUnmarshal(body, &req)
		if err != nil {
			rsp.Status = "9999"
			rsp.ErrText = "Failed"
			return rsp
		}

		betAmount = decimal.NewFromFloat(req.Bet)
		winAmount = decimal.NewFromFloat(req.Win)
		netAmount = decimal.NewFromFloat(req.NetWin)
		mType = fmt.Sprintf("%d", req.MType)
		transferId = fmt.Sprintf("%d", req.TransferId)
		gameSeqNo = fmt.Sprintf("%d", req.GameSeqNo)
		username = req.Uid
		//gameDate = req.GameDate
		//lastModifyTime = req.LastModifyTime
		ipAddress = req.IpAddress
		pid = helper.JDBSlot
		platformName = "JDBSlot"
		gameType = "3"
	case 7: //fish
		req := JDBBetNSettleFish{}
		err := helper.JsonUnmarshal(body, &req)
		if err != nil {
			rsp.Status = "9999"
			rsp.ErrText = "Failed"
			return rsp
		}

		betAmount = decimal.NewFromFloat(req.Bet)
		winAmount = decimal.NewFromFloat(req.Win)
		netAmount = decimal.NewFromFloat(req.NetWin)
		mType = fmt.Sprintf("%d", req.MType)
		transferId = fmt.Sprintf("%d", req.TransferId)
		gameSeqNo = fmt.Sprintf("%d", req.GameSeqNo)
		username = req.Uid
		//gameDate = req.GameDate
		//lastModifyTime = req.LastModifyTime
		ipAddress = req.IpAddress
		pid = helper.JDBFISH
		platformName = "JDBFish"
		gameType = "2"
	case 9: //arcade
		req := JDBBetNSettleArcade{}
		err := helper.JsonUnmarshal(body, &req)
		if err != nil {
			rsp.Status = "9999"
			rsp.ErrText = "Failed"
			return rsp
		}

		betAmount = decimal.NewFromFloat(req.Bet)
		winAmount = decimal.NewFromFloat(req.Win)
		netAmount = decimal.NewFromFloat(req.NetWin)
		mType = fmt.Sprintf("%d", req.MType)
		transferId = fmt.Sprintf("%d", req.TransferId)
		gameSeqNo = fmt.Sprintf("%d", req.GameSeqNo)
		username = req.Uid
		//gameDate = req.GameDate
		//lastModifyTime = req.LastModifyTime
		ipAddress = req.IpAddress
	case 12: //彩票
		req := JDBBetNSettleLottery{}
		err := helper.JsonUnmarshal(body, &req)
		if err != nil {
			rsp.Status = "9999"
			rsp.ErrText = "Failed"
			return rsp
		}

		betAmount = decimal.NewFromFloat(req.Bet)
		winAmount = decimal.NewFromFloat(req.Win)
		netAmount = decimal.NewFromFloat(req.NetWin)
		mType = fmt.Sprintf("%d", req.MType)
		transferId = fmt.Sprintf("%d", req.TransferId)
		gameSeqNo = fmt.Sprintf("%d", req.GameSeqNo)
		username = req.Uid
		//gameDate = req.GameDate
		//lastModifyTime = req.LastModifyTime
		ipAddress = req.IpAddress
	default:
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	rowID := "jdb" + transferId
	uid := strings.Replace(username, "fullwinabc", "", -1)

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	if balance.LessThan(betAmount.Abs()) {
		rsp.Status = "6006"
		rsp.ErrText = "Your Cash Balance not enough."
		return rsp
	}

	_, _, _, err := walletBetStatus(transferId, gameSeqNo, pid, "bet")
	if err != nil && err != sql.ErrNoRows {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	if err == nil {
		rsp.Balance, _ = balance.Float64()
		rsp.Status = "0000"
		return rsp
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	//投注
	winMoney, validBetAmount, _, err := walletBetNSettleTransaction(mb, transferId, gameSeqNo, pid, balance, betAmount.Abs(), winAmount, ts)
	if err != nil {
		if err.Error() == helper.BalanceErr {
			rsp.Status = "6006"
			rsp.ErrText = "Your Cash Balance not enough."
			return rsp
		}

		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Balance, _ = balance.Float64()
			rsp.Status = "0000"
			return rsp
		}

		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	gm := GameRecord{
		BillNo:         transferId,
		Result:         "Hand number:" + gameSeqNo + "|ip:" + ipAddress,
		GameName:       JDBGmap[mType],
		GameCode:       mType,
		PlayType:       mType,
		RowId:          rowID,
		ApiBillNo:      transferId,
		MainBillNo:     "",
		HandicapType:   "",
		Handicap:       "",
		Odds:           0.00,
		StartTime:      0,
		Resettle:       0,
		Presettle:      0,
		ParentName:     mb.ParentName,
		ParentUid:      mb.ParentID,
		TopName:        mb.TopName,
		TopUid:         mb.TopID,
		GrandName:      mb.GrandName,
		GrandID:        mb.GrandID,
		GreatGrandName: mb.GreatGrandName,
		GreatGrandID:   mb.GreatGrandID,
		Uid:            mb.Uid,
		Name:           mb.Username,
		Prefix:         meta.Prefix,
		CreatedAt:      ts.UnixMilli(),
		UpdatedAt:      ts.UnixMilli(),
		PlayerName:     meta.Prefix + mb.Username,
		GameType:       gameType,
		ApiType:        pid,
		ApiName:        platformName,
		BetTime:        ts.UnixMilli(),
		SettleTime:     ts.UnixMilli(),
		SettleAmount:   winMoney,
	}
	gm.BetAmount, _ = betAmount.Abs().Float64()
	gm.ValidBetAmount = validBetAmount
	gm.NetAmount, _ = netAmount.Float64()
	gm.Flag = 1
	query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	myredis.AddLogToRedis(query)
	err = tx.Commit()
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	rsp.Balance, _ = balance.Float64()
	rsp.Status = "0000"
	return rsp
}

/*
{"action":4,"ts":1447452951820,"transferId":"123456789","uid":"testpl01"}
*/
//jdbCancelBetNSettle 取消投付
func jdbCancelBetNSettle(body []byte, ts time.Time) JDBResponse {

	rsp := JDBResponse{
		Status: "0000",
	}
	req := JDBCancelBetNSettle{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}
	uid := strings.Replace(req.Uid, "fullwinabc", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.Status = "7603"
		rsp.ErrText = "Invalid username."
		return rsp
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	ex := g.Ex{
		"bill_no": req.TransferId,
	}
	tnxs, err := walletTransactions(ex)
	if err != nil || len(tnxs) != 2 {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	betAmount, _ := decimal.NewFromString(tnxs[0].Amount)
	settleAmount, _ := decimal.NewFromString(tnxs[1].Amount)
	pid := tnxs[0].PlatformID

	winAmount := settleAmount.Sub(betAmount)
	cashType := helper.TransactionBetCancel
	if winAmount.GreaterThan(zero) {
		cashType = helper.TransactionSettledBetCancel
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	err = walletBetNSettleCancelTransaction(mb, []string{}, req.TransferId, "", pid, "void", winAmount.Abs(), cashType, ts.UnixMilli())
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.Balance, _ = balance.Float64()
			rsp.Status = "0000"
			return rsp
		}

		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	rowID := "jdb" + req.TransferId
	gm := g.Record{
		"flag":             "2",
		"valid_bet_amount": "0",
		"net_amount":       "0",
		"updated_at":       ts.UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	err = tx.Commit()
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	balance, err = myUserHelp.GetBalance(mb, 2)
	if err != nil {
		rsp.Status = "9999"
		rsp.ErrText = "Failed"
		return rsp
	}

	rsp.Balance, _ = balance.Float64()
	rsp.Status = "0000"
	return rsp
}
