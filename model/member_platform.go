package model

import (
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strconv"
)

func PlatToMap(m MemberPlatform) map[string]interface{} {

	data := map[string]interface{}{
		"id":         m.ID,
		"username":   m.Username,
		"password":   m.Password,
		"pid":        m.Pid,
		"balance":    m.Balance,
		"state":      m.State,
		"created_at": m.CreatedAt,
	}

	return data
}

func MemberPlatformInsert(param map[string]string) (MemberPlatform, error) {

	mp := MemberPlatform{
		ID:       param["id"],
		Pid:      param["pid"],
		Balance:  "0.0000",
		Username: param["username"],
		Password: param["password"],
		State:    1,
	}

	createAt, _ := strconv.ParseInt(param["s"], 10, 64)
	mp.CreatedAt = uint32(createAt)
	query, _, _ := dialect.Insert("tbl_member_platform").Rows(&mp).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return mp, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	_ = MemberPlatformUpdateCache(param["username"], param["pid"])

	return mp, nil
}

// 更新redis 用户信息
func MemberPlatformUpdateCache(username, pid string) error {

	mp := MemberPlatform{}

	ex := g.Ex{
		"username": username,
		"pid":      pid,
	}
	t := dialect.From("tbl_member_platform")
	query, _, _ := t.Select(colsMemberPlatform...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&mp, query)
	if err != nil {
		return pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	pipe := meta.MerchantRedis.TxPipeline()
	defer pipe.Close()

	key := fmt.Sprintf("m:plat:%s:%s", username, pid)
	pipe.Unlink(Ctx, key)
	pipe.HMSet(Ctx, key, PlatToMap(mp))
	pipe.Persist(Ctx, key)
	_, err = pipe.Exec(Ctx)
	if err != nil {
		return pushError(err, helper.RedisErr)
	}

	return nil
}

// 更新redis 用户信息
func MemberPlatformUpdate(username, pid string, record g.Record) error {

	ex := g.Ex{
		"username": username,
		"pid":      pid,
	}
	query, _, _ := dialect.Update("tbl_member_platform").Set(record).Where(ex).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	_ = MemberPlatformUpdateCache(username, pid)

	return nil
}

func BalanceIsEnough(mb myUserHelp.TblMemberBase, amount decimal.Decimal) (decimal.Decimal, error) {

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		return balance, err
	}

	if balance.Sub(amount).IsNegative() {
		return balance, errors.New(helper.LackOfBalance)
	}

	return balance, nil
}
