package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
)

type Plat_t interface {
	//场馆注册
	Reg(args map[string]string) error
	//场馆启动
	Launch(args map[string]string) (string, error)
}

var (
	evo    plat_evo_t
	vivo   plat_vivo_t
	pp     plat_pp_t
	jili   plat_jili_t
	tada   plat_tada_t
	jdb    plat_jdb_t
	pg     plat_pg_t
	ibc    plat_ibc_t
	pb     plat_pb_t
	bti    plat_bti_t
	oneApi plat_one_t
)
var Plats = map[string]Plat_t{
	// 真人
	helper.EVO:      evo,  //EVO真人
	helper.VIVO:     vivo, //VIVO真人
	helper.PPCasino: pp,   //PP真人
	// 捕鱼
	helper.JILIFISH: jili, //JILI捕鱼
	helper.JDBFISH:  jdb,  //JDB捕鱼
	// 电子
	helper.JILI:    jili,   //JILI电子
	helper.TADA:    tada,   //JILI电子
	helper.PP:      pp,     //PP电子
	helper.PG:      pg,     //PG电子
	helper.JDBSlot: jdb,    //JDB电子
	helper.OnePG:   oneApi, //one api pg
	// 小游戏
	helper.JDBSpribe: jdb, //JDB SPRIBE

	// 体育
	helper.IBCSB: ibc, //SABA体育
	helper.VIVER: pp,  //VIVER体育
	helper.PPSB:  pp,  //PP虚拟体育
	helper.BTI:   bti, //BTI体育
	helper.PB:    pb,  //平博体育
	// 棋牌
	helper.JILIPoker: jili, //JILI棋牌
}

func PlatIsExists(username, pid string) (MemberPlatform, error) {

	ex := g.Ex{
		"username": username,
		"pid":      pid,
	}
	m := MemberPlatform{}
	query, _, _ := dialect.From("tbl_member_platform").Select(colsMemberPlatform...).Where(ex).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return m, nil
}

func PlatformRedis(pid string) (PlatJson, error) {

	plat := PlatJson{}
	k := fmt.Sprintf("plat:%s", pid)
	cmd := meta.MerchantRedis.Get(Ctx, k)
	fmt.Println(cmd.String())
	res, err := cmd.Result()
	if err == redis.Nil || err != nil {
		return plat, err
	}

	err = json.Unmarshal([]byte(res), &plat)
	if err != nil {
		return plat, err
	}

	return plat, nil
}
