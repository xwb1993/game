package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"time"
)

type plat_vivo_t struct{}

func (plat_vivo_t) Reg(args map[string]string) error {
	return nil
}

// 场馆启动
func (plat_vivo_t) Launch(args map[string]string) (string, error) {

	launchURL := fmt.Sprintf("%s/?token=%s&operatorID=%s&serverid=%s", meta.PlatCfg.VIVO.API, args["puid"], meta.PlatCfg.VIVO.OperatorID, meta.PlatCfg.VIVO.ServerID)
	gameCode, ok := args["gamecode"]
	if ok {
		directGame := fmt.Sprintf("&application=%s&tableID=%s", vivoGameApplication[gameCode], gameCode)
		launchURL += directGame
	} else {
		launchURL += "&application=lobby"
	}

	PushLog(nil, args["username"], "VIVO", launchURL, 200, nil, nil)
	return launchURL, nil
}

func VIVOAuthenticate(ctx *fasthttp.RequestCtx) VIVOAuthenticateRsp {

	token := string(ctx.QueryArgs().Peek("token"))
	hash := string(ctx.QueryArgs().Peek("hash"))
	rsp := VIVOAuthenticateRsp{
		REQUEST: VIVOAuthenticateReq{
			TOKEN: token,
			HASH:  hash,
		},
		TIME: ctx.Time().Format("Jan 2006 15:04:05"),
	}

	uid := myUserHelp.GetUidFromToken(token)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(uid)
	if !ret {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "400",
		}
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.RESPONSE = VIVOAuthenticateSuccessRsp{
		RESULT:        "OK",
		USERID:        mb.Pid,
		USERNAME:      meta.Prefix + mb.Username,
		FIRSTNAME:     meta.Prefix,
		LASTNAME:      mb.Username,
		EMAIL:         fmt.Sprintf("%s@%s.com", mb.Username, meta.Prefix),
		CURRENCY:      meta.PlatCfg.VIVO.Currency,
		BALANCE:       balance.String(),
		GAMESESSIONID: meta.Prefix + mb.Pid,
	}
	return rsp
}

func VIVOChangeBalance(ctx *fasthttp.RequestCtx) VIVOChangeBalanceRsp {

	//userId=12345
	userId := string(ctx.QueryArgs().Peek("userId"))
	//Amount=75.00
	sAmount := string(ctx.QueryArgs().Peek("Amount"))
	//TransactionID=12344546
	transactionID := string(ctx.QueryArgs().Peek("TransactionID"))
	//TrnType=BET|WIN|CANCELED_BET
	trnType := string(ctx.QueryArgs().Peek("TrnType"))
	//TrnDescription=GameRound:TableID=2
	trnDescription := string(ctx.QueryArgs().Peek("TrnDescription"))
	//roundId=12345
	roundId := string(ctx.QueryArgs().Peek("roundId"))
	//gameId=5
	gameId := string(ctx.QueryArgs().Peek("gameId"))
	//History=127,15;129,50
	history := string(ctx.QueryArgs().Peek("History"))
	//isRoundFinished=false
	isRoundFinished := string(ctx.QueryArgs().Peek("isRoundFinished"))
	//hash=4b3j2n4n533k234j5k4jnn3h4k3k23
	hash := string(ctx.QueryArgs().Peek("hash"))
	//sessionId=785df376123dfg
	//sessionId := string(ctx.QueryArgs().Peek("sessionId"))
	rsp := VIVOChangeBalanceRsp{
		REQUEST: VIVOChangeBalanceReq{
			USERID:          userId,
			AMOUNT:          sAmount,
			TRANSACTIONID:   transactionID,
			TRNTYPE:         trnType,
			TRNDESCRIPTION:  trnDescription,
			ROUNDID:         roundId,
			GAMEID:          gameId,
			HISTORY:         history,
			ISROUNDFINISHED: isRoundFinished,
			HASH:            hash,
		},
		TIME: ctx.Time().Format("Jan 2006 15:04:05"),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "300",
		}
		return rsp
	}

	amount, err := decimal.NewFromString(sAmount)
	if err != nil {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "300",
		}
		return rsp
	}

	var (
		cashType  int
		trxID     = fmt.Sprintf("%s|%s", mb.Uid, transactionID)
		betAmount decimal.Decimal
	)
	switch trnType {
	case "BET":
		cashType = helper.TransactionBet
	case "WIN":
		cashType = helper.TransactionPayout
	case "CANCELED_BET":
		cashType = helper.TransactionBetCancel
	default:
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "300",
		}
		return rsp
	}

	// 派彩和取消注单，必须有投注注单
	if cashType == helper.TransactionPayout || cashType == helper.TransactionBetCancel {
		ex := g.Ex{
			"bill_no":   roundId,
			"cash_type": helper.TransactionBet,
		}

		trxBet, bTransExsit := walletTransactionFindOne(ex)
		if !bTransExsit {
			rsp.RESPONSE = VIVOFailedRsp{
				RESULT: "FAILED",
				CODE:   "300",
			}
			return rsp
		}

		betAmount, _ = decimal.NewFromString(trxBet.Amount)
	}
	ex := g.Ex{
		"bill_no":      roundId,
		"operation_no": trxID,
		"cash_type":    cashType,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)

	// 投注/派彩/取消注单，重复执行，返回成功
	if bTransExsit {
		balance, _ := myUserHelp.GetBalance(mb, 2)
		rsp.RESPONSE = VIVOChangeSuccessRsp{
			RESULT:                "OK",
			ECSYSTEMTRANSACTIONID: trx.ID,
			BALANCE:               balance.String(),
		}
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	var (
		id    string
		query string
	)

	var trans memberTransaction
	switch cashType {
	case helper.TransactionBet: // 投注
		trans = walletBetTransaction(mb, roundId, trxID, helper.VIVO, balance, amount.Abs(), ctx.Time())
	case helper.TransactionPayout: //派彩
		trans = walletSettleTransaction(mb, roundId, trxID, helper.VIVO, balance, amount.Abs(), ctx.Time())
	case helper.TransactionBetCancel: //取消注单
		trans = walletCancelTransaction(mb, roundId, trxID, helper.VIVO, balance, amount.Abs(), ctx.Time())
	}

	SetTransactionToRedis(ex, trans)
	rowID := fmt.Sprintf("vivo%s|%s", mb.Uid, roundId)
	switch cashType {
	case helper.TransactionBet: // 投注
		gm := GameRecord{
			BillNo:         trxID,
			Result:         "trn Description:" + trnDescription,
			GameName:       vivoGameName[gameId],
			GameCode:       gameId,
			PlayType:       gameId,
			RowId:          rowID,
			ApiBillNo:      trxID,
			MainBillNo:     "",
			HandicapType:   "",
			Handicap:       "",
			Odds:           0.00,
			StartTime:      0,
			Resettle:       0,
			Presettle:      0,
			TopName:        mb.TopName,
			TopUid:         mb.TopID,
			ParentName:     mb.ParentName,
			ParentUid:      mb.ParentID,
			GrandName:      mb.GrandName,
			GrandID:        mb.GrandID,
			GreatGrandName: mb.GreatGrandName,
			GreatGrandID:   mb.GreatGrandID,
			Uid:            mb.Uid,
			Name:           mb.Username,
			Prefix:         meta.Prefix,
			CreatedAt:      time.Now().UnixMilli(),
			UpdatedAt:      time.Now().UnixMilli(),
			BetTime:        time.Now().UnixMilli(),
			PlayerName:     meta.Prefix + mb.Username,
			GameType:       gameTypeCasino,
			ApiType:        helper.VIVO,
			ApiName:        "VIVO",
			ValidBetAmount: 0,
			NetAmount:      0,
		}
		gm.BetAmount, _ = amount.Abs().Float64()
		query, _, _ = dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
	case helper.TransactionPayout: //派彩
		gm := g.Record{
			"flag":             "1",
			"valid_bet_amount": betAmount.String(),
			"net_amount":       "0",
			"updated_at":       time.Now().UnixMilli(),
			"settle_time":      time.Now().UnixMilli(),
		}
		if amount.GreaterThan(betAmount) {
			gm["net_amount"] = amount.Sub(betAmount).String()
		}
		query, _, _ = dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	case helper.TransactionBetCancel: //取消注单
		gm := g.Record{
			"flag":             "2",
			"valid_bet_amount": "0",
			"net_amount":       "0",
			"updated_at":       time.Now().UnixMilli(),
		}
		query, _, _ = dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	}

	myredis.AddLogToRedis(query)
	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.RESPONSE = VIVOChangeSuccessRsp{
		RESULT:                "OK",
		ECSYSTEMTRANSACTIONID: id,
		BALANCE:               balance.String(),
	}

	return rsp
}

func VIVOStatus(ctx *fasthttp.RequestCtx) VIVOStatusRsp {

	//userId=12345
	userId := string(ctx.QueryArgs().Peek("userId"))
	//casinoTransactionId =12344546
	casinoTransactionId := string(ctx.QueryArgs().Peek("casinoTransactionId"))
	//hash=8cb54b1924dbbd626a3b079a47527d17
	hash := string(ctx.QueryArgs().Peek("hash"))
	rsp := VIVOStatusRsp{
		REQUEST: VIVOStatusReq{
			USERID:              userId,
			CASINOTRANSACTIONID: casinoTransactionId,
			HASH:                hash,
		},
		TIME: ctx.Time().Format("Jan 2006 15:04:05"),
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "302",
		}
		return rsp
	}

	trxID := fmt.Sprintf("%s|%s", mb.Uid, casinoTransactionId)
	ex := g.Ex{
		"operation_no": trxID,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "302",
		}
		return rsp
	}

	rsp.RESPONSE = VIVOStatusSuccessRsp{
		RESULT:                "OK",
		ECSYSTEMTRANSACTIONID: trx.ID,
	}
	return rsp
}

func VIVOGetBalance(ctx *fasthttp.RequestCtx) VIVOGetBalanceRsp {

	//userId=12345
	userId := string(ctx.QueryArgs().Peek("userId"))
	//hash=8cb54b1924dbbd626a3b079a47527d17
	hash := string(ctx.QueryArgs().Peek("hash"))
	rsp := VIVOGetBalanceRsp{
		REQUEST: VIVOGetBalanceReq{
			USERID: userId,
			HASH:   hash,
		},
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(userId)
	if !ret {
		rsp.RESPONSE = VIVOFailedRsp{
			RESULT: "FAILED",
			CODE:   "302",
		}
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.RESPONSE = VIVOGetBalanceSuccessRsp{
		RESULT:  "OK",
		BALANCE: balance.String(),
	}
	return rsp
}
