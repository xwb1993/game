package model

import "game/contrib/helper"

var (
	ppPlatform = map[string]string{
		helper.PP:       "PPSLOT",
		helper.PPCasino: "PPCasino",
		helper.PPSB:     "VIVER",
		helper.VIVER:    "VIVER",
	}
)

var (
	ppGameName = map[string]string{
		"1001":               "Dragon Tiger",
		"101":                "Live Casino Lobby",
		"102":                "Roulette Lobby",
		"1024":               "Andar Bahar",
		"103":                "Blackjack Lobby",
		"104":                "Baccarat Lobby",
		"105":                "Gameshows",
		"107":                "Sic Bo",
		"108":                "Dragon Tiger",
		"109":                "Sic Bo & Dragon Tiger",
		"110":                "D&W",
		"1101":               "Sweet Bonanza CandyLand",
		"111":                "Other Promos",
		"114":                "Asian Games",
		"1401":               "Boom City",
		"201":                "Roulette 2",
		"203":                "Speed Roulette 1",
		"204":                "Mega Roulette",
		"225":                "Auto-Roulette 1",
		"229":                "Roulette 8 - Indian",
		"230":                "Roulette 10 - Ruby",
		"240":                "PowerUP Roulette",
		"401":                "Baccarat 1",
		"402":                "Speed Baccarat 1",
		"545":                "Roulette 9 - The Club",
		"701":                "Mega Sic Bo",
		"801":                "Mega Wheel",
		"901":                "ONE Blackjack",
		"902":                "BetVictor ONE Blackjack",
		"903":                "ONE Blackjack 3 - Dutch",
		"bca":                "Baccarat",
		"bjma":               "Multihand Blackjack",
		"bjmb":               "American Blackjack",
		"bnadvanced":         "Dragon Bonus Baccarat",
		"bndt":               "Dragon Tiger",
		"cs3irishcharms":     "Irish Charms",
		"cs3w":               "Diamonds are Forever 3 Lines",
		"cs5moneyroll":       "Money Roll",
		"cs5triple8gold":     "888 Gold",
		"rla":                "Roulette",
		"sc7piggies":         "7 Piggies 5:000",
		"sc7piggiesai":       "7 Piggies 25:000",
		"scdiamond":          "Diamond Strike 100:000",
		"scdiamondai":        "Diamond Strike 250:000",
		"scgoldrush":         "Gold Rush 250:000",
		"scgoldrushai":       "Gold Rush 500:000",
		"scpanda":            "Panda Gold 10:000",
		"scpandai":           "Panda Gold 50:000",
		"scqog":              "Queen of Gold 100:000",
		"scqogai":            "Queen of Gold 100:000",
		"scsafari":           "Hot Safari 50:000",
		"scsafariai":         "Hot Safari 75:000",
		"scwolfgold":         "Wolf Gold 1 Million",
		"scwolfgoldai":       "Wolf Gold 1:000:000",
		"vs100firehot":       "Fire Hot 100",
		"vs100sh":            "Shining Hot 100",
		"vs1024butterfly":    "Jade Butterfly",
		"vs1024dtiger":       "The Dragon Tiger",
		"vs1024gmayhem":      "Gorilla Mayhem",
		"vs1024lionsd":       "5 Lions Dance",
		"vs1024mahjpanda":    "Mahjong Panda",
		"vs1024moonsh":       "Moonshot",
		"vs1024temuj":        "Temujin Treasures",
		"vs10amm":            "Amazing Money Machine",
		"vs10bbbonanza":      "Big Bass Bonanza",
		"vs10bbextreme":      "Big Bass Amazon Xtreme",
		"vs10bbhas":          "Big Bass - Hold & Spinner",
		"vs10bbkir":          "Big Bass Bonanza - Keeping it Reel",
		"vs10bblpop":         "Bubble Pop",
		"vs10bookazteck":     "Book of Aztec King",
		"vs10bookfallen":     "Book of Fallen",
		"vs10bookoftut":      "Book of Tut",
		"vs10bookviking":     "Book of Vikings",
		"vs10bxmasbnza":      "Christmas Big Bass Bonanza",
		"vs10chkchase":       "Chicken Chase",
		"vs10cowgold":        "Cowboys Gold",
		"vs10crownfire":      "Crown of Fire",
		"vs10egrich":         "Queen of Gods",
		"vs10egypt":          "Ancient Egypt",
		"vs10egyptcls":       "Ancient Egypt Classic",
		"vs10eyestorm":       "Eye of the Storm",
		"vs10fdrasbf":        "Floating Dragon - Dragon Boat Festival",
		"vs10firestrike":     "Fire Strike",
		"vs10firestrike2":    "Fire Strike 2",
		"vs10fisheye":        "Fish Eye",
		"vs10floatdrg":       "Floating Dragon",
		"vs10fruity2":        "Extra Juicy",
		"vs10gizagods":       "Gods of Giza",
		"vs10goldfish":       "Fishin Reels",
		"vs10jnmntzma":       "Jane Hunter and the Mask of Montezuma",
		"vs10kingofdth":      "Kingdom of the Dead",
		"vs10luckcharm":      "Lucky: Grace & Charm",
		"vs10madame":         "Madame Destiny",
		"vs10mayangods":      "John Hunter And The Mayan Gods",
		"vs10mmm":            "Magic Money Maze",
		"vs10nudgeit":        "Rise of Giza PowerNudge",
		"vs10powerlines":     "Peak Power",
		"vs10returndead":     "Return of the Dead",
		"vs10runes":          "Gates of Valhalla",
		"vs10snakeeyes":      "Snakes & Ladders - Snake Eyes",
		"vs10snakeladd":      "Snakes and Ladders Megadice",
		"vs10spiritadv":      "Spirit of Adventure",
		"vs10starpirate":     "Star Pirates Code",
		"vs10threestar":      "Three Star Fortune",
		"vs10tictac":         "Tic Tac Take",
		"vs10tut":            "Book Of Tut Respin",
		"vs10txbigbass":      "Big Bass Splash",
		"vs10vampwolf":       "Vampires vs Wolves",
		"vs10wildtut":        "Mysterious Egypt",
		"vs117649starz":      "Starz Megaways",
		"vs12bbb":            "Bigger Bass Bonanza",
		"vs12bbbxmas":        "Bigger Bass Blizzard - Christmas Catch",
		"vs12tropicana":      "Club Tropicana",
		"vs13g":              "Devil's 13",
		"vs15b":              "Crazy 7s",
		"vs15diamond":        "Diamond Strike",
		"vs15fairytale":      "Fairytale Fortune",
		"vs15godsofwar":      "Zeus vs Hades - Gods of War",
		"vs15ktv":            "KTV",
		"vs1600drago":        "Drago - Jewels of Fortune",
		"vs18mashang":        "Treasure Horse",
		"vs1ball":            "Lucky Dragon Ball",
		"vs1dragon8":         "888 Dragons",
		"vs1fortunetree":     "Tree of Riches",
		"vs1fufufu":          "Fu Fu Fu",
		"vs1masterjoker":     "Master Joker",
		"vs1money":           "Money Money Money",
		"vs1tigers":          "Triple Tigers",
		"vs20aladdinsorc":    "Aladdin and the Sorcerer",
		"vs20amuleteg":       "Fortune of Giza",
		"vs20asgard":         "Kingdom of Asgard",
		"vs20aztecgates":     "Gates of Aztec",
		"vs20bchprty":        "Wild Beach Party",
		"vs20beefed":         "Fat Panda",
		"vs20bermuda":        "Bermuda Riches",
		"vs20bl":             "Busy Bees",
		"vs20cashmachine":    "Cash Box",
		"vs20chickdrop":      "Chicken Drop",
		"vs20chicken":        "The Great Chicken Escape",
		"vs20cleocatra":      "Cleocatra",
		"vs20clspwrndg":      "Sweet Powernudge",
		"vs20clustwild":      "Sticky Bees",
		"vs20cms":            "Sugar Rush Summer Time",
		"vs20cmv":            "Sugar Rush Valentine's Day",
		"vs20colcashzone":    "Colossal Cash Zone",
		"vs20cw":             "Sugar Rush Winter",
		"vs20daydead":        "Day of Dead",
		"vs20doghouse":       "The Dog House",
		"vs20doghousemh":     "The Dog House Multihold",
		"vs20drgbless":       "Dragon Hero",
		"vs20drtgold":        "Drill That Gold",
		"vs20dugems":         "Hot Pepper",
		"vs20egypttrs":       "Egyptian Fortunes",
		"vs20eightdragons":   "8 Dragons",
		"vs20eking":          "Emerald King",
		"vs20ekingrr":        "Emerald King Rainbow Road",
		"vs20emptybank":      "Empty the Bank",
		"vs20excalibur":      "Excalibur Unleashed",
		"vs20farmfest":       "Barn Festival",
		"vs20fh":             "Fire Hot 20",
		"vs20fparty2":        "Fruit Party 2",
		"vs20framazon":       "Fruits of the Amazon",
		"vs20fruitparty":     "Fruit Party",
		"vs20fruitsw":        "Sweet Bonanza",
		"vs20gatotfury":      "Gatot Kaca's Fury",
		"vs20gatotgates":     "Gates of Gatot Kaca",
		"vs20gg":             "Spooky Fortune",
		"vs20gobnudge":       "Goblin Heist Powernudge",
		"vs20godiva":         "Lady Godiva",
		"vs20goldclust":      "Rabbit Garden",
		"vs20goldfever":      "Gems Bonanza",
		"vs20gorilla":        "Jungle Gorilla",
		"vs20hburnhs":        "Hot to Burn Hold and Spin",
		"vs20hercpeg":        "Hercules and Pegasus",
		"vs20hockey":         "Hockey League",
		"vs20honey":          "Honey Honey Honey",
		"vs20hotzone":        "African Elephant",
		"vs20hstgldngt":      "Heist for the Golden Nuggets",
		"vs20jewelparty":     "Jewel Rush",
		"vs20kraken":         "Release the Kraken",
		"vs20kraken2":        "Release the Kraken 2",
		"vs20lampinf":        "Lamp Of Infinity",
		"vs20lcount":         "Gems of Serengeti",
		"vs20leprechaun":     "Leprechaun Song",
		"vs20leprexmas":      "Leprechaun Carol",
		"vs20lobcrab":        "Lobster Bob's Crazy Crab Shack",
		"vs20ltng":           "Pinup Girls",
		"vs20magicpot":       "The Magic Cauldron",
		"vs20mammoth":        "Mammoth Gold Megaways",
		"vs20midas":          "The Hand of Midas",
		"vs20mochimon":       "Mochimon",
		"vs20mparty":         "Wild Hop & Drop",
		"vs20mtreasure":      "Pirate Golden Age",
		"vs20muertos":        "Muertos Multiplier Megaways",
		"vs20mustanggld2":    "Clover Gold",
		"vs20mvwild":         "Jasmine Dreams",
		"vs20mysteryst":      "Country Farming",
		"vs20octobeer":       "Octobeer Fortunes",
		"vs20olympgate":      "Gates of Olympus",
		"vs20pbonanza":       "Pyramid Bonanza",
		"vs20phoenixf":       "Phoenix Forge",
		"vs20piggybank":      "Piggy Bankers",
		"vs20pistols":        "Wild West Duels",
		"vs20porbs":          "Santa's Great Gifts",
		"vs20procount":       "Wisdom of Athena",
		"vs20rainbowg":       "Rainbow Gold",
		"vs20rhino":          "Great Rhino",
		"vs20rhinoluxe":      "Great Rhino Deluxe",
		"vs20rockvegas":      "Rock Vegas",
		"vs20santa":          "Santa",
		"vs20santawonder":    "Santa's Wonderland",
		"vs20sbxmas":         "Sweet Bonanza Xmas",
		"vs20schristmas":     "Starlight Christmas",
		"vs20sh":             "Shining Hot 20",
		"vs20sknights":       "The Knight King",
		"vs20smugcove":       "Smugglers Cove",
		"vs20sparta":         "Shield Of Sparta",
		"vs20splmystery":     "Spellbinding Mystery",
		"vs20starlight":      "Starlight Princess",
		"vs20starlightx":     "Starlight Princess 1000",
		"vs20stickysymbol":   "The Great Stick-up",
		"vs20stickywild":     "Wild Bison Charge",
		"vs20sugarrush":      "Sugar Rush",
		"vs20superlanche":    "Monster Superlanche",
		"vs20superx":         "Super X",
		"vs20swordofares":    "Sword of Ares",
		"vs20terrorv":        "Cash Elevator",
		"vs20theights":       "Towering Fortunes",
		"vs20trsbox":         "Treasure Wild",
		"vs20trswild2":       "Black Bull",
		"vs20tweethouse":     "The Tweety House",
		"vs20ultim5":         "The Ultimate 5",
		"vs20underground":    "Down the Rails",
		"vs20vegasmagic":     "Vegas Magic",
		"vs20wildboost":      "Wild Booster",
		"vs20wildparty":      "3 Buzzing Wilds",
		"vs20wildpix":        "Wild Pixies",
		"vs20wolfie":         "Greedy Wolf",
		"vs20xmascarol":      "Christmas Carol Megaways",
		"vs243caishien":      "Caishen's Cash",
		"vs243chargebull":    "Raging Bull",
		"vs243ckemp":         "Cheeky Emperor",
		"vs243dancingpar":    "Dance Party",
		"vs243discolady":     "Disco Lady",
		"vs243empcaishen":    "Emperor Caishen",
		"vs243fortseren":     "Greek Gods",
		"vs243fortune":       "Caishen's Gold",
		"vs243koipond":       "Koi Pond",
		"vs243lions":         "5 Lions",
		"vs243lionsgold":     "5 Lions Gold",
		"vs243mwarrior":      "Monkey Warrior",
		"vs243nudge4gold":    "Hellvis Wild",
		"vs243queenie":       "Queenie",
		"vs25archer":         "Fire Archer",
		"vs25asgard":         "Asgard",
		"vs25aztecking":      "Aztec King",
		"vs25bkofkngdm":      "Book Of Kingdoms",
		"vs25bomb":           "Bomb Bonanza",
		"vs25btygold":        "Bounty Gold",
		"vs25chilli":         "Chilli Heat",
		"vs25copsrobbers":    "Cash Patrol",
		"vs25davinci":        "Da Vinci's Treasure",
		"vs25dragonkingdom":  "Dragon Kingdom",
		"vs25dwarves_new":    "Dwarven Gold Deluxe",
		"vs25gladiator":      "Wild Gladiator",
		"vs25gldox":          "Golden Ox",
		"vs25goldparty":      "Gold Party",
		"vs25goldpig":        "Golden Pig",
		"vs25goldrush":       "Gold Rush",
		"vs25h":              "Fruity Blast",
		"vs25holiday":        "Holiday Ride",
		"vs25hotfiesta":      "Hot Fiesta",
		"vs25jokerking":      "Joker King",
		"vs25jokrace":        "Joker Race",
		"vs25journey":        "Journey to the West",
		"vs25kfruit":         "Aztec Blaze",
		"vs25kingdoms":       "3 Kingdoms - Battle of Red Cliffs",
		"vs25kingdomsnojp":   "3 Kingdoms - Battle of Red Cliffs",
		"vs25mmouse":         "Money Mouse",
		"vs25mustang":        "Mustang Gold",
		"vs25newyear":        "Lucky New Year",
		"vs25pandagold":      "Panda's Fortune",
		"vs25pandatemple":    "Panda Fortune 2",
		"vs25peking":         "Peking Luck",
		"vs25pyramid":        "Pyramid King",
		"vs25queenofgold":    "Queen of Gold",
		"vs25rio":            "Heart of Rio",
		"vs25rlbank":         "Reel Banks",
		"vs25romeoandjuliet": "Romeo and Juliet",
		"vs25safari":         "Hot Safari",
		"vs25samurai":        "Rise of Samurai",
		"vs25scarabqueen":    "John Hunter and the Tomb of the Scarab Queen",
		"vs25sea":            "Great Reef",
		"vs25spgldways":      "Secret City Gold",
		"vs25spotz":          "Knight Hot Spotz",
		"vs25tigerwar":       "The Tiger Warrior",
		"vs25tigeryear":      "Lucky New Year - Tiger Treasures",
		"vs25vegas":          "Vegas Nights",
		"vs25walker":         "Wild Walker",
		"vs25wildspells":     "Wild Spells",
		"vs25wolfgold":       "Wolf Gold",
		"vs25wolfjpt":        "Wolf Gold Power Jackpot",
		"vs30catz":           "The Catfather Part II",
		"vs3train":           "Gold Train",
		"vs4096bufking":      "Buffalo King",
		"vs4096magician":     "Magician's Secrets",
		"vs4096mystery":      "Mysterious",
		"vs4096robber":       "Robber Strike",
		"vs40bigjuan":        "Big Juan",
		"vs40cleoeye":        "Eye of Cleopatra",
		"vs40cosmiccash":     "Cosmic Cash",
		"vs40firehot":        "Fire Hot 40",
		"vs40frrainbow":      "Fruit Rainbow",
		"vs40hotburnx":       "Hot To Burn Extreme",
		"vs40madwheel":       "The Wild Machine",
		"vs40mstrwild":       "Happy Hooves",
		"vs40pirate":         "Pirate Gold",
		"vs40pirgold":        "Pirate Gold Deluxe",
		"vs40sh":             "Shining Hot 40",
		"vs40spartaking":     "Spartan King",
		"vs40streetracer":    "Street Racer",
		"vs40voodoo":         "Voodoo Magic",
		"vs40wanderw":        "Wild Depths",
		"vs40wildwest":       "Wild West Gold",
		"vs432congocash":     "Congo Cash",
		"vs50aladdin":        "3 Genie Wishes",
		"vs50chinesecharms":  "Lucky Dragons",
		"vs50dmdcascade":     "Diamond Cascade",
		"vs50hercules":       "Hercules Son of Zeus",
		"vs50jucier":         "Sky Bounty",
		"vs50juicyfr":        "Juicy Fruits",
		"vs50kingkong":       "Mighty Kong",
		"vs50mightra":        "Might of Ra",
		"vs50northgard":      "North Guardians",
		"vs50pixie":          "Pixie Wings",
		"vs50safariking":     "Safari King",
		"vs576hokkwolf":      "Hokkaido Wolf",
		"vs576treasures":     "Wild Wild Riches",
		"vs5aztecgems":       "Aztec Gems",
		"vs5drhs":            "Dragon Hot Hold & Spin",
		"vs5drmystery":       "Dragon Kingdom - Eyes of Fire",
		"vs5firehot":         "Fire Hot 5",
		"vs5hotburn":         "Hot to Burn",
		"vs5joker":           "Joker's Jewels",
		"vs5littlegem":       "Little Gem Hold and Spin",
		"vs5sh":              "Shining Hot 5",
		"vs5spjoker":         "Super Joker",
		"vs5strh":            "Striking Hot 5",
		"vs5super7":          "Super 7s ",
		"vs5trdragons":       "Triple Dragons",
		"vs5ultra":           "Ultra Hold and Spin",
		"vs5ultrab":          "Ultra Burn",
		"vs75bronco":         "Bronco Spirit",
		"vs75empress":        "Golden Beauty",
		"vs7776aztec":        "Aztec Bonanza",
		"vs7776secrets":      "Aztec Treasure",
		"vs7fire88":          "Fire 88",
		"vs7monkeys":         "7 Monkeys",
		"vs7pigs":            "7 Piggies",
		"vs88hockattack":     "Hockey Attack",
		"vs8magicjourn":      "Magic Journey",
		"vs9aztecgemsdx":     "Aztec Gems Deluxe",
		"vs9catz":            "The Catfather",
		"vs9chen":            "Master Chen's Fortune",
		"vs9hockey":          "Hockey League Wild Match",
		"vs9hotroll":         "Hot Chilli",
		"vs9madmonkey":       "Monkey Madness",
		"vs9outlaw":          "Pirates Pub",
		"vs9piggybank":       "Piggy Bank Bills",
		"vsprg10bbbnza":      "Big Bass Bonanza Jackpot Play",
		"vsprg10bigbass":     "Big Bass Splash Jackpot Play",
		"vsprg10booktut":     "Book of Tut Jackpot Play",
		"vsprg20doghouse":    "The Dog House Jackpot Play",
		"vsprg20fruitpty":    "Fruit Party Jackpot Play",
		"vsprg20fruitsw":     "Sweet Bonanza Jackpot Play",
		"vsprg20olympus":     "Gates of Olympus Jackpot Play",
		"vsprg20starpr":      "Starlight Princess Jackpot Play",
		"vsprg20sugarush":    "Sugar Rush Jackpot Play",
		"vsprg5joker":        "Joker's Jewels Jackpot Play",
		"vsprgwaysdogs":      "The Dog House Megaways Jackpot Play",
		"vswaysaztecking":    "Aztec King Megaways",
		"vswaysbankbonz":     "Cash Bonanza",
		"vswaysbbb":          "Big Bass Bonanza Megaways",
		"vswaysbook":         "Book of Golden Sands",
		"vswaysbufking":      "Buffalo King Megaways",
		"vswayschilheat":     "Chilli Heat Megaways",
		"vswaysconcoll":      "Firebird Spirit - Connect & Collect",
		"vswayscryscav":      "Crystal Caverns Megaways",
		"vswaysdogs":         "The Dog House Megaways",
		"vswayselements":     "Elemental Gems Megaways",
		"vswayseternity":     "Diamonds of Egypt",
		"vswaysfltdrg":       "Floating Dragon Hold & Spin Megaways",
		"vswaysfrbugs":       "Frogs & Bugs",
		"vswaysfrywld":       "Spin & Score Megaways",
		"vswaysfuryodin":     "Fury of Odin Megaways",
		"vswayshammthor":     "Power of Thor Megaways",
		"vswayshive":         "Star Bounty",
		"vswaysjkrdrop":      "Tropical Tiki",
		"vswayslight":        "Lucky Lightning",
		"vswayslions":        "5 Lions Megaways",
		"vswayslofhero":      "Legend of Heroes",
		"vswaysluckyfish":    "Lucky Fishing",
		"vswaysmadame":       "Madame Destiny Megaways",
		"vswaysmonkey":       "3 Dancing Monkeys",
		"vswaysmorient":      "Mystery of the Orient",
		"vswaysoldminer":     "Old Gold Miner Megaways",
		"vswayspizza":        "PIZZA PIZZA PIZZA",
		"vswayspowzeus":      "Power of Merlin Megaways",
		"vswaysrabbits":      "5 Rabbits Megaways",
		"vswaysredqueen":     "The Red Queen",
		"vswaysrhino":        "Great Rhino Megaways",
		"vswaysrockblst":     "Rocket Blast Megaways",
		"vswaysrsm":          "Wild Celebrity Bus Megaways",
		"vswayssamurai":      "Rise of Samurai Megaways",
		"vswaysstrwild":      "Candy Stars",
		"vswaysultrcoin":     "Cowboy Coins",
		"vswayswerewolf":     "Curse of the Werewolf Megaways",
		"vswayswest":         "Mystic Chief",
		"vswayswildwest":     "Wild West Gold Megaways",
		"vswayswwhex":        "Wild Wild Bananas",
		"vswayswwriches":     "Wild Wild Riches Megaways",
		"vswaysxjuicy":       "Extra Juicy Megaways",
		"vswaysyumyum":       "Yum Yum Powerways",
		"vswayszombcarn":     "Zombie Carnival",
		"vpdr7":              "Greyhound Racing",
		"vpdt11":             "Darts",
		"vpfh3":              "Flat Horse Racing",
		"vplfl6":             "Fantastic League Football",
		"vplobby":            "",
		"vpmr9":              "Force 1 Racing",
		"vppso4":             "Penalty Shootout",
		"vpsc10":             "Steeplechase",
	}
)

type PPLoginRsp struct {
	Error       string `json:"error"`
	Description string `json:"description"`
	GameURL     string `json:"gameURL"`
}

type PPResponse struct {
	Error       string `json:"error"`
	Description string `json:"description"`
}

type PPAuthRsp struct {
	UserId      string  `json:"userId"`
	Currency    string  `json:"currency"`
	Cash        float64 `json:"cash"`
	Bonus       float64 `json:"bonus"`
	Country     string  `json:"country"`
	Error       int     `json:"error"`
	Description string  `json:"description"`
}

type PPBalanceRsp struct {
	Currency    string  `json:"currency"`
	Cash        float64 `json:"cash"`
	Bonus       float64 `json:"bonus"`
	Error       int     `json:"error"`
	Description string  `json:"description"`
}

type PPBetRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	UsedPromo     float64 `json:"usedPromo"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPResultRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPBonusWinRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPJackpotWinRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPEndRoundRsp struct {
	Cash        float64 `json:"cash"`
	Bonus       float64 `json:"bonus"`
	Error       int     `json:"error"`
	Description string  `json:"description"`
}

type PPRefundRsp struct {
	TransactionId string `json:"transactionId"`
	Error         int    `json:"error"`
	Description   string `json:"description"`
}

type PPPromoWinRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPAdjustmentRsp struct {
	TransactionId string  `json:"transactionId"`
	Currency      string  `json:"currency"`
	Cash          float64 `json:"cash"`
	Bonus         float64 `json:"bonus"`
	Error         int     `json:"error"`
	Description   string  `json:"description"`
}

type PPInsufficientBalanceRsp struct {
	Error       int    `json:"error"`
	Description string `json:"description"`
}
