package model

import "encoding/xml"

var (
	vivoGameApplication = map[string]string{
		"1":   "roulette",
		"160": "roulette",
		"161": "roulette",
		"162": "roulette",
		"167": "roulette",
		"183": "roulette",
		"212": "blackjack",
		"222": "blackjack",
		"223": "blackjack",
		"229": "roulette",
		"230": "roulette",
		"239": "baccarat",
		"240": "baccarat",
		"241": "baccarat",
		"242": "baccarat",
		"243": "baccarat",
		"244": "roulette",
		"245": "roulette",
		"255": "Hold'em",
		"26":  "roulette",
		"271": "TeenPatti",
		"281": "DragonTiger",
		"293": "blackjack",
		"3":   "baccarat",
		"310": "roulette",
		"348": "baccarat",
		"349": "baccarat",
		"350": "baccarat",
		"351": "baccarat",
		"352": "baccarat",
		"353": "baccarat",
		"354": "baccarat",
		"355": "baccarat",
		"36":  "roulette",
		"43":  "roulette",
	}
	vivoGameName = map[string]string{
		"1":   "Bulgaria Roulette",
		"160": "Galaxy Baccarat 1",
		"161": "Galaxy Baccarat 2",
		"162": "Galaxy Baccarat 3",
		"167": "Galaxy Roulette",
		"183": "Portomaso Roulette",
		"212": "Burgas Blackjack",
		"222": "Las Vegas Blackjack",
		"223": "Blackjack",
		"229": "European Auto Roulette",
		"230": "VA Roulette",
		"239": "VA Bacarat 1",
		"240": "VA Bacarat 2",
		"241": "VA Bacarat 3",
		"242": "VA Bacarat 4",
		"243": "VA Bacarat 5",
		"244": "Oracle Blaze Roulette",
		"245": "Oracle 360 Roulette",
		"255": "Casino Hold'em",
		"26":  "Oriental Roulette",
		"271": "Teen Patti",
		"281": "Dragon Tiger",
		"293": "Limitless Blackjack",
		"3":   "Macau Baccarat",
		"310": "European Roulette",
		"348": "Las Vegas 1 Baccarat",
		"349": "Las Vegas 2 Baccarat",
		"350": "Las Vegas 3 Baccarat",
		"351": "Singapore 1 Baccarat",
		"352": "Singapore 2 Baccarat",
		"353": "Singapore 3 Baccarat",
		"354": "Montecarlo 1 Baccarat",
		"355": "Montecarlo 2 Baccarat",
		"36":  "French Roulette",
		"43":  "Las Vegas Roulette",
	}
)

type VIVOFailedRsp struct {
	Text   string `xml:",chardata"`
	RESULT string `xml:"RESULT"`
	CODE   string `xml:"CODE"`
}

type VIVOAuthenticateReq struct {
	Text  string `xml:",chardata"`
	TOKEN string `xml:"TOKEN"`
	HASH  string `xml:"HASH"`
}

type VIVOAuthenticateSuccessRsp struct {
	Text          string `xml:",chardata"`
	RESULT        string `xml:"RESULT"`
	USERID        string `xml:"USERID"`
	USERNAME      string `xml:"USERNAME"`
	FIRSTNAME     string `xml:"FIRSTNAME"`
	LASTNAME      string `xml:"LASTNAME"`
	EMAIL         string `xml:"EMAIL"`
	CURRENCY      string `xml:"CURRENCY"`
	BALANCE       string `xml:"BALANCE"`
	GAMESESSIONID string `xml:"GAMESESSIONID"`
}

type VIVOAuthenticateRsp struct {
	XMLName  xml.Name            `xml:"VGSSYSTEM"`
	Text     string              `xml:",chardata"`
	REQUEST  VIVOAuthenticateReq `xml:"REQUEST"`
	TIME     string              `xml:"TIME"`
	RESPONSE interface{}         `xml:"RESPONSE"`
}

type VIVOChangeBalanceReq struct {
	Text            string `xml:",chardata"`
	USERID          string `xml:"USERID"`
	AMOUNT          string `xml:"AMOUNT"`
	TRANSACTIONID   string `xml:"TRANSACTIONID"`
	TRNTYPE         string `xml:"TRNTYPE"`
	GAMEID          string `xml:"GAMEID"`
	ROUNDID         string `xml:"ROUNDID"`
	TRNDESCRIPTION  string `xml:"TRNDESCRIPTION"`
	HISTORY         string `xml:"HISTORY"`
	ISROUNDFINISHED string `xml:"ISROUNDFINISHED"`
	HASH            string `xml:"HASH"`
}

type VIVOChangeSuccessRsp struct {
	Text                  string `xml:",chardata"`
	RESULT                string `xml:"RESULT"`
	ECSYSTEMTRANSACTIONID string `xml:"ECSYSTEMTRANSACTIONID"`
	BALANCE               string `xml:"BALANCE"`
}

type VIVOChangeBalanceRsp struct {
	XMLName  xml.Name             `xml:"VGSSYSTEM"`
	Text     string               `xml:",chardata"`
	REQUEST  VIVOChangeBalanceReq `xml:"REQUEST"`
	TIME     string               `xml:"TIME"`
	RESPONSE interface{}          `xml:"RESPONSE"`
}

type VIVOStatusReq struct {
	Text                string `xml:",chardata"`
	USERID              string `xml:"USERID"`
	CASINOTRANSACTIONID string `xml:"CASINOTRANSACTIONID"`
	HASH                string `xml:"HASH"`
}

type VIVOStatusSuccessRsp struct {
	Text                  string `xml:",chardata"`
	RESULT                string `xml:"RESULT"`
	ECSYSTEMTRANSACTIONID string `xml:"ECSYSTEMTRANSACTIONID"`
}

type VIVOStatusRsp struct {
	XMLName  xml.Name      `xml:"VGSSYSTEM"`
	Text     string        `xml:",chardata"`
	REQUEST  VIVOStatusReq `xml:"REQUEST"`
	TIME     string        `xml:"TIME"`
	RESPONSE interface{}   `xml:"RESPONSE"`
}

type VIVOGetBalanceReq struct {
	Text   string `xml:",chardata"`
	USERID string `xml:"USERID"`
	HASH   string `xml:"HASH"`
}

type VIVOGetBalanceSuccessRsp struct {
	Text    string `xml:",chardata"`
	RESULT  string `xml:"RESULT"`
	BALANCE string `xml:"BALANCE"`
}

type VIVOGetBalanceRsp struct {
	XMLName  xml.Name          `xml:"VGSSYSTEM"`
	Text     string            `xml:",chardata"`
	REQUEST  VIVOGetBalanceReq `xml:"REQUEST"`
	TIME     string            `xml:"TIME"`
	RESPONSE interface{}       `xml:"RESPONSE"`
}
