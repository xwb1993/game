package model

import (
	myUserHelp "common/userHelp"
	"context"
	"errors"
	"fmt"
	"game/contrib/conn"
	"game/contrib/helper"
	"game/contrib/session"
	"game/contrib/tracerr"
	ryrpc "game/rpc"
	"runtime"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"github.com/spaolacci/murmur3"

	g "github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/mysql"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
)

type MetaTable struct {
	MerchantDB *sqlx.DB
	//MerchantSlaveDB *sqlx.DB
	MerchantBean  *conn.Connection
	MerchantRedis *redis.Client
	IsDev         bool
	Prefix        string
	EsPrefix      string
	PullPrefix    string
	Lang          string
	Program       string
	WebURL        string
	H5URL         string
	WalletMode    string
	PlatCfg       PlatCfg_t
}

var (
	LockTimeout time.Duration = 20 * time.Second
)

var (
	meta               *MetaTable
	loc                *time.Location
	Ctx                = context.Background()
	dialect            = g.Dialect("mysql")
	zero               = decimal.NewFromInt(0)
	one                = decimal.NewFromFloat(0.1)
	colsMember         = helper.EnumFields(myUserHelp.TblMemberBase{})
	colsGameConfig     = helper.EnumFields(ryrpc.TblGameConfig{})
	colsMemberPlatform = helper.EnumFields(MemberPlatform{})
	colTransaction     = helper.EnumFields(memberTransaction{})
	colGame            = helper.EnumFields(Game{})
	colsMemberBalance  = helper.EnumFields(MemberBalance{})
	colsBonusConfig    = helper.EnumFields(ryrpc.TblBonusConfig{})
	bot                *tgbotapi.BotAPI
)

func Constructor(mt *MetaTable) {
	meta = mt
	session.New(mt.MerchantRedis, "bl8")
	loc, _ = time.LoadLocation("America/Sao_Paulo")
}

func Close() {
	meta.MerchantBean.Conn.Release()
	meta.MerchantDB.Close()
	meta.MerchantRedis.Close()
}

type zinc_t struct {
	ID       string `json:"id"`
	Content  string `json:"content"`
	Flags    string `json:"flags"`
	Filename string `json:"filename"`
	Index    string `json:"_index"`
}

func pushError(err error, code string) error {

	fmt.Println(err)
	_, file, line, _ := runtime.Caller(1)
	paths := strings.Split(file, "/")
	l := len(paths)
	if l > 2 {
		file = paths[l-2] + "/" + paths[l-1]
	}
	path := fmt.Sprintf("%s:%d", file, line)
	id := helper.GenId()
	ts := time.Now()
	data := zinc_t{
		ID:       id,
		Content:  tracerr.SprintSource(err, 2, 2),
		Flags:    code,
		Filename: path,
		Index:    fmt.Sprintf("%s_%04d%02d", meta.Program, ts.Year(), ts.Month()),
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("pushError MarshalBinary err =  ", err.Error())
		return err
	}

	_ = BeanPut("zinc_fluent_log", b)
	return fmt.Errorf("系统错误 %s", id)
}

func BeanPut(topic string, b []byte) error {
	_, err := meta.MerchantBean.Put(topic, b, 0, 0, 0)
	if err != nil {
		fmt.Println("pushLog MerchantBean Put err =  ", err.Error())
	}

	return err
}

// 投注锁
func BetLock(username string) error {

	lockBet := fmt.Sprintf("l:b:%s", username)
	return Lock(lockBet)
}

// 投注完成需要解锁
func BetUnlock(username string) {
	lockBet := fmt.Sprintf("l:b:%s", username)
	Unlock(lockBet)
}

func Lock(id string) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(Ctx, val, "1", LockTimeout).Result()
	if err != nil {
		return pushError(err, helper.RedisErr)
	}
	if !ok {
		return errors.New(helper.RequestBusy)
	}

	return nil
}

func LockWait(id string, ttl time.Duration) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)

	for {
		ok, err := meta.MerchantRedis.SetNX(Ctx, val, "1", ttl).Result()
		if err != nil {
			return pushError(err, helper.RedisErr)
		}

		if !ok {
			time.Sleep(50 * time.Millisecond)
			continue
		}

		return nil
	}

	return nil
}

func LockTTL(id string, ttl time.Duration) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(Ctx, val, "1", ttl).Result()
	if err != nil || !ok {
		return pushError(err, helper.RedisErr)
	}

	return nil
}

func Unlock(id string) {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	res, err := meta.MerchantRedis.Unlink(Ctx, val).Result()
	if err != nil || res != 1 {
		fmt.Println("Unlock res = ", res)
		fmt.Println("Unlock err = ", err)
	}
}

func LockSetExpire(id string, expiration time.Duration) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.Expire(Ctx, val, expiration).Result()
	if err != nil || !ok {
		return pushError(err, helper.RedisErr)
	}

	return nil
}

func MurmurHash(str string, seed uint32) uint64 {

	h64 := murmur3.New64WithSeed(seed)
	h64.Write([]byte(str))
	v := h64.Sum64()
	h64.Reset()

	return v
}
