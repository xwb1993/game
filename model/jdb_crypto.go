package model

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"fmt"
)

func JDBAesEncrypt(plaintext []byte, key []byte, iv []byte) (string, error) {

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("err=", err)
		return "", errors.New("invalid decrypt key")
	}
	blockSize := block.BlockSize()
	plaintext = JDBAesZeroPadding(plaintext, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, iv)
	ciphertext := make([]byte, len(plaintext))
	blockMode.CryptBlocks(ciphertext, plaintext)
	encryptString := base64.RawURLEncoding.EncodeToString([]byte(ciphertext))
	return encryptString, nil
}

func JDBAesDecrypt(cipherText string, key []byte, iv []byte) (string, error) {
	decodeData, err := base64.RawURLEncoding.DecodeString(cipherText)
	if err != nil {
		return "", errors.New("invalid decrypt data")
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("err=", err)
		return "", errors.New("invalid decrypt key")
	}
	blockModel := cipher.NewCBCDecrypter(block, iv)
	plainText := make([]byte, len(decodeData))
	blockModel.CryptBlocks(plainText, decodeData)
	return string(bytes.TrimRight(plainText, string([]byte{0}))), nil
}

func JDBAesZeroPadding(cipherText []byte, blockSize int) []byte {
	length := len(cipherText)
	if length%blockSize == 0 {
		return cipherText
	} else {
		padding := blockSize - len(cipherText)%blockSize
		paddingText := bytes.Repeat([]byte{byte(0)}, padding)
		return append(cipherText, paddingText...)
	}
}
