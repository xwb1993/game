package model

type PlatCfg_t struct {
	EVO struct {
		API        string `toml:"api"`
		MerchantID string `toml:"merchant_id"`
		Country    string `toml:"country"`
		Language   string `toml:"language"`
		Currency   string `toml:"currency"`
		CasinoKey  string `toml:"casino_key"`
		APIToken   string `toml:"api_token"`
	} `toml:"evo"`
	VIVO struct {
		API        string `toml:"api"`
		OperatorID string `toml:"operator_id"`
		ServerID   string `toml:"server_id"`
		PassKey    string `toml:"pass_key"`
		Lang       string `toml:"lang"`
		Currency   string `toml:"currency"`
		Country    string `toml:"country"`
	} `toml:"vivo"`
	IBC struct {
		API         string `toml:"api"`
		Prefix      string `toml:"prefix"`
		VendorID    string `toml:"vendor_id"`
		Currency    string `toml:"currency"`
		Lang        string `toml:"lang"`
		OperatorId  string `toml:"operator_id"`
		MaxTransfer string `toml:"max_transfer"`
		MinTransfer string `toml:"min_transfer"`
		OddsType    string `toml:"odds_type"`
	} `toml:"ibc"`
	BTI struct {
		API      string `toml:"api"`
		Currency string `toml:"currency"`
		Lang     string `toml:"lang"`
		OddsType string `toml:"odds_type"`
	} `toml:"bti"`
	PG struct {
		API           string `toml:"api"`
		OperatorToken string `toml:"operator_token"`
		SecretKey     string `toml:"secret_key"`
		Salt          string `toml:"salt"`
		Currency      string `toml:"currency"`
		Language      string `toml:"language"`
		LaunchURL     string `toml:"launch_url"`
		DataURL       string `toml:"data_url"`
	} `toml:"pg"`
	JDB struct {
		API   string `toml:"api"`
		Agent string `toml:"agent"`
		Key   string `toml:"key"`
		Iv    string `toml:"iv"`
		Dc    string `toml:"dc"`
		Lang  string `toml:"lang"`
	} `toml:"jdb"`
	Jl struct {
		API      string `toml:"api"`
		AgentID  string `toml:"agent_id"`
		AgentKey string `toml:"agent_key"`
		Lang     string `toml:"lang"`
		Currency string `toml:"currency"`
	} `toml:"jl"`
	TADA struct {
		API      string `toml:"api"`
		AgentID  string `toml:"agent_id"`
		AgentKey string `toml:"agent_key"`
		Lang     string `toml:"lang"`
		Currency string `toml:"currency"`
	} `toml:"tada"`
	PP struct {
		API         string `toml:"api"`
		SecureLogin string `toml:"secure_login"`
		ProviderID  string `toml:"provider_id"`
		SecretKey   string `toml:"secret_key"`
		Lang        string `toml:"lang"`
		Currency    string `toml:"currency"`
		Country     string `toml:"country"`
	} `toml:"pp"`
	One struct {
		API       string `toml:"api"`
		APIKey    string `toml:"api_key"`
		APISecret string `toml:"api_secret"`
		Lang      string `toml:"lang"`
		Currency  string `toml:"currency"`
	} `toml:"one"`
	EVOTest struct {
		API        string `toml:"api"`
		MerchantID string `toml:"merchant_id"`
		Country    string `toml:"country"`
		Language   string `toml:"language"`
		Currency   string `toml:"currency"`
		CasinoKey  string `toml:"casino_key"`
		APIToken   string `toml:"api_token"`
	} `toml:"evo_test"`
	JlTest struct {
		API      string `toml:"api"`
		AgentID  string `toml:"agent_id"`
		AgentKey string `toml:"agent_key"`
		Lang     string `toml:"lang"`
		Currency string `toml:"currency"`
	} `toml:"jl_test"`
	TADATest struct {
		API      string `toml:"api"`
		AgentID  string `toml:"agent_id"`
		AgentKey string `toml:"agent_key"`
		Lang     string `toml:"lang"`
		Currency string `toml:"currency"`
	} `toml:"tada_test"`
	PPTest struct {
		API         string `toml:"api"`
		SecureLogin string `toml:"secure_login"`
		ProviderID  string `toml:"provider_id"`
		SecretKey   string `toml:"secret_key"`
		Lang        string `toml:"lang"`
		Currency    string `toml:"currency"`
		Country     string `toml:"country"`
	} `toml:"pp_test"`
	PGTest struct {
		API           string `toml:"api"`
		OperatorToken string `toml:"operator_token"`
		SecretKey     string `toml:"secret_key"`
		Salt          string `toml:"salt"`
		Currency      string `toml:"currency"`
		Language      string `toml:"language"`
		LaunchURL     string `toml:"launch_url"`
		DataURL       string `toml:"data_url"`
	} `toml:"pg_test"`
}
