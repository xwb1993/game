package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

// 查看投注帐变
func walletBetStatus(billNo, operationNo, platformID, flag string) (string, []string, decimal.Decimal, error) {

	var (
		status   = ""
		transIDs []string
	)
	totalAmount := decimal.Decimal{}

	if billNo == "" && operationNo == "" {
		return "", transIDs, totalAmount, errors.New(helper.ParamErr)
	}

	ex := g.Ex{
		"bill_no":   billNo,
		"cash_type": helper.TransactionBet,
	}
	switch platformID {
	default:
		if operationNo != "" && flag != "cancel" {
			ex["operation_no"] = operationNo
		}
	}

	trans, err := walletTransactions(ex)
	if err != nil {
		return "", transIDs, totalAmount, err
	}

	if len(trans) == 0 {
		return "", transIDs, totalAmount, sql.ErrNoRows
	}

	for _, v := range trans {
		betAmount, _ := decimal.NewFromString(v.Amount)
		switch flag {
		case "rollback":
			status = v.Remark
			// 回滚时注单状态只能是已取消或已结算
			if v.Remark != "void" && v.Remark != "settled" {
				continue
			}
		case "resettle":
			status = v.Remark
			// 重新结算时注单状态只能是已结算
			if v.Remark != "settled" {
				continue
			}
		case "settle": //结算时存在有效注单后不再更新状态
			if status != "running" {
				status = v.Remark
			}

			if v.Remark == "void" { //已取消的注单不计算有效注单
				continue
			}
		case "cancel":
			if v.OperationNo == operationNo { //取消指定子单
				// 取消注单的情况下，未结算时，可以取消单个子注单，已结算注单只能取消整个注单
				if v.Remark == "running" || v.Remark == "void" {
					transIDs = []string{v.ID}
					return v.Remark, transIDs, betAmount, nil
				}
			}
			status = v.Remark
		case "deduct", "status", "rollin", "rollout", "bet": //投注，指定注单查询
			status = v.Remark
		}

		transIDs = append(transIDs, v.ID)
		totalAmount = totalAmount.Add(betAmount)
	}

	return status, transIDs, totalAmount, nil
}

// 查看结算帐变
func walletSettledBetStatus(billNo, platformID, flag string) (decimal.Decimal, error) {

	payoutAmount := decimal.Decimal{}

	ex := g.Ex{
		"bill_no":   billNo,
		"cash_type": helper.TransactionPayout,
	}
	// 没参加活动
	trans, err := walletLastTransaction(ex)
	if err != nil {
		return payoutAmount, err
	}

	betAmount, _ := decimal.NewFromString(trans.Amount)
	payoutAmount = payoutAmount.Add(betAmount)

	return payoutAmount, nil
}

// 中心钱包帐变
func walletTransaction(mb myUserHelp.TblMemberBase, transIDs []string, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	// 获取加减操作符
	operate := transactionsOperate(cashType)
	if operate == "" {
		return errors.New(helper.CashTypeErr)
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		return err
	}

	// 余额小于要扣除金额且不为重新结算扣除场景
	if amount.Cmp(balance) > 0 && operate == OperateSub &&
		cashType != helper.TransactionResettleDeduction &&
		cashType != helper.TransactionCancelPayout &&
		cashType != helper.TransactionCancelledBetRollback {
		fmt.Printf("balance error, walletTransaction amount = %s, balance = %s, cash_type : %d\n", amount.String(), balance.String(), cashType)
		return errors.New(helper.BalanceErr)
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if operate == OperateSub {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if len(transIDs) > 0 {
		ex := g.Ex{
			"id": transIDs,
		}
		if platformID != helper.EVO && (cashType == helper.TransactionPayout ||
			cashType == helper.TransactionResettlePlus ||
			cashType == helper.TransactionResettleDeduction ||
			cashType == helper.TransactionBetCancel ||
			cashType == helper.TransactionSettledBetCancel ||
			cashType == helper.TransactionCancelPayout ||
			cashType == helper.TransactionCancelledBetRollback) {
			operationNo += ":"
			operationNo += id
		}
		record := g.Record{}
		switch cashType {
		case helper.TransactionPayout, helper.TransactionResettlePlus, helper.TransactionResettleDeduction: //结算
			record = g.Record{
				"remark": "settled",
			}
		case helper.TransactionBetCancel, helper.TransactionSettledBetCancel: //取消投注 取消已结算投注
			record = g.Record{
				"remark": "void",
			}
		case helper.TransactionCancelPayout, helper.TransactionCancelledBetRollback:
			record = g.Record{
				"remark": "running",
			}
		}

		if len(record) > 0 {
			query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
			myredis.UpdateSqlFieldToRedis(query)
		}
	}

	// cq9结算为0的帐变也要存储
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	if amount.Cmp(decimal.Zero) > 0 {
		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else if operate == "-" {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return nil
}

// 中心钱包帐变
func walletMultiTransaction(tx *sql.Tx, mb myUserHelp.TblMemberBase, transIDs []string,
	billNo, operationNo, platformID, remark string, balance, amount decimal.Decimal, cashType int, createAt int64) error {

	// 获取加减操作符
	operate := transactionsOperate(cashType)
	if operate == "" {
		_ = tx.Rollback()
		return errors.New(helper.CashTypeErr)
	}

	// 余额小于要扣除金额且不为重新结算扣除场景
	if amount.Cmp(balance) > 0 && operate == OperateSub &&
		cashType != helper.TransactionResettleDeduction &&
		cashType != helper.TransactionCancelPayout &&
		cashType != helper.TransactionCancelledBetRollback {
		_ = tx.Rollback()
		fmt.Printf("balance error, walletTransaction amount = %s, balance = %s, cash_type : %d\n", amount.String(), balance.String(), cashType)
		return errors.New(helper.BalanceErr)
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if operate == OperateSub {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if len(transIDs) > 0 {
		ex := g.Ex{
			"id": transIDs,
		}
		if cashType == helper.TransactionPayout ||
			cashType == helper.TransactionResettlePlus ||
			cashType == helper.TransactionResettleDeduction ||
			cashType == helper.TransactionBetCancel ||
			cashType == helper.TransactionSettledBetCancel ||
			cashType == helper.TransactionCancelPayout ||
			cashType == helper.TransactionCancelledBetRollback {
			operationNo += ":"
			operationNo += id
		}
		record := g.Record{}
		switch cashType {
		case helper.TransactionPayout, helper.TransactionResettlePlus, helper.TransactionResettleDeduction: //结算
			record = g.Record{
				"remark": "settled",
			}
		case helper.TransactionBetCancel, helper.TransactionSettledBetCancel: //取消投注 取消已结算投注
			record = g.Record{
				"remark": "void",
			}
		case helper.TransactionCancelPayout, helper.TransactionCancelledBetRollback:
			record = g.Record{
				"remark": "running",
			}
		}

		if len(record) > 0 {
			query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
			fmt.Println(query)
			_, err := tx.Exec(query)
			if err != nil {
				fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
				_ = tx.Rollback()
				return err
			}
		}
	}

	// cq9结算为0的帐变也要存储
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		fmt.Println(query)
		_, err := tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			//记录日志
			if strings.HasPrefix(err.Error(), "Error 1062") {
				PushLog(nil, "", "duplicate", "", 200, nil, err)
			}

			return err
		}
	}

	if amount.Cmp(decimal.Zero) > 0 {
		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else if operate == "-" {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return nil
}

// 延迟收到的投注请求（在取消注单请求之后）
func walletLateBetTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, amount decimal.Decimal, createdAt int64) memberTransaction {

	balance, _ := myUserHelp.GetBalance(mb, 2)
	trans := memberTransaction{
		ID:           helper.GenId(),
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBetCancel,
		Amount:       amount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  balance.String(),
		OperationNo:  operationNo,
		CreatedAt:    createdAt,
		Remark:       "void",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	return trans
}

func transactionsOperate(cashType int) string {

	switch cashType {
	// 投注,重新结算减币,取消派彩,已结算注单取消
	case helper.TransactionReserve, //预投注
		helper.TransactionBet,                  //投注
		helper.TransactionResettleDeduction,    //重新结算减币
		helper.TransactionCancelPayout,         //取消派彩
		helper.TransactionSettledBetCancel,     ///投注取消(已结算注单)
		helper.TransactionCancelledBetRollback, //已取消注单回滚
		helper.TransactionAdjustDiv,            //场馆调整减
		helper.TransactionBetNSettleLose:       //电子投付输
		return "-"
		// 投注取消,派彩,重新结算加币
	case helper.TransactionBetCancel, //投注取消
		helper.TransactionPayout,        //派彩
		helper.TransactionResettlePlus,  //重新结算加币
		helper.TransactionPromoPayout,   //场馆活动派彩
		helper.TransactionAdjustPlus,    //场馆调整加
		helper.TransactionBetNSettleWin, //电子投付赢
		//EVO红利
		helper.TransactionEVOPrize,   //游戏奖金(EVO)
		helper.TransactionEVOPromote, //推广(EVO)
		helper.TransactionEVOJackpot: //头奖(EVO)
		return "+"
		// 错误的帐变类型
	default:
		return ""
	}
}

// 取投注的帐变
func walletTransactions(ex g.Ex) ([]memberTransaction, error) {

	var trans []memberTransaction
	query, _, _ := dialect.From("tbl_balance_transaction").
		Select(colTransaction...).Where(ex).Order(g.C("created_at").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&trans, query)

	return trans, err
}

/*
利用redis判断订单号是否存在
*/

func SetTransactionToRedis(ex g.Ex, trans memberTransaction) {
	dataType, _ := json.Marshal(ex)
	strRedisKey := string(dataType)

	strTrans, _ := json.Marshal(trans)

	meta.MerchantRedis.Set(context.Background(), strRedisKey, string(strTrans), 3600*time.Second)
}
func walletTransactionFindOne(ex g.Ex) (memberTransaction, bool) {

	trans := memberTransaction{}
	dataType, _ := json.Marshal(ex)
	strRedisKey := string(dataType)

	res, err := meta.MerchantRedis.Get(context.Background(), strRedisKey).Result()
	if err != nil {
		return trans, false
	} else {
		unmarErr := json.Unmarshal([]byte(res), &trans)
		if unmarErr == nil {
			return trans, true
		} else {
			return trans, false
		}
	}
}

// 取第一笔帐变（投注）
func walletFirstTransaction(ex g.Ex) (memberTransaction, error) {

	trans := memberTransaction{}
	query, _, _ := dialect.From("tbl_balance_transaction").
		Select(colTransaction...).Where(ex).Order(g.C("created_at").Asc()).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&trans, query)
	if err != nil && err != sql.ErrNoRows {
		return trans, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return trans, nil
}

// 取最后一笔帐变（结算）
func walletLastTransaction(ex g.Ex) (memberTransaction, error) {

	trans := memberTransaction{}
	query, _, _ := dialect.From("tbl_balance_transaction").
		Select(colTransaction...).Where(ex).Order(g.C("created_at").Desc()).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&trans, query)
	if err != nil && err != sql.ErrNoRows {
		return trans, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return trans, err
}

func JsonResponse(platform string, ctx *fasthttp.RequestCtx, res interface{}) {

	//requestURI := fmt.Sprintf("%s?%s", string(ctx.Path()), ctx.QueryArgs().String())
	//requestBody := ctx.PostBody()
	content, _ := helper.JsonMarshal(res)
	fmt.Println(string(content))
	ctx.Response.Header.Set("Content-Type", "application/json; charset=UTF-8")
	ctx.SetBody(content)

	// 生产环境不输出日志
	//if !meta.IsDev {
	//	return
	//}
	//

	//if platform == "SABA" {
	//	requestBody, _ = gunzip(requestBody)
	//}
	//
	//ts := time.Now()
	//l := map[string]interface{}{
	//	"platform":    platform,
	//	"requestURL":  requestURI,
	//	"requestBody": string(ctx.PostBody()),
	//  "host":        string(ctx.Host()),
	//	"body":        string(content),
	//	"ts":          ts.In(loc).UnixMicro(),
	//	"_index":      fmt.Sprintf("callback_%04d%02d", ts.Year(), ts.Month()),
	//}
	//b, _ := helper.JsonMarshal(&l)
	//_ = BeanPut("zinc_fluent_log", b)
}

func JsonJDBResponse(platform string, ctx *fasthttp.RequestCtx, res interface{}, body []byte) {

	//requestURI := fmt.Sprintf("%s?%s", string(ctx.Path()), ctx.QueryArgs().String())
	content, _ := helper.JsonMarshal(res)
	fmt.Println(string(content))
	ctx.Response.Header.Set("Content-Type", "application/json; charset=UTF-8")
	ctx.SetBody(content)

	// 生产环境不输出日志
	//if !meta.IsDev {
	//	return
	//}
	//

	//ts := time.Now()
	//l := map[string]interface{}{
	//	"platform":    platform,
	//	"requestURL":  requestURI,
	//	"requestBody": string(body),
	//  "host":        string(ctx.Host()),
	//	"body":        string(content),
	//	"ts":          ts.In(loc).UnixMicro(),
	//	"_index":      fmt.Sprintf("callback_%04d%02d", ts.Year(), ts.Month()),
	//}
	//b, _ := helper.JsonMarshal(&l)
	//_ = BeanPut("zinc_fluent_log", b)
}

func XMLResponse(platform string, ctx *fasthttp.RequestCtx, v interface{}) {

	requestURI := fmt.Sprintf("%s?%s", string(ctx.Path()), ctx.QueryArgs().String())
	requestBody := string(ctx.PostBody())
	content, _ := helper.XMLMarshal(v)
	fmt.Println(string(content))
	ctx.Response.Header.Set("Content-Type", "text/xml")
	ctx.SetBody(content)

	// 生产环境不输出日志
	//if !meta.IsDev {
	//	return
	//}

	ts := time.Now()
	l := map[string]interface{}{
		"platform":    platform,
		"requestURL":  requestURI,
		"requestBody": requestBody,
		"host":        string(ctx.Host()),
		"body":        string(content),
		"ts":          ts.In(loc).UnixMicro(),
		"_index":      fmt.Sprintf("callback_%04d%02d", ts.Year(), ts.Month()),
	}
	b, _ := helper.JsonMarshal(&l)
	_ = BeanPut("zinc_fluent_log", b)
}

func BTIResponse(platform string, ctx *fasthttp.RequestCtx, v string) {

	requestURI := fmt.Sprintf("%s?%s", string(ctx.Path()), ctx.QueryArgs().String())
	requestBody := string(ctx.PostBody())
	fmt.Println(v)
	ctx.Response.Header.Set("Content-Type", "text/plain")
	ctx.SetBody([]byte(v))

	ts := time.Now()
	l := map[string]interface{}{
		"platform":    platform,
		"requestURL":  requestURI,
		"requestBody": requestBody,
		"host":        string(ctx.Host()),
		"body":        v,
		"ts":          ts.In(loc).UnixMicro(),
		"_index":      fmt.Sprintf("callback_%04d%02d", ts.Year(), ts.Month()),
	}
	b, _ := helper.JsonMarshal(&l)
	_ = BeanPut("zinc_fluent_log", b)
}
