package model

var (
	JDBGmap = map[string]string{
		"14001": "Cock Fight",
		"14002": "Maya Run",
		"14003": "Panda Panda",
		"14004": "Zelda",
		"14005": "Mr. Bao",
		"14006": "Billionaire",
		"14007": "One Punch Man",
		"14008": "Dragon Warrior",
		"14010": "Dragon",
		"14011": "Guardians of The Galaxy",
		"14012": "Street Fighter",
		"14013": "China Rouge",
		"14015": "Star Wars",
		"14016": "Kingsman",
		"14017": "War of Beauty",
		"14018": "Daji",
		"14019": "Gems Gems",
		"14020": "Curvy Magician",
		"14021": "Rolling In Money",
		"14022": "Mining Upstart",
		"14023": "Poker King",
		"14025": "Lucky Racing",
		"14026": "Fa Da Cai",
		"14027": "LuckySeven",
		"14029": "OrientAnimals",
		"14030": "TripleKingKong",
		"14033": "BirdsParty",
		"14034": "GoLaiFu",
		"14035": "DragonsWorld",
		"14036": "SuperNiubi",
		"14038": "EgyptTreasure",
		"14039": "Fortune Treasure",
		"14040": "PirateTreasure",
		"14041": "Mjolnir",
		"14042": "TreasureBowl",
		"14043": "GoldenDisco",
		"14044": "FunkyKingKong",
		"14045": "SuperNiubiDeluxe",
		"14046": "MinerBabe",
		"14047": "Moneybags Man",
		"14048": "DoubleWilds",
		"14049": "PopPopFruity",
		"14050": "Spindrift",
		"14051": "DragonsGate",
		"14052": "JungleJungle",
		"14053": "Spindrift2",
		"14054": "LuckyDiamond",
		"14055": "Kong",
		"14056": "RexBrothers",
		"14057": "NinjaX",
		"14058": "Wonder Elephant",
		"14059": "MarvelousIV",
		"14060": "LanternWealth",
		"14061": "MayaGoldCrazy",
		"14062": "War Of Empires",
		"14063": "BigThreeDragons",
		"14064": "Boom Fiesta",
		"14065": "Blossom of Wealth",
		"14066": "Star Line",
		"14067": "Glamorous Girl",
		"14068": "ProsperityTiger",
		"14069": "BBQ Burger",
		"14070": "Book of Mystery",
		"14075": "Fortune Neko",
		"15001": "Rooster In Love",
		"15002": "Monkey King",
		"15004": "Fire Bull",
		"15005": "Wealthy Fuwa",
		"15006": "Inca Empire",
		"15009": "Ninja Rush",
		"15010": "Chef Panda",
		"15012": "Legendary5",
		"15013": "Mystery of Ninetails",
		"8001":  "Lucky Dragons",
		"8002":  "Flirting Scholar Tang",
		"8003":  "Winning Mask",
		"8004":  "Wukong",
		"8006":  "Formosa Bear",
		"8007":  "Lucky Qilin",
		"8014":  "Lucky Lion",
		"8015":  "Moonlight Treasure",
		"8016":  "Coffeeholics",
		"8017":  "New Year",
		"8018":  "Napoleon",
		"8019":  "Four Treasures",
		"8020":  "Open Sesame",
		"8021":  "Banana Saga",
		"8022":  "Mahjong",
		"8023":  "Olympian Temple",
		"8024":  "Crystal Realm",
		"8025":  "Burglar",
		"8026":  "Dancing Papa",
		"8027":  "Chef-Doeuvre",
		"8028":  "Lucky Miner",
		"8029":  "Candy Land",
		"8030":  "Crazy Scientist",
		"8031":  "Super Dumpling",
		"8034":  "Cash Man",
		"8035":  "Lucky Phoenix",
		"8036":  "Dragon King",
		"8037":  "Magic Show",
		"8044":  "Beauty And The Kingdom",
		"8046":  "Guan Gong",
		"8047":  "Winning Mask II",
		"8048":  "OpenSesameII",
		"8049":  "Flirting Scholar Tang II",
		"8050":  "FortuneHorse",
		"8051":  "XiYangYang",
		// fish
		"7001": "Dragon Fishing",
		"7002": "Dragon Fishing II",
		"7003": "Cai Shen Fishing",
		"7004": "Five Dragons Fishing",
		"7005": "Fishing YiLuFa",
		"7006": "Dragon Master",
		"7007": "Fishing Disco",
		// spribe
		"22001": "Aviator",
		"22002": "Dice",
		"22003": "Goal",
		"22004": "Plinko",
		"22005": "Mines",
		"22006": "Hilo",
		"22007": "Keno",
		"22008": "Mini Roulette",
	}
	gTypes = map[string]string{
		"14001": "0",
		"14002": "0",
		"14003": "0",
		"14004": "0",
		"14005": "0",
		"14006": "0",
		"14007": "0",
		"14008": "0",
		"14010": "0",
		"14011": "0",
		"14012": "0",
		"14013": "0",
		"14015": "0",
		"14016": "0",
		"14017": "0",
		"14018": "0",
		"14019": "0",
		"14020": "0",
		"14021": "0",
		"14022": "0",
		"14023": "0",
		"14025": "0",
		"14026": "0",
		"14027": "0",
		"14029": "0",
		"14030": "0",
		"14033": "0",
		"14034": "0",
		"14035": "0",
		"14036": "0",
		"14038": "0",
		"14039": "0",
		"14040": "0",
		"14041": "0",
		"14042": "0",
		"14043": "0",
		"14044": "0",
		"14045": "0",
		"14046": "0",
		"14047": "0",
		"14048": "0",
		"14049": "0",
		"14050": "0",
		"14051": "0",
		"14052": "0",
		"14053": "0",
		"14054": "0",
		"14055": "0",
		"14056": "0",
		"14057": "0",
		"14058": "0",
		"14059": "0",
		"14060": "0",
		"14061": "0",
		"14062": "0",
		"14063": "0",
		"14064": "0",
		"14065": "0",
		"14066": "0",
		"14067": "0",
		"14068": "0",
		"14069": "0",
		"14070": "0",
		"14075": "0",
		"15001": "0",
		"15002": "0",
		"15004": "0",
		"15005": "0",
		"15006": "0",
		"15009": "0",
		"15010": "0",
		"15012": "0",
		"15013": "0",
		"8001":  "0",
		"8002":  "0",
		"8003":  "0",
		"8004":  "0",
		"8006":  "0",
		"8007":  "0",
		"8014":  "0",
		"8015":  "0",
		"8016":  "0",
		"8017":  "0",
		"8018":  "0",
		"8019":  "0",
		"8020":  "0",
		"8021":  "0",
		"8022":  "0",
		"8023":  "0",
		"8024":  "0",
		"8025":  "0",
		"8026":  "0",
		"8027":  "0",
		"8028":  "0",
		"8029":  "0",
		"8030":  "0",
		"8031":  "0",
		"8034":  "0",
		"8035":  "0",
		"8036":  "0",
		"8037":  "0",
		"8044":  "0",
		"8046":  "0",
		"8047":  "0",
		"8048":  "0",
		"8049":  "0",
		"8050":  "0",
		"8051":  "0",

		// spribe
		"22001": "22",
		"22002": "22",
		"22003": "22",
		"22004": "22",
		"22005": "22",
		"22006": "22",
		"22007": "22",
		"22008": "22",
		// FISH
		"7001": "7",
		"7002": "7",
		"7003": "7",
		"7004": "7",
		"7005": "7",
		"7006": "7",
		"7007": "7",
	}
)

type JDBResponse struct {
	Status  string  `json:"status"`
	Balance float64 `json:"balance"`
	ErrText string  `json:"err_text"`
}

type spribeRsp struct {
	Status  string `json:"status"`
	Path    string `json:"path"`
	ErrText string `json:"err_text"`
}

type JDBBalance struct {
	Action   int    `json:"action"`
	Ts       int64  `json:"ts"`
	Uid      string `json:"uid"`
	Currency string `json:"currency"`
}

type JDBBet struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	Uid            string  `json:"uid"`
	Currency       string  `json:"currency"`
	Amount         float64 `json:"amount"`
	GameRoundSeqNo int64   `json:"gameRoundSeqNo"`
	MType          int     `json:"mType"`
}

type JDBSettle struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	Uid            string  `json:"uid"`
	Currency       string  `json:"currency"`
	Amount         float64 `json:"amount"`
	RefTransferIds []int64 `json:"refTransferIds"`
	GameRoundSeqNo int64   `json:"gameRoundSeqNo"`
	GameSeqNo      int64   `json:"gameSeqNo"`
	GType          int     `json:"gType"`
	MType          int     `json:"mType"`
	ReportDate     string  `json:"reportDate"`
	GameDate       string  `json:"gameDate"`
	LastModifyTime string  `json:"lastModifyTime"`
	Win            float64 `json:"win"`
	Bet            float64 `json:"bet"`
	ValidBet       float64 `json:"validBet"`
	NetWin         float64 `json:"netWin"`
	Tax            float64 `json:"tax"`
	SessionNo      string  `json:"sessionNo"`
}

type JDBCancelBet struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	Uid            string  `json:"uid"`
	Currency       string  `json:"currency"`
	Amount         float64 `json:"amount"`
	RefTransferIds []int64 `json:"refTransferIds"`
	GameRoundSeqNo int64   `json:"gameRoundSeqNo"`
}

type JDBBetNSettleSlot struct {
	Action            int     `json:"action"`
	Ts                int64   `json:"ts"`
	TransferId        int64   `json:"transferId"`
	GameSeqNo         int64   `json:"gameSeqNo"`
	Uid               string  `json:"uid"`
	GType             int     `json:"gType "`
	MType             int     `json:"mType"`
	ReportDate        string  `json:"reportDate"`
	GameDate          string  `json:"gameDate"`
	Currency          string  `json:"currency"`
	Bet               float64 `json:"bet"`
	Win               float64 `json:"win"`
	NetWin            float64 `json:"netWin"`
	Denom             float64 `json:"denom"`
	IpAddress         string  `json:"ipAddress"`
	ClientType        string  `json:"clientType"`
	SystemTakeWin     float64 `json:"systemTakeWin"`
	LastModifyTime    string  `json:"lastModifyTime"`
	JackpotWin        float64 `json:"jackpotWin"`
	JackpotContribute float64 `json:"jackpotContribute"`
	HasFreeGame       int     `json:"hasFreeGame"`
	HasGamble         int     `json:"hasGamble"`
}

type JDBBetNSettleFish struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	GameSeqNo      int64   `json:"gameSeqNo"`
	Uid            string  `json:"uid"`
	GType          int     `json:"gType "`
	MType          int     `json:"mType"`
	ReportDate     string  `json:"reportDate"`
	GameDate       string  `json:"gameDate"`
	Currency       string  `json:"currency"`
	Bet            float64 `json:"bet"`
	Win            float64 `json:"win"`
	NetWin         float64 `json:"netWin"`
	Denom          float64 `json:"denom"`
	IpAddress      string  `json:"ipAddress"`
	ClientType     string  `json:"clientType"`
	SystemTakeWin  float64 `json:"systemTakeWin"`
	LastModifyTime string  `json:"lastModifyTime"`
	RoomType       int     `json:"roomType"`
}

type JDBBetNSettleArcade struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	GameSeqNo      int64   `json:"gameSeqNo"`
	Uid            string  `json:"uid"`
	GType          int     `json:"gType "`
	MType          int     `json:"mType"`
	ReportDate     string  `json:"reportDate"`
	GameDate       string  `json:"gameDate"`
	Currency       string  `json:"currency"`
	Bet            float64 `json:"bet"`
	Win            float64 `json:"win"`
	NetWin         float64 `json:"netWin"`
	Denom          float64 `json:"denom"`
	IpAddress      string  `json:"ipAddress"`
	ClientType     string  `json:"clientType"`
	SystemTakeWin  float64 `json:"systemTakeWin"`
	LastModifyTime string  `json:"lastModifyTime"`
	HasGamble      int     `json:"hasGamble"`
	HasBonusGame   int     `json:"hasBonusGame"`
}

type JDBBetNSettleLottery struct {
	Action         int     `json:"action"`
	Ts             int64   `json:"ts"`
	TransferId     int64   `json:"transferId"`
	GameSeqNo      int64   `json:"gameSeqNo"`
	Uid            string  `json:"uid"`
	GType          int     `json:"gType "`
	MType          int     `json:"mType"`
	ReportDate     string  `json:"reportDate"`
	GameDate       string  `json:"gameDate"`
	Currency       string  `json:"currency"`
	Bet            float64 `json:"bet"`
	Win            float64 `json:"win"`
	NetWin         float64 `json:"netWin"`
	Denom          float64 `json:"denom"`
	IpAddress      string  `json:"ipAddress"`
	ClientType     string  `json:"clientType"`
	SystemTakeWin  int     `json:"systemTakeWin"`
	LastModifyTime string  `json:"lastModifyTime"`
	HasBonusGame   int     `json:"hasBonusGame"`
}

type JDBCancelBetNSettle struct {
	Action     int    `json:"action"`
	Ts         int64  `json:"ts"`
	TransferId string `json:"transferId"`
	Uid        string `json:"uid"`
}
