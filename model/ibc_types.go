package model

import "time"

type IBCResult struct {
	Status       string      `json:"status"`
	Txns         interface{} `json:"txns"`
	Msg          string      `json:"msg"`
	Balance      float64     `json:"balance"`
	RefId        string      `json:"refId"`
	LicenseeTxId string      `json:"licenseeTxId"`
	BalanceTs    string      `json:"balanceTs"`
}

type IBCCheckTicketResult struct {
	ErrorCode int    `json:"error_code"`
	Message   string `json:"message"`
	Data      struct {
		BetType      int    `json:"betType"`
		UserId       string `json:"userId"`
		LicenseeTxId string `json:"licenseeTxId"`
		WinlostDate  string `json:"winlostDate"`
		TxId         int64  `json:"txId"`
		RefId        string `json:"refId"`
		//Stake        int     `json:"stake"`
		Odds         float64 `json:"odds"`
		TransHistory []struct {
			Action     string `json:"action"`
			ActionDate string `json:"actionDate"`
			//Stake       int     `json:"stake"`
			//Odds        float64 `json:"odds"`
			//Status      int     `json:"status"`
			OperationId string `json:"operationId"`
			//CreditAmount    float64 `json:"creditAmount"`
			//DebitAmount     float64 `json:"debitAmount"`
			//InRetry         bool `json:"inRetry"`
			//ReachRetryLimit bool `json:"reachRetryLimit"`
		} `json:"transHistory"`
	} `json:"Data"`
}

type IBCConfirmBetParlayResult struct {
	Status  string  `json:"status"`
	Msg     string  `json:"msg"`
	Balance float64 `json:"balance"`
}

type IBCConfirmBetParlaySuccess struct {
	Status  string  `json:"status"`
	Balance float64 `json:"balance"`
}

type IBCConfirmBetParlayFailed struct {
	Status string `json:"status"`
	Msg    string `json:"msg"`
}

type IBCResultTxn struct {
	RefId        string `json:"refId"`
	LicenseeTxId string `json:"licenseeTxId"`
}

type IBCBalanceReq struct {
	Key     string `json:"key"`
	Message struct {
		Action string `json:"action"`
		UserId string `json:"userId"`
	} `json:"message"`
}

type IBCBetReq struct {
	Key     string `json:"key"`
	Message struct {
		Action          string  `json:"action"`
		OperationId     string  `json:"operationId"`
		UserId          string  `json:"userId"`
		Currency        int     `json:"currency"`
		MatchId         int     `json:"matchId"`
		HomeId          int     `json:"homeId"`
		AwayId          int     `json:"awayId"`
		HomeName        string  `json:"homeName"`
		AwayName        string  `json:"awayName"`
		KickOffTime     string  `json:"kickOffTime"`
		BetTime         string  `json:"betTime"`
		BetAmount       float64 `json:"betAmount"`
		ActualAmount    float64 `json:"actualAmount"`
		SportType       int     `json:"sportType"`
		SportTypeName   string  `json:"sportTypeName"`
		BetType         int     `json:"betType"`
		BetTypeName     string  `json:"betTypeName"`
		OddsType        int     `json:"oddsType"`
		OddsId          int     `json:"oddsId"`
		Odds            float64 `json:"odds"`
		BetChoice       string  `json:"betChoice"`
		BetChoiceEn     string  `json:"betChoice_en"`
		UpdateTime      string  `json:"updateTime"`
		LeagueId        int     `json:"leagueId"`
		LeagueName      string  `json:"leagueName"`
		LeagueNameEn    string  `json:"leagueName_en"`
		SportTypeNameEn string  `json:"sportTypeName_en"`
		BetTypeNameEn   string  `json:"betTypeName_en"`
		HomeNameEn      string  `json:"homeName_en"`
		AwayNameEn      string  `json:"awayName_en"`
		IP              string  `json:"ip"`
		IsLive          bool    `json:"isLive"`
		RefId           string  `json:"refId"`
		TsId            string  `json:"tsId"`
		Point           string  `json:"point"`
		Point2          string  `json:"Point2"`
		BetTeam         string  `json:"betTeam"`
		HomeScore       int     `json:"homeScore"`
		AwayScore       int     `json:"awayScore"`
		BaStatus        bool    `json:"baStatus"`
		Excluding       string  `json:"excluding"`
		BetFrom         string  `json:"betFrom"`
		CreditAmount    float64 `json:"creditAmount"`
		DebitAmount     float64 `json:"debitAmount"`
		OddsInfo        string  `json:"oddsInfo"`
		MatchDatetime   string  `json:"matchDatetime"`
		BetRemark       string  `json:"betRemark"`
		TicketDetail    []struct {
			MatchId         int       `json:"matchId"`
			HomeId          int       `json:"homeId"`
			AwayId          int       `json:"awayId"`
			HomeName        string    `json:"homeName"`
			AwayName        string    `json:"awayName"`
			KickOffTime     time.Time `json:"kickOffTime"`
			SportType       int       `json:"sportType"`
			SportTypeName   string    `json:"sportTypeName"`
			BetType         int       `json:"betType"`
			BetTypeName     string    `json:"betTypeName"`
			OddsId          int       `json:"oddsId"`
			Odds            float64   `json:"odds"`
			OddsType        int       `json:"oddsType"`
			BetChoice       string    `json:"betChoice"`
			BetChoiceEn     string    `json:"betChoice_en"`
			LeagueId        int       `json:"leagueId"`
			LeagueName      string    `json:"leagueName"`
			IsLive          bool      `json:"isLive"`
			Point           string    `json:"point"`
			Point2          string    `json:"point2"`
			BetTeam         string    `json:"betTeam"`
			HomeScore       int       `json:"homeScore"`
			AwayScore       int       `json:"awayScore"`
			BaStatus        bool      `json:"baStatus"`
			Excluding       string    `json:"excluding"`
			LeagueNameEn    string    `json:"leagueName_en"`
			SportTypeNameEn string    `json:"sportTypeName_en"`
			HomeNameEn      string    `json:"homeName_en"`
			AwayNameEn      string    `json:"awayName_en"`
			BetTypeNameEn   string    `json:"betTypeName_en"`
			MatchDateTime   time.Time `json:"matchDateTime"`
			BetRemark       string    `json:"betRemark"`
		} `json:"ticketDetail"`
	} `json:"message"`
}

type IBCConfirmReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action          string          `json:"action"`
		OperationId     string          `json:"operationId"`
		UserId          string          `json:"userId"`
		UpdateTime      string          `json:"updateTime"`
		TransactionTime string          `json:"transactionTime"`
		Txns            []IBCConfirmReq `json:"txns"`
	}
}
type IBCConfirmReq struct {
	RefId         string  `json:"refId"`
	TxId          int64   `json:"txId"`
	LicenseeTxId  string  `json:"licenseeTxId"`
	Odds          float64 `json:"odds"`
	OddsType      int     `json:"oddsType"`
	ActualAmount  float64 `json:"actualAmount"`
	IsOddsChanged bool    `json:"isOddsChanged"`
	CreditAmount  float64 `json:"creditAmount"`
	DebitAmount   float64 `json:"debitAmount"`
	WinlostDate   string  `json:"winlostDate"`
}

type IBCCancelBetReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action          string            `json:"action"`
		OperationId     string            `json:"operationId"`
		UserId          string            `json:"userId"`
		UpdateTime      string            `json:"updateTime"`
		TransactionTime string            `json:"transactionTime"`
		Txns            []IBCCancelBetReq `json:"txns"`
	}
}
type IBCCancelBetReq struct {
	RefId        string  `json:"refId"`
	CreditAmount float64 `json:"creditAmount"`
	DebitAmount  float64 `json:"debitAmount"`
}

type IBCSettleReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action      string         `json:"action"`
		OperationId string         `json:"operationId"`
		Txns        []IBCSettleReq `json:"txns"`
	}
}
type IBCSettleReq struct {
	UserId       string  `json:"userId"`
	RefId        string  `json:"refId"`
	TxId         int64   `json:"txId"`
	UpdateTime   string  `json:"updateTime"`
	WinlostDate  string  `json:"winlostDate"`
	Status       string  `json:"status"`
	Payout       float64 `json:"payout"`
	CreditAmount float64 `json:"creditAmount"`
	DebitAmount  float64 `json:"debitAmount"`
	ExtraStatus  string  `json:"extraStatus"`
}

type IBCResettleReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action      string           `json:"action"`
		OperationId string           `json:"operationId"`
		Txns        []IBCResettleReq `json:"txns"`
	}
}
type IBCResettleReq struct {
	UserId       string  `json:"userId"`
	RefId        string  `json:"refId"`
	TxId         int64   `json:"txId"`
	UpdateTime   string  `json:"updateTime"`
	WinlostDate  string  `json:"winlostDate"`
	Status       string  `json:"status"`
	Payout       float64 `json:"payout"`
	CreditAmount float64 `json:"creditAmount"`
	DebitAmount  float64 `json:"debitAmount"`
	ExtraStatus  string  `json:"extraStatus"`
}

type IBCUnsettleReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action      string           `json:"action"`
		OperationId string           `json:"operationId"`
		Txns        []IBCUnsettleReq `json:"txns"`
	}
}
type IBCUnsettleReq struct {
	UserId       string  `json:"userId"`
	RefId        string  `json:"refId"`
	TxId         int64   `json:"txId"`
	UpdateTime   string  `json:"updateTime"`
	CreditAmount float64 `json:"creditAmount"`
	DebitAmount  float64 `json:"debitAmount"`
	ExtraStatus  string  `json:"extraStatus"`
}

type IBCPlaceBetParlayReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action         string                          `json:"action"`
		OperationId    string                          `json:"operationId"`
		UserId         string                          `json:"userId"`
		Currency       int                             `json:"currency"`
		BetTime        string                          `json:"betTime"`
		UpdateTime     string                          `json:"updateTime"`
		TotalBetAmount float64                         `json:"totalBetAmount"`
		IP             string                          `json:"IP"`
		TsId           string                          `json:"tsId"`
		BetFrom        string                          `json:"betFrom"`
		CreditAmount   float64                         `json:"creditAmount"`
		DebitAmount    float64                         `json:"debitAmount"`
		Txns           []IBCPlaceBetParlayCombo        `json:"txns"`
		TicketDetail   []IBCPlaceBetParlayTicketDetail `json:"ticketDetail"`
	}
}
type IBCPlaceBetParlayCombo struct {
	RefId        string                   `json:"refId"`
	ParlayType   string                   `json:"parlayType"`
	BetAmount    float64                  `json:"betAmount"`
	CreditAmount float64                  `json:"creditAmount"`
	DebitAmount  float64                  `json:"debitAmount"`
	Detail       []map[string]interface{} `json:"detail"`
}
type IBCPlaceBetParlayDetail struct {
	Type     int     `json:"type"`
	Name     string  `json:"name"`
	BetCount int     `json:"betCount"`
	Stake    float64 `json:"stake"`
	Odds     float64 `json:"odds"`
}
type IBCPlaceBetParlaySingleBetDetail struct {
	MatchId int `json:"matchId"`
}
type IBCPlaceBetParlayTicketDetail struct {
	MatchId         int     `json:"matchId"`
	HomeId          int     `json:"homeId"`
	AwayId          int     `json:"awayId"`
	HomeName        string  `json:"homeName"`
	AwayName        string  `json:"awayName"`
	KickOffTime     string  `json:"kickOffTime"`
	SportType       int     `json:"sportType"`
	SportTypeName   string  `json:"sportTypeName"`
	BetType         int     `json:"betType"`
	BetTypeName     string  `json:"betTypeName"`
	OddsId          int     `json:"oddsId"`
	Odds            float64 `json:"odds"`
	OddsType        int     `json:"oddsType"`
	BetChoice       string  `json:"betChoice"`
	BetChoiceEn     string  `json:"betChoice_en"`
	LeagueId        int     `json:"leagueId"`
	LeagueName      string  `json:"leagueName"`
	IsLive          bool    `json:"isLive"`
	Point           string  `json:"point"`
	Point2          string  `json:"point2"`
	BetTeam         string  `json:"betTeam"`
	HomeScore       int     `json:"homeScore"`
	AwayScore       int     `json:"awayScore"`
	BaStatus        bool    `json:"baStatus"`
	Excluding       string  `json:"excluding"`
	LeagueNameEn    string  `json:"leagueName_en"`
	SportTypeNameEn string  `json:"sportTypeName_en"`
	HomeNameEn      string  `json:"homeName_en"`
	AwayNameEn      string  `json:"awayName_en"`
	BetTypeNameEn   string  `json:"betTypeName_en"`
	MatchDatetime   string  `json:"matchDatetime"`
	BetRemark       string  `json:"betRemark"`
}

type IBCConfirmBetParlayReqData struct {
	Key     string `json:"key"`
	Message struct {
		Action          string                            `json:"action"`
		OperationId     string                            `json:"operationId"`
		UserId          string                            `json:"userId"`
		UpdateTime      string                            `json:"updateTime"`
		TransactionTime string                            `json:"transactionTime"`
		Txns            []IBCConfirmBetParlayTicketInfo   `json:"txns"`
		TicketDetail    []IBCConfirmBetParlayTicketDetail `json:"ticketDetail"`
	} `json:"message"`
}
type IBCConfirmBetParlayTicketInfo struct {
	RefId         string  `json:"refId"`
	TxId          int64   `json:"txId"`
	LicenseeTxId  string  `json:"licenseeTxId"`
	ActualAmount  float64 `json:"actualAmount"`
	IsOddsChanged bool    `json:"isOddsChanged"`
	CreditAmount  float64 `json:"creditAmount"`
	DebitAmount   float64 `json:"debitAmount"`
	WinlostDate   string  `json:"winlostDate"`
	Odds          float64 `json:"odds"`
}
type IBCConfirmBetParlayTicketDetail struct {
	MatchId       int     `json:"matchId"`
	SportType     int     `json:"sportType"`
	BetType       int     `json:"betType"`
	OddsId        int     `json:"oddsId"`
	Odds          float64 `json:"odds"`
	OddsType      int     `json:"oddsType"`
	LeagueId      int     `json:"leagueId"`
	IsLive        bool    `json:"isLive"`
	IsOddsChanged bool    `json:"isOddsChanged"`
}

/*
{"key":"uw9mzogdeb","message":{"action":"AdjustBalance","time":"2023-08-07T04:35:49.776-04:00","userId":"lb861500191095090765","currency":20,"txId":40000,"refId":"4302200_20000_U","operationId":"4302200_8_30000_U","winlostDate":"2023-08-07T00:00:00.000-04:00","betType":17003,"betTypeName":"Saba Loyalty Points Exchange","balanceInfo":{"creditAmount":10,"debitAmount":0}}}
*/
type IBCAdjustBalanceReq struct {
	Key     string `json:"key"`
	Message struct {
		Action      string `json:"action"`
		Time        string `json:"time"`
		UserId      string `json:"userId"`
		Currency    int    `json:"currency"`
		TxId        int    `json:"txId"`
		RefId       string `json:"refId"`
		OperationId string `json:"operationId"`
		WinlostDate string `json:"winlostDate"`
		BetType     int    `json:"betType"`
		BetTypeName string `json:"betTypeName"`
		BalanceInfo struct {
			CreditAmount float64 `json:"creditAmount"`
			DebitAmount  float64 `json:"debitAmount"`
		} `json:"balanceInfo"`
	} `json:"message"`
}
