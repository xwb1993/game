package model

type OneLoginRsp struct {
	Status  string `json:"status"`
	TraceId string `json:"traceId"`
	Data    struct {
		GameUrl string `json:"gameUrl"`
		Token   string `json:"token"`
	} `json:"data"`
}

type OneFailedRsp struct {
	TraceId string `json:"traceId"`
	Status  string `json:"status"`
}

type OneBalanceReq struct {
	TraceId  string `json:"traceId"`
	Username string `json:"username"`
	Currency string `json:"currency"`
	Token    string `json:"token"`
}

type OneBalanceData struct {
	Username string  `json:"username"`
	Currency string  `json:"currency"`
	Balance  float64 `json:"balance"`
}

type OneRsp struct {
	TraceId string         `json:"traceId"`
	Status  string         `json:"status"`
	Data    OneBalanceData `json:"data"`
}

type OneBetReq struct {
	TraceId               string  `json:"traceId"`
	Username              string  `json:"username"`
	TransactionId         string  `json:"transactionId"`
	BetId                 string  `json:"betId"`
	ExternalTransactionId string  `json:"externalTransactionId"`
	Amount                float64 `json:"amount"`
	Currency              string  `json:"currency"`
	Token                 string  `json:"token"`
	GameCode              string  `json:"gameCode"`
	RoundID               string  `json:"roundId"`
	Timestamp             int64   `json:"timestamp"`
}

type OneBetResultReq struct {
	TraceId               string  `json:"traceId"`
	Username              string  `json:"username"`
	TransactionId         string  `json:"transactionId"`
	BetId                 string  `json:"betId"`
	ExternalTransactionId string  `json:"externalTransactionId"`
	RoundID               string  `json:"roundId"`
	BetAmount             float64 `json:"betAmount"`
	WinAmount             float64 `json:"winAmount"`
	EffectiveTurnover     float64 `json:"effectiveTurnover"`
	WinLoss               float64 `json:"winLoss"`
	JackpotAmount         float64 `json:"jackpotAmount"`
	ResultType            string  `json:"resultType"` // WIN BET_WIN BET_LOSE END
	IsFreeSpin            int     `json:"isFreespin"`
	IsEndRound            int     `json:"isEndRound"`
	Currency              string  `json:"currency"`
	Token                 string  `json:"token"`
	GameCode              string  `json:"gameCode"`
	BetTime               int64   `json:"betTime"`
	SettledTime           int64   `json:"settledTime"`
}

type OneRollbackReq struct {
	TraceId               string `json:"traceId"`
	Username              string `json:"username"`
	Currency              string `json:"currency"`
	TransactionId         string `json:"transactionId"`
	BetId                 string `json:"betId"`
	ExternalTransactionId string `json:"externalTransactionId"`
	RoundID               string `json:"roundId"`
	GameCode              string `json:"gameCode"`
	Timestamp             int64  `json:"timestamp"`
}
