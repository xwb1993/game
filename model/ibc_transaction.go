package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
)

// 中心钱包帐变
func walletIBCTransaction(mb myUserHelp.TblMemberBase, transIDs []string, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	// 获取加减操作符
	operate := transactionsOperate(cashType)
	if operate == "" {
		return errors.New(helper.CashTypeErr)
	}

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		return err
	}

	// 余额小于要扣除金额且不为重新结算扣除场景
	if amount.Cmp(balance) > 0 && operate == OperateSub &&
		cashType != helper.TransactionResettleDeduction &&
		cashType != helper.TransactionCancelPayout &&
		cashType != helper.TransactionCancelledBetRollback {
		fmt.Printf("balance error, walletTransaction amount = %s, balance = %s, cash_type : %d\n", amount.String(), balance.String(), cashType)
		return errors.New(helper.BalanceErr)
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if operate == OperateSub {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if len(transIDs) > 0 {
		ex := g.Ex{
			"id": transIDs,
		}
		if cashType == helper.TransactionPayout ||
			cashType == helper.TransactionResettlePlus ||
			cashType == helper.TransactionResettleDeduction ||
			cashType == helper.TransactionBetCancel ||
			cashType == helper.TransactionSettledBetCancel ||
			cashType == helper.TransactionCancelPayout ||
			cashType == helper.TransactionCancelledBetRollback {
			operationNo += ":"
			operationNo += id
		}
		record := g.Record{}
		switch cashType {
		case helper.TransactionPayout, helper.TransactionResettlePlus, helper.TransactionResettleDeduction: //结算
			record = g.Record{
				"remark": "settled",
			}
		case helper.TransactionBetCancel, helper.TransactionSettledBetCancel: //取消投注 取消已结算投注
			record = g.Record{
				"remark": "void",
			}
		case helper.TransactionCancelPayout, helper.TransactionCancelledBetRollback:
			record = g.Record{
				"remark": "running",
			}
		}

		if len(record) > 0 {
			query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
			myredis.UpdateSqlFieldToRedis(query)
		}
	}

	// cq9结算为0的帐变也要存储
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	if amount.Cmp(decimal.Zero) > 0 {
		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else if operate == "-" {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return nil
}
