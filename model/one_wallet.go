package model

import (
	myUserHelp "common/userHelp"
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"strings"
)

const ONE string = "ONE"

var (
	onePlatform = map[string]string{
		helper.OnePG: "ONEPG",
	}
)

type plat_one_t struct{}

func (plat_one_t) Reg(args map[string]string) error {
	return nil
}

func (plat_one_t) Launch(args map[string]string) (string, error) {

	pid := args["pid"]
	gameCode := args["gamecode"]
	req := map[string]string{
		"traceId":   UUID(),
		"username":  meta.Prefix + args["puid"],
		"gameCode":  gameCode,
		"language":  meta.PlatCfg.One.Lang,
		"platform":  "H5",
		"currency":  meta.PlatCfg.One.Currency,
		"lobbyUrl":  meta.H5URL,
		"ipAddress": args["ip"],
	}
	if args["deviceType"] == "1" {
		req["platform"] = "web"
		req["lobbyUrl"] = meta.WebURL
	}
	requestBody, _ := helper.JsonMarshal(req)
	headers := map[string]string{
		"Content-Type": "application/json",
		"X-API-Key":    meta.PlatCfg.One.APIKey,
		"X-Signature":  HmacSha256(string(requestBody), meta.PlatCfg.One.APISecret),
	}
	requestURI := fmt.Sprintf("%s/game/url", meta.PlatCfg.One.API)
	statusCode, body, err := HttpPostHeaderWithPushLog(requestBody, args["username"], onePlatform[pid], requestURI, headers)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformLoginErr)
	}

	rsp := OneLoginRsp{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		fmt.Println("one:Launch:format", string(body), err)
		return "", errors.New(helper.FormatErr)
	}

	if rsp.Status != "SC_OK" {
		return "", errors.New(helper.PlatformLoginErr)
	}

	return rsp.Data.GameUrl, nil
}

func oneCheckSignature(body, signature string) bool {
	return HmacSha256(body, meta.PlatCfg.One.APISecret) == signature
}

func OneBalance(ctx *fasthttp.RequestCtx) interface{} {

	body := ctx.PostBody()
	signature := string(ctx.Request.Header.Peek("X-Signature"))
	fmt.Println("signature : ", signature)
	fmt.Println("requestBody : ", string(body))
	req := OneBalanceReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		return OneFailedRsp{
			TraceId: UUID(),
			Status:  "SC_WRONG_PARAMETERS",
		}
	}

	if !oneCheckSignature(string(body), signature) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_SIGNATURE",
		}
	}

	if req.Currency != meta.PlatCfg.One.Currency {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_WRONG_CURRENCY",
		}
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.Username)
	if !ret {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_USER_NOT_EXISTS",
		}
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	data := OneBalanceData{
		Username: req.Username,
		Currency: req.Currency,
	}
	data.Balance, _ = balance.Float64()
	return OneRsp{
		TraceId: req.TraceId,
		Status:  "SC_OK",
		Data:    data,
	}
}

func OneBet(ctx *fasthttp.RequestCtx) interface{} {

	body := ctx.PostBody()
	signature := string(ctx.Request.Header.Peek("X-Signature"))
	fmt.Println("signature : ", signature)
	fmt.Println("requestBody : ", string(body))
	req := OneBetReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		return OneFailedRsp{
			TraceId: UUID(),
			Status:  "SC_WRONG_PARAMETERS",
		}
	}

	if !oneCheckSignature(string(body), signature) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_SIGNATURE",
		}
	}

	if req.Currency != meta.PlatCfg.One.Currency {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_WRONG_CURRENCY",
		}
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.Username)
	if !ret {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_USER_NOT_EXISTS",
		}
	}

	amount := decimal.NewFromFloat(req.Amount)
	balance, _ := myUserHelp.GetBalance(mb, 2)
	if amount.GreaterThan(balance) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INSUFFICIENT_FUNDS",
		}
	}

	ex := g.Ex{
		"bill_no": req.BetId,
		//"operation_no": req.BetId,
		"cash_type": helper.TransactionBet,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	data := OneBalanceData{
		Username: req.Username,
		Currency: req.Currency,
	}
	if err == nil {
		data.Balance, _ = balance.Float64()
		return OneRsp{
			TraceId: req.TraceId,
			Status:  "SC_OK",
			Data:    data,
		}
	}

	var (
		pid = helper.OnePG
	)
	if strings.HasPrefix(req.GameCode, "PP") {
		pid = helper.OnePP
	} else if strings.HasPrefix(req.GameCode, "JL") {
		pid = helper.OneJILI
	}
	//投注
	walletBetTransaction(mb, req.BetId, req.TransactionId, pid, balance, amount.Abs(), ctx.Time())
	balance, _ = myUserHelp.GetBalance(mb, 2)
	data.Balance, _ = balance.Float64()
	return OneRsp{
		TraceId: req.TraceId,
		Status:  "SC_OK",
		Data:    data,
	}
}

func OneBetResult(ctx *fasthttp.RequestCtx) interface{} {

	body := ctx.PostBody()
	signature := string(ctx.Request.Header.Peek("X-Signature"))
	fmt.Println("signature : ", signature)
	fmt.Println("requestBody : ", string(body))
	req := OneBetResultReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		return OneFailedRsp{
			TraceId: UUID(),
			Status:  "SC_WRONG_PARAMETERS",
		}
	}

	if !oneCheckSignature(string(body), signature) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_SIGNATURE",
		}
	}

	if req.Currency != meta.PlatCfg.One.Currency {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_WRONG_CURRENCY",
		}
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.Username)
	if !ret {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_USER_NOT_EXISTS",
		}
	}

	betAmount := decimal.NewFromFloat(req.BetAmount)
	winAmount := decimal.NewFromFloat(req.WinAmount)
	jackpotAmount := decimal.NewFromFloat(req.JackpotAmount)
	if jackpotAmount.GreaterThan(decimal.Zero) {
		winAmount = winAmount.Add(jackpotAmount)
	}
	balance, _ := myUserHelp.GetBalance(mb, 2)
	if betAmount.GreaterThan(balance) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INSUFFICIENT_FUNDS",
		}
	}

	ex := g.Ex{
		"bill_no": req.BetId,
		//"operation_no": req.BetId,
		"cash_type": helper.TransactionBet,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	data := OneBalanceData{
		Username: req.Username,
		Currency: req.Currency,
	}
	// 处理PG投付重复请求
	if err == nil && (req.ResultType == "BET_WIN" || req.ResultType == "BET_LOSE" || (req.ResultType == "END" && trx.Remark == "settled")) {
		data.Balance, _ = balance.Float64()
		return OneRsp{
			TraceId: req.TraceId,
			Status:  "SC_OK",
			Data:    data,
		}
	}

	// 结算注单不存在
	if err == sql.ErrNoRows && req.ResultType == "WIN" {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	var (
		pid = helper.OnePG
	)
	if strings.HasPrefix(req.GameCode, "PP") {
		pid = helper.OnePP
	} else if strings.HasPrefix(req.GameCode, "JL") {
		pid = helper.OneJILI
	}
	switch req.ResultType {
	case "BET_WIN", "BET_LOSE":
		//投付
		_, _, trans, _ := walletBetNSettleTransaction(mb, req.BetId, req.TransactionId, pid, balance, betAmount.Abs(), winAmount.Abs(), ctx.Time())
		SetTransactionToRedis(ex, trans)
	case "WIN", "END":
		if req.ResultType == "END" {
			winAmount = decimal.Zero
		}
		walletSettleTransaction(mb, req.BetId, req.TransactionId, pid, balance, winAmount.Abs(), ctx.Time())
	}

	if req.ResultType != "END" {
		balance, _ = myUserHelp.GetBalance(mb, 2)
	}
	data.Balance, _ = balance.Float64()
	return OneRsp{
		TraceId: req.TraceId,
		Status:  "SC_OK",
		Data:    data,
	}
}

func OneRollback(ctx *fasthttp.RequestCtx) interface{} {

	body := ctx.PostBody()
	signature := string(ctx.Request.Header.Peek("X-Signature"))
	fmt.Println("signature : ", signature)
	fmt.Println("requestBody : ", string(body))
	req := OneRollbackReq{}
	err := helper.JsonUnmarshal(body, &req)
	if err != nil {
		return OneFailedRsp{
			TraceId: UUID(),
			Status:  "SC_WRONG_PARAMETERS",
		}
	}

	if !oneCheckSignature(string(body), signature) {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_SIGNATURE",
		}
	}

	if req.Currency != meta.PlatCfg.One.Currency {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_WRONG_CURRENCY",
		}
	}

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(req.Username)
	if !ret {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_USER_NOT_EXISTS",
		}
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	data := OneBalanceData{
		Username: req.Username,
		Currency: req.Currency,
	}
	ex := g.Ex{
		"bill_no":   req.BetId,
		"cash_type": helper.TransactionBet,
	}
	// 投注和结算注单
	trxs, err := walletTransactions(ex)
	if err != nil && err != sql.ErrNoRows {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	var (
		betAmount    = decimal.Zero
		settleAmount = decimal.Zero
		pid          = helper.OnePG
	)
	for _, v := range trxs {
		if v.CashType == helper.TransactionBet {
			betAmount, _ = decimal.NewFromString(v.Amount)
		} else if v.CashType == helper.TransactionPayout {
			settleAmount, _ = decimal.NewFromString(v.Amount)
		}
	}
	// 会员当前订单输赢金额
	winAmount := settleAmount.Sub(betAmount)
	if strings.HasPrefix(req.GameCode, "PP") {
		pid = helper.OnePP
	} else if strings.HasPrefix(req.GameCode, "JL") {
		pid = helper.OneJILI
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	ts := ctx.Time()
	if zero.GreaterThanOrEqual(winAmount) {
		walletCancelTransaction(mb, req.BetId, req.TransactionId, pid, balance, winAmount.Abs(), ts)
	} else {
		walletCancelSettledWinTransaction(mb, req.BetId, req.TransactionId, pid, balance, winAmount.Abs(), ts)
	}
	if err != nil {
		if err.Error() == helper.BalanceErr {
			return OneFailedRsp{
				TraceId: req.TraceId,
				Status:  "SC_INSUFFICIENT_FUNDS",
			}
		}

		if strings.HasPrefix(err.Error(), "Error 1062") {
			data.Balance, _ = balance.Float64()
			return OneRsp{
				TraceId: req.TraceId,
				Status:  "SC_OK",
				Data:    data,
			}
		}

		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	err = tx.Commit()
	if err != nil {
		return OneFailedRsp{
			TraceId: req.TraceId,
			Status:  "SC_INVALID_REQUEST",
		}
	}

	balance, _ = myUserHelp.GetBalance(mb, 2)
	data.Balance, _ = balance.Float64()
	return OneRsp{
		TraceId: req.TraceId,
		Status:  "SC_OK",
		Data:    data,
	}
}
