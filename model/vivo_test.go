package model

import (
	"fmt"
	"game/contrib/conn"
	"game/contrib/helper"
	"github.com/xuri/excelize/v2"
	"testing"
	"time"
)

func TestGenVIVOTableList(t *testing.T) {

	db := conn.InitDB("root:SVAdwsbB@@agw2WG@tcp(23.234.28.245:13309)/lb8888?charset=utf8&parseTime=True&loc=Local", 10, 10)

	f, err := excelize.OpenFile("/Users/brandon/Documents/BR/game_list/vivo.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("TableList")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, row := range rows {

		fmt.Println(row)
		if len(row) != 2 || row[0] == "" {
			continue
		}

		data := Game{
			ID:         helper.GenId(),
			PlatformID: helper.VIVO,                                   //场馆ID
			Name:       row[0],                                        //游戏名称
			EnName:     row[0],                                        //英文名称
			BrAlias:    row[0],                                        //巴西别名
			ClientType: "",                                            //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
			GameType:   1,                                             //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
			GameID:     row[1],                                        //游戏ID
			Img:        fmt.Sprintf("/images-br/VIVO/%s.png", row[1]), //手机图片
			Online:     1,                                             //0 下线 1上线
			IsHot:      1,                                             //0 正常 1热门
			IsNew:      1,                                             //是否最新:0=否,1=是
			Sorting:    1,                                             //排序
			TagID:      "[]",
			CreatedAt:  time.Now().Unix(), //添加时间
		}
		query, _, _ := dialect.Insert("tbl_game_lists").Rows(&data).ToSQL()
		_, err = db.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("%s,[%s]", err.Error(), query))
		}
	}
}
