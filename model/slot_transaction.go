package model

import (
	myConfig "common/config"
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"strconv"
	"time"
)

// 中心钱包帐变
func walletBetNSettleTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount, amount decimal.Decimal, ts time.Time) (winMoney, validAmount float64, trans memberTransaction, err error) {
	betAfterAmount := balance.Sub(betAmount)
	//if betAmount.Cmp(decimal.Zero) > 0 {
	trans = memberTransaction{
		ID:           helper.GenId(),
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBet,
		Amount:       betAmount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "settled",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	//}
	var value float64
	var discount, settleAmount, validBetAmount decimal.Decimal
	if platformID == helper.PG {
		if amount.Sub(betAmount).Cmp(decimal.Zero) > 0 {
			discount := myConfig.GetGameCfgDecimal(myConfig.CfgWithdrawFEE)
			fmt.Println("discount===", discount)
			//扣税金额
			settleAmount = amount.Sub(betAmount).Mul(discount)
		}
	}

	//扣完税后赢的钱
	profitAmount := amount.Sub(settleAmount)
	winMoney, _ = profitAmount.Float64()
	//扣完税和本金后赢的钱
	winAmount := profitAmount.Sub(betAmount)
	fmt.Println("settleAmount===", settleAmount)
	fmt.Println("profitAmount===", profitAmount)
	fmt.Println("winAmount===", winAmount)

	cmp := winAmount.Cmp(decimal.Zero)
	if cmp != 0 {
		if cmp == 1 {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount)
		} else {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount)
		}
	}

	if profitAmount.Cmp(decimal.Zero) > 0 {
		settleAfterAmount := betAfterAmount.Add(profitAmount)
		trans := memberTransaction{
			ID:           helper.GenId(),
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       profitAmount.String(),
			BeforeAmount: betAfterAmount.String(),
			AfterAmount:  settleAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.Add(1 * time.Millisecond).UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	if cmp == 0 {
		validAmount = 0
		return winMoney, validAmount, trans, nil
	}
	if betAmount.Cmp(decimal.Zero) > 0 {
		if winAmount.Cmp(decimal.Zero) > 0 {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.WinFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount)
			} else {
				discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalWinFlowMultiple)
			}
		} else {
			if mb.WinFlowMultiple != "0" {
				value, _ = strconv.ParseFloat(mb.LoseFlowMultiple, 64)
				discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
				validBetAmount = winAmount.Mul(discount).Abs()
			} else {
				discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalLoseFlowMultiple)
				validBetAmount = winAmount.Mul(discount).Abs()
			}
		}
	}

	validAmount, _ = validBetAmount.Float64()
	if validAmount > 0 {
		err = FlowBonusTransaction(mb, validAmount, platformID)
		if err != nil {
			return winMoney, validAmount, trans, err
		}
	}

	return winMoney, validAmount, trans, nil
}

// TransactionAdjustPlus
// TransactionAdjustDiv
// 中心钱包帐变
func walletBetNSettleAdjustTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		return err
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionAdjustDiv {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)

		operate := "+"
		if cashType == helper.TransactionAdjustDiv {
			operate = "-"
		}

		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return nil
}

// TransactionBetCancel
// TransactionSettledBetCancel
// 中心钱包帐变
func walletBetNSettleCancelTransaction(mb myUserHelp.TblMemberBase, transIDs []string, billNo, operationNo, platformID, remark string, amount decimal.Decimal, cashType int, createAt int64) error {

	balance, err := myUserHelp.GetBalance(mb, 2)
	if err != nil {
		return err
	}

	// 结算为输的注单，只修改状态，不产生帐变，场馆业务代码自行控制
	afterAmount := balance.Add(amount)
	if cashType == helper.TransactionSettledBetCancel {
		afterAmount = balance.Sub(amount)
	}

	id := helper.GenId()
	if len(transIDs) > 0 {
		ex := g.Ex{
			"id": transIDs,
		}
		operationNo += ":"
		operationNo += id
		record := g.Record{
			"remark": "void",
		}
		query, _, _ := dialect.Update("tbl_balance_transaction").Set(record).Where(ex).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	// cq9结算为0的帐变也要存储
	if amount.Cmp(decimal.Zero) > 0 {
		trans := memberTransaction{
			ID:           id,
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     cashType,
			Amount:       amount.String(),
			BeforeAmount: balance.String(),
			AfterAmount:  afterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    createAt,
			Remark:       remark,
			Tester:       mb.Tester,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	if amount.Cmp(decimal.Zero) > 0 {
		operate := "+"
		if cashType == helper.TransactionSettledBetCancel {
			operate = "-"
		}

		if operate == "+" {
			myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		} else {
			myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", amount)
		}
	}

	return nil
}

// FlowBonusTransaction 给上面三级流水奖金
func FlowBonusTransaction(mb myUserHelp.TblMemberBase, validAmount float64, platformID string) error {

	tbc, err := getBonusConfig("invite")
	if err != nil {
		return err
	}
	var parent, grand, greatGrand myUserHelp.TblMemberBase
	if mb.ParentID != "0" {
		parent, _ = myUserHelp.GetMemberBaseInfoByUid(mb.ParentID)
	}
	if mb.GrandID != "0" {
		grand, _ = myUserHelp.GetMemberBaseInfoByUid(mb.GrandID)
	}
	if mb.GreatGrandID != "0" {
		greatGrand, _ = myUserHelp.GetMemberBaseInfoByUid(mb.GreatGrandID)
	}
	//上级流水奖金
	if mb.ParentID != "0" && parent.CanBonus == 1 {
		flowBonus := tbc.LvlOneRebate * validAmount
		myredis.AddUserFieldValueByFloat64(mb.ParentID, myConfig.G_tbl_member_balance+"brl", flowBonus)
		myredis.AddUserFieldValueByFloat64(mb.ParentID, myConfig.G_tbl_member_balance+"unlock_amount", flowBonus)

		parentBalance, _ := myUserHelp.GetMemberBalanceInfo(mb.ParentID)
		parentBrl := decimal.NewFromFloat(parentBalance.Brl)
		balAfter := parentBrl.Add(decimal.NewFromFloat(flowBonus))
		fmt.Printf("%0.2f", parentBalance.Brl)
		// 上级新增账变记录
		parentTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: fmt.Sprintf("%f", parentBalance.Brl),
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.ParentID,
			Username:     mb.ParentName,
			Remark:       "flow bonus",
			Tester:       parent.Tester,
			PlatformID:   platformID,
		}

		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(parentTrans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	//上上级流水奖金
	if mb.GrandID != "0" && grand.CanBonus == 1 {
		flowBonus := tbc.LvlTwoRebate * validAmount
		myredis.AddUserFieldValueByFloat64(mb.Uid, myConfig.G_tbl_member_balance+"brl", flowBonus)
		myredis.AddUserFieldValueByFloat64(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", flowBonus)
		grandBalance, _ := myUserHelp.GetMemberBalanceInfo(mb.GrandID)
		grandBrl := decimal.NewFromFloat(grandBalance.Brl)
		balAfter := grandBrl.Add(decimal.NewFromFloat(flowBonus))
		// 上级新增账变记录
		grandTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: fmt.Sprintf("%f", grandBalance.Brl),
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.GrandID,
			Username:     mb.GrandName,
			Remark:       "flow bonus",
			Tester:       grand.Tester,
			PlatformID:   platformID,
		}

		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(grandTrans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	//上上上级流水奖金
	if mb.GreatGrandID != "0" && greatGrand.CanBonus == 1 {
		flowBonus := tbc.LvlThreeRebate * validAmount
		myredis.AddUserFieldValueByFloat64(mb.GreatGrandID, myConfig.G_tbl_member_balance+"brl", flowBonus)
		myredis.AddUserFieldValueByFloat64(mb.GreatGrandID, myConfig.G_tbl_member_balance+"unlock_amount", flowBonus)

		greatGrandBalance, _ := myUserHelp.GetMemberBalanceInfo(mb.GreatGrandID)
		greatGrandBrl := decimal.NewFromFloat(greatGrandBalance.Brl)
		balAfter := greatGrandBrl.Add(decimal.NewFromFloat(flowBonus))
		// 上级新增账变记录
		greatGrandTrans := memberTransaction{
			AfterAmount:  balAfter.String(),
			Amount:       fmt.Sprintf("%f", flowBonus),
			BeforeAmount: fmt.Sprintf("%f", greatGrandBalance.Brl),
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionFlowBonus,
			UID:          mb.GreatGrandID,
			Username:     mb.GreatGrandName,
			Remark:       "flow bonus",
			Tester:       greatGrand.Tester,
			PlatformID:   platformID,
		}

		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(greatGrandTrans).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	return nil
}

func walletBetSettleTransaction(mb myUserHelp.TblMemberBase, billNo, operationNo, platformID string, balance, betAmount, amount decimal.Decimal, ts time.Time, memWallet Wallet) (winMoney, validAmount float64, err error) {
	betAfterAmount := balance.Sub(betAmount)
	trans := memberTransaction{
		ID:           helper.GenId(),
		UID:          mb.Uid,
		PlatformID:   platformID,
		Username:     mb.Username,
		BillNo:       billNo,
		CashType:     helper.TransactionBet,
		Amount:       betAmount.String(),
		BeforeAmount: balance.String(),
		AfterAmount:  betAfterAmount.String(),
		OperationNo:  operationNo,
		CreatedAt:    ts.UnixMilli(),
		Remark:       "settled",
		Tester:       mb.Tester,
	}
	query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	myredis.UpdateSqlFieldToRedis(query)
	var value float64
	var discount, settleAmount, validBetAmount decimal.Decimal
	if platformID == helper.PG {
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			return winMoney, validAmount, err
		}
		if amount.Sub(betAmount).Cmp(decimal.Zero) > 0 {

			discount = myConfig.GetGameCfgDecimal(myConfig.CfgPGFEE)
			fmt.Println("discount===", discount)
			//扣税金额
			settleAmount = amount.Sub(betAmount).Mul(discount)
		}
	}

	//扣完税后赢的钱
	profitAmount := amount.Sub(settleAmount)
	//扣完税和本金后赢的钱
	winAmount := profitAmount.Sub(betAmount)
	fmt.Println("settleAmount===", settleAmount)
	fmt.Println("profitAmount===", profitAmount)
	fmt.Println("winAmount===", winAmount)

	cmp := winAmount.Cmp(decimal.Zero)
	if memWallet.IsValid {
		fmt.Println("执行到这里1===", memWallet.UnlockAmount)
		if cmp != 0 {
			operate := "-"
			if cmp == 1 {
				operate = "+"
			}

			if memWallet.DepositLockAmount.Cmp(decimal.Zero) == 0 {
				if operate == "+" {
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"agency_amount", winAmount.Abs())
				} else if operate == "-" {
					myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"agency_amount", winAmount.Abs())
				}
				myredis.SetUserFieldValue(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", memWallet.UnlockAmount.String())
				myredis.SetUserFieldValue(mb.Uid, myConfig.G_tbl_member_balance+"deposit_lock_amount", memWallet.DepositLockAmount.String())

			} else {

				if operate == "+" {
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"deposit_lock_amount", winAmount.Abs())
				} else if operate == "-" {
					myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
					myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"deposit_lock_amount", winAmount.Abs())
				}
				myredis.SetUserFieldValue(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", memWallet.UnlockAmount.String())
			}
		}

		if betAmount.Cmp(decimal.Zero) > 0 {
			if winAmount.Cmp(decimal.Zero) > 0 {
				if mb.WinFlowMultiple != "0" {
					value, _ = strconv.ParseFloat(mb.WinFlowMultiple, 64)
					discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
					validBetAmount = winAmount.Mul(discount)
				} else {
					discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalWinFlowMultiple)
					validBetAmount = winAmount.Mul(discount)
				}
			} else {
				if mb.WinFlowMultiple != "0" {
					value, _ = strconv.ParseFloat(mb.LoseFlowMultiple, 64)
					discount = decimal.NewFromFloat(value).Div(decimal.NewFromInt(100))
					validBetAmount = winAmount.Mul(discount).Abs()
				} else {
					discount = myConfig.GetGameCfgDecimal(myConfig.CfgTotalLoseFlowMultiple)
					validBetAmount = winAmount.Mul(discount).Abs()
				}
			}
		}
		validAmount, _ = validBetAmount.Float64()
		if cmp == 0 {
			validAmount = 0
		}
	} else {
		fmt.Println("执行到这里2===", memWallet.UnlockAmount)
		if cmp != 0 {
			operate := "-"
			if cmp == 1 {
				operate = "+"
			}
			if operate == "+" {
				myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs())
				myredis.AddUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", winAmount.Abs())
			} else if operate == "-" {
				myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"brl", winAmount.Abs().Abs())
				myredis.MinusUserFieldValueByDemical(mb.Uid, myConfig.G_tbl_member_balance+"unlock_amount", winAmount.Abs())
			}
		}
		validAmount = 0
	}

	if profitAmount.Cmp(decimal.Zero) > 0 {
		settleAfterAmount := betAfterAmount.Add(profitAmount)
		transac := memberTransaction{
			ID:           helper.GenId(),
			UID:          mb.Uid,
			PlatformID:   platformID,
			Username:     mb.Username,
			BillNo:       billNo,
			CashType:     helper.TransactionPayout,
			Amount:       profitAmount.String(),
			BeforeAmount: betAfterAmount.String(),
			AfterAmount:  settleAfterAmount.String(),
			OperationNo:  operationNo,
			CreatedAt:    ts.Add(1 * time.Millisecond).UnixMilli(),
			Remark:       "settled",
			Tester:       mb.Tester,
		}
		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(transac).ToSQL()
		myredis.UpdateSqlFieldToRedis(query)
	}

	winMoney, _ = profitAmount.Float64()
	if validAmount > 0 {
		err = FlowBonusTransaction(mb, validAmount, platformID)
		if err != nil {
			return winMoney, validAmount, err
		}
	}

	return winMoney, validAmount, nil
}
