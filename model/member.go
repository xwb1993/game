package model

import (
	"database/sql"
	"errors"
	"fmt"
	"game/contrib/helper"
	ryrpc "game/rpc"
	g "github.com/doug-martin/goqu/v9"
)

func getBonusConfig(code string) (ryrpc.TblBonusConfig, error) {

	var data ryrpc.TblBonusConfig
	query, _, _ := dialect.From("tbl_bonus_config").Select(colsBonusConfig...).Where(g.Ex{"code": code}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return data, pushError(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
