package model

import (
	myredis "common/redis"
	myUserHelp "common/userHelp"
	"errors"
	"fmt"
	"game/contrib/helper"
	g "github.com/doug-martin/goqu/v9"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type plat_jili_t struct{}

func jlKeyG(agentID, agentKey string) string {

	tss := jlTimeString()
	tss = tss + agentID + agentKey
	fmt.Println("jl:keyGString", tss)
	return MD5Hash(tss)
}

func jlPack(queryString, keyG string) string {
	md5String := queryString + keyG
	fmt.Println("jl:md5String", md5String)
	return helper.RandomKey(6) + MD5Hash(md5String) + helper.RandomKey(6)
}

func jlTimeString() string {
	// 默认为UTC - 4
	ts := time.Now().In(usEasternLoc)
	date := fmt.Sprintf("%02d%02d%d", ts.Year()%100, ts.Month(), ts.Day())
	fmt.Println("jl:", ts.Format("2006-01-02 15:04:05"), date)
	return date //ts.Format("2006012")
}

// 场馆注册
func (plat_jili_t) Reg(args map[string]string) error {
	return nil
}

// 游戏启动
func (plat_jili_t) Launch(args map[string]string) (string, error) {
	pid := args["pid"]
	if args["gamecode"] == "" {
		return "", errors.New(helper.PlatformLoginErr)
	}
	dst := "fullwin" + "_" + args["puid"]
	ops := helper.ThinkEncrypt(dst, "jiligame", 0)
	var requestURI string
	if args["tester"] == "1" || args["tester"] == "3" {
		keyG := jlKeyG(meta.PlatCfg.Jl.AgentID, meta.PlatCfg.Jl.AgentKey)
		queryString := fmt.Sprintf("Token=%s&GameId=%s&Lang=%s&AgentId=%s",
			ops, args["gamecode"], meta.PlatCfg.Jl.Lang, meta.PlatCfg.Jl.AgentID)
		key := jlPack(queryString, keyG)
		requestURI = fmt.Sprintf("%s/singleWallet/LoginWithoutRedirect?%s&Key=%s", meta.PlatCfg.Jl.API, queryString, key)
	} else if args["tester"] == "2" {
		keyG := jlKeyG(meta.PlatCfg.JlTest.AgentID, meta.PlatCfg.JlTest.AgentKey)
		queryString := fmt.Sprintf("Token=%s&GameId=%s&Lang=%s&AgentId=%s",
			ops, args["gamecode"], meta.PlatCfg.JlTest.Lang, meta.PlatCfg.JlTest.AgentID)
		key := jlPack(queryString, keyG)
		requestURI = fmt.Sprintf("%s/singleWallet/LoginWithoutRedirect?%s&Key=%s", meta.PlatCfg.JlTest.API, queryString, key)
	}

	statusCode, body, err := HttpGetWithPushLog(args["username"], jlPlatform[pid], requestURI)
	fmt.Println("statusCode===", statusCode)
	fmt.Println("body===", string(body))
	fmt.Println("err===", err)
	if err != nil {
		return "", errors.New(helper.PlatformLoginErr)
	}

	fmt.Println("jili:Launch", requestURI)
	fmt.Println("jili:Launch", string(body))

	if statusCode != fasthttp.StatusOK {
		return "", errors.New(helper.PlatformRegErr)
	}

	rsp := JLRegResult{}
	err = helper.JsonUnmarshal(body, &rsp)
	if err != nil {
		fmt.Println("pg:Reg:format", string(body), err)
		return "", errors.New(helper.FormatErr)
	}

	return rsp.Data, nil
}

func JLAuth(ctx *fasthttp.RequestCtx) JLRsp {
	fmt.Println(string(ctx.PostBody()))
	rsp := JLRsp{
		ErrorCode: 0,
		Message:   "Success",
		Currency:  meta.PlatCfg.Jl.Currency,
	}

	values, err := url.ParseQuery(string(ctx.PostBody()))
	if err != nil {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	req := JLAuthReq{
		ReqId: values.Get("reqId"),
		Token: values.Get("token"),
	}

	if req.Token == "" {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}
	dst := helper.ThinkDecrypt(req.Token, "jiligame")
	dst = strings.Replace(dst, "fullwin_", "", -1)

	mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
	if !ret {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Username = meta.Prefix + mb.Username
	rsp.Balance, _ = balance.Float64()
	return rsp
}

func JLBet(ctx *fasthttp.RequestCtx) JLRsp {

	fmt.Println(string(ctx.PostBody()))
	rsp := JLRsp{
		ErrorCode: 0,
		Message:   "Success",
		Currency:  meta.PlatCfg.Jl.Currency,
	}

	values, err := url.ParseQuery(string(ctx.PostBody()))
	if err != nil {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	req := JLBetReq{}
	req.Token = values.Get("token")
	req.Currency = values.Get("currency")
	req.Game, _ = strconv.Atoi(values.Get("game"))
	req.Round, _ = strconv.ParseInt(values.Get("round"), 10, 64)
	req.BetAmount, _ = strconv.ParseFloat(values.Get("betAmount"), 64)
	req.WinloseAmount, _ = strconv.ParseFloat(values.Get("winloseAmount"), 64)
	req.TransactionId, _ = strconv.ParseInt(values.Get("transactionId"), 10, 64)
	req.GameCategory, _ = strconv.Atoi(values.Get("gameCategory"))
	if req.Currency != meta.PlatCfg.Jl.Currency {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	dst := helper.ThinkDecrypt(req.Token, "jiligame")
	dst = strings.Replace(dst, "fullwin_", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
	if !ret {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Username = meta.Prefix + mb.Username
	rsp.Balance, _ = balance.Float64()
	// 投注金额
	betAmount := decimal.NewFromFloat(req.BetAmount)
	//派彩金额
	winAmount := decimal.NewFromFloat(req.WinloseAmount)
	// 输赢金额加余额是否大于0
	if balance.LessThan(betAmount.Abs()) {
		rsp.ErrorCode = 2
		rsp.Message = "Not enough balance"
		return rsp
	}

	var (
		trxID    string
		pid      = helper.JILI
		apiName  = "JILI Slot"
		gameType = "3"
	)
	// 鱼机游戏大单号
	if req.GameCategory == 5 {
		trxID = fmt.Sprintf("%d", req.TransactionId)
		pid = helper.JILIFISH
		apiName = "JILI Fish"
		gameType = "2"
	}

	// 离线开奖的触发局局号
	if req.IsFreeRound {
		trxID = fmt.Sprintf("%d", req.TransactionId)
	}
	ex := g.Ex{
		"bill_no":   req.Round,
		"cash_type": helper.TransactionBet,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if bTransExsit {
		rsp.ErrorCode = 1
		rsp.Message = "Already accepted"
		return rsp
	}

	round := fmt.Sprintf("%d", req.Round)
	//投注
	winMoney, validAmount, trans, err := walletBetNSettleTransaction(mb, round, trxID, pid, balance, betAmount.Abs(), winAmount.Abs(), ctx.Time())
	SetTransactionToRedis(ex, trans)
	if err != nil {
		if err.Error() == helper.BalanceErr {
			rsp.ErrorCode = 2
			rsp.Message = "Not enough balance"
			return rsp
		}

		if strings.HasPrefix(err.Error(), "Error 1062") {
			rsp.ErrorCode = 1
			rsp.Message = "Already accepted"
			return rsp
		}

		rsp.ErrorCode = 5
		rsp.Message = "Other error"
		return rsp
	}

	rowID := "jili" + round
	gameCode := fmt.Sprintf("%d", req.Game)
	if winAmount.Cmp(betAmount) == 0 || betAmount.Cmp(decimal.Zero) == 0 {
		validAmount = 0
	}
	if betAmount.Cmp(decimal.Zero) > 0 || winAmount.Cmp(decimal.Zero) > 0 {
		gm := GameRecord{
			BillNo:         round,
			Result:         fmt.Sprintf("IsFreeRound:%v", req.IsFreeRound),
			GameName:       jlGameName[gameCode],
			GameCode:       gameCode,
			PlayType:       gameCode,
			RowId:          rowID,
			ApiBillNo:      round,
			MainBillNo:     "",
			HandicapType:   "",
			Handicap:       "",
			Odds:           0.00,
			StartTime:      0,
			Resettle:       0,
			Presettle:      0,
			ParentName:     mb.ParentName,
			ParentUid:      mb.ParentID,
			TopName:        mb.TopName,
			TopUid:         mb.TopID,
			Uid:            mb.Uid,
			GrandName:      mb.GrandName,
			GrandID:        mb.GrandID,
			GreatGrandName: mb.GreatGrandName,
			GreatGrandID:   mb.GreatGrandID,
			Name:           mb.Username,
			Prefix:         meta.Prefix,
			CreatedAt:      time.Now().UnixMilli(),
			UpdatedAt:      time.Now().UnixMilli(),
			PlayerName:     mb.Username,
			GameType:       gameType,
			ApiType:        pid,
			ApiName:        apiName,
			Flag:           1,
			SettleAmount:   winMoney,
			Tester:         "1",
		}
		gm.BetAmount, _ = betAmount.Float64()
		gm.ValidBetAmount = validAmount
		gm.NetAmount, _ = winAmount.Sub(betAmount).Float64()
		gm.BetTime = time.Now().UnixMilli()
		gm.SettleTime = time.Now().UnixMilli()
		query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
		myredis.AddLogToRedis(query)
	}
	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

func JLCancelBet(ctx *fasthttp.RequestCtx) JLRsp {

	fmt.Println(string(ctx.PostBody()))
	rsp := JLRsp{
		ErrorCode: 0,
		Message:   "Success",
		Currency:  meta.PlatCfg.Jl.Currency,
	}

	values, err := url.ParseQuery(string(ctx.PostBody()))
	if err != nil {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	req := JLCancelBetReq{}
	req.Token = values.Get("token")
	req.Currency = values.Get("currency")
	req.Game, _ = strconv.Atoi(values.Get("game"))
	req.Round, _ = strconv.ParseInt(values.Get("round"), 10, 64)
	req.BetAmount, _ = strconv.ParseFloat(values.Get("betAmount"), 64)
	req.WinloseAmount, _ = strconv.ParseFloat(values.Get("winloseAmount"), 64)

	if req.Currency != meta.PlatCfg.Jl.Currency {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	dst := helper.ThinkDecrypt(req.Token, "jiligame")
	dst = strings.Replace(dst, "fullwin_", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
	if !ret {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Username = meta.Prefix + mb.Username
	rsp.Balance, _ = balance.Float64()
	// 投注金额
	betAmount := decimal.NewFromFloat(req.BetAmount)

	ex := g.Ex{
		"bill_no":   req.Round,
		"cash_type": helper.TransactionBet,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.ErrorCode = 2
		rsp.Message = "Round not found"
		return rsp
	}

	round := fmt.Sprintf("%d", req.Round)
	// 取消投注
	walletCancelTransaction(mb, round, "", trx.PlatformID, balance, betAmount.Abs(), ctx.Time())

	rowID := "jili" + round
	gm := g.Record{
		"flag":             "2",
		"valid_bet_amount": "0",
		"net_amount":       "0",
		"updated_at":       time.Now().UnixMilli(),
		"bet_time":         time.Now().UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

func JLSessionBet(ctx *fasthttp.RequestCtx) JLRsp {

	fmt.Println(string(ctx.PostBody()))
	rsp := JLRsp{
		ErrorCode: 0,
		Message:   "Success",
		Currency:  meta.PlatCfg.Jl.Currency,
	}

	values, err := url.ParseQuery(string(ctx.PostBody()))
	if err != nil {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	req := JLSessionBetReq{}
	req.Token = values.Get("token")
	req.Currency = values.Get("currency")
	req.Game, _ = strconv.Atoi(values.Get("game"))
	req.Type, _ = strconv.Atoi(values.Get("type"))
	req.Round, _ = strconv.ParseInt(values.Get("round"), 10, 64)
	req.SessionId, _ = strconv.ParseInt(values.Get("sessionId"), 10, 64)
	req.BetAmount, _ = strconv.ParseFloat(values.Get("betAmount"), 64)
	req.WinloseAmount, _ = strconv.ParseFloat(values.Get("winloseAmount"), 64)

	if req.Currency != meta.PlatCfg.Jl.Currency {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	dst := helper.ThinkDecrypt(req.Token, "jiligame")
	dst = strings.Replace(dst, "fullwin_", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
	if !ret {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Username = meta.Prefix + mb.Username
	rsp.Balance, _ = balance.Float64()

	// 投注金额
	betAmount := decimal.NewFromFloat(req.BetAmount)
	//派彩金额
	settleAmount := decimal.NewFromFloat(req.WinloseAmount)

	// 投注注单
	ex := g.Ex{
		"bill_no":   req.SessionId,
		"uid":       mb.Uid,
		"cash_type": helper.TransactionBet,
	}

	trx, bTransExsit := walletTransactionFindOne(ex)
	if req.Type == 1 {
		if bTransExsit {
			rsp.ErrorCode = 1
			rsp.Message = "Already accepted"
			return rsp
		}

		// 输赢金额加余额是否大于0
		if balance.LessThan(betAmount.Abs()) {
			rsp.ErrorCode = 2
			rsp.Message = "Not enough balance"
			return rsp
		}
	} else {
		if !bTransExsit {
			rsp.ErrorCode = 5
			rsp.Message = "Other error"
			return rsp
		}

		// 派彩
		ex = g.Ex{
			"bill_no":      req.SessionId,
			"operation_no": req.Round,
			"cash_type":    helper.TransactionPayout,
		}
		_, bTransPayoutExsit := walletTransactionFindOne(ex)
		if bTransPayoutExsit {
			rsp.ErrorCode = 1
			rsp.Message = "Already accepted"
			return rsp
		}
	}

	sessionId := fmt.Sprintf("%d", req.SessionId)
	round := fmt.Sprintf("%d", req.Round)
	//投注
	if req.Type == 1 {
		walletBetTransaction(mb, sessionId, round, helper.JILIPoker, balance, betAmount.Abs(), ctx.Time())
	} else { //结算
		walletSettleTransaction(mb, sessionId, round, helper.JILIPoker, balance, settleAmount.Abs(), ctx.Time())
	}

	rowID := fmt.Sprintf("jili%s:%s", sessionId, round)
	if req.Type == 1 {
		gameCode := fmt.Sprintf("%d", req.Game)
		gm := GameRecord{
			BillNo:         round,
			Result:         "",
			GameName:       jlGameName[gameCode],
			GameCode:       gameCode,
			PlayType:       gameCode,
			RowId:          rowID,
			ApiBillNo:      round,
			MainBillNo:     "",
			HandicapType:   "",
			Handicap:       "",
			Odds:           0.00,
			StartTime:      0,
			Resettle:       0,
			Presettle:      0,
			ParentName:     mb.ParentName,
			ParentUid:      mb.ParentID,
			TopName:        mb.TopName,
			TopUid:         mb.TopID,
			Uid:            mb.Uid,
			GrandName:      mb.GrandName,
			GrandID:        mb.GrandID,
			GreatGrandName: mb.GreatGrandName,
			GreatGrandID:   mb.GreatGrandID,
			Name:           mb.Username,
			Prefix:         meta.Prefix,
			CreatedAt:      time.Now().UnixMilli(),
			UpdatedAt:      time.Now().UnixMilli(),
			PlayerName:     mb.Username,
			GameType:       gameTypePoker,
			ApiType:        helper.JILIPoker,
			ApiName:        "JILI Poker",
			Flag:           1,
			Tester:         "1",
		}
		gm.BetAmount, _ = betAmount.Float64()
		gm.BetTime = time.Now().UnixMilli()
		query, _, _ := dialect.Insert("tbl_game_record").Rows(&gm).ToSQL()
		myredis.AddLogToRedis(query)
	} else {
		betAmount, _ = decimal.NewFromString(trx.Amount)
		gm := g.Record{
			"flag":             "1",
			"valid_bet_amount": betAmount.String(),
			"net_amount":       settleAmount.Sub(betAmount).String(),
			"settle_time":      time.Now().UnixMilli(),
			"updated_at":       time.Now().UnixMilli(),
			"bet_time":         time.Now().UnixMilli(),
		}
		query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
		myredis.AddLogToRedis(query)
	}

	balance, _ = myUserHelp.GetBalance(mb, 2)
	rsp.Balance, _ = balance.Float64()
	return rsp
}

func JLCancelSessionBet(ctx *fasthttp.RequestCtx) JLRsp {

	fmt.Println(string(ctx.PostBody()))
	rsp := JLRsp{
		ErrorCode: 0,
		Message:   "Success",
		Currency:  meta.PlatCfg.Jl.Currency,
	}

	values, err := url.ParseQuery(string(ctx.PostBody()))
	if err != nil {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	req := JLCancelSessionBetReq{}
	req.Token = values.Get("token")
	req.Currency = values.Get("currency")
	req.Game, _ = strconv.Atoi(values.Get("game"))
	req.Type, _ = strconv.Atoi(values.Get("type"))
	req.Round, _ = strconv.ParseInt(values.Get("round"), 10, 64)
	req.SessionId, _ = strconv.ParseInt(values.Get("sessionId"), 10, 64)
	req.BetAmount, _ = strconv.ParseFloat(values.Get("betAmount"), 64)
	req.WinloseAmount, _ = strconv.ParseFloat(values.Get("winloseAmount"), 64)

	if req.Currency != meta.PlatCfg.Jl.Currency {
		rsp.ErrorCode = 3
		rsp.Message = "Invalid parameter"
		return rsp
	}

	dst := helper.ThinkDecrypt(req.Token, "jiligame")
	dst = strings.Replace(dst, "fullwin_", "", -1)
	mb, ret := myUserHelp.GetMemberBaseInfoByUid(dst)
	if !ret {
		rsp.ErrorCode = 4
		rsp.Message = "Token expired"
		return rsp
	}

	balance, _ := myUserHelp.GetBalance(mb, 2)
	rsp.Username = meta.Prefix + mb.Username
	rsp.Balance, _ = balance.Float64()

	betAmount := decimal.NewFromFloat(req.BetAmount)
	ex := g.Ex{
		"bill_no":      req.SessionId,
		"operation_no": req.Round,
		"cash_type":    helper.TransactionBet,
	}

	_, bTransExsit := walletTransactionFindOne(ex)
	if !bTransExsit {
		rsp.ErrorCode = 2
		rsp.Message = "Round not found"
		return rsp
	}

	sessionId := fmt.Sprintf("%d", req.SessionId)
	round := fmt.Sprintf("%d", req.Round)

	// 取消投注
	walletCancelTransaction(mb, sessionId, round, helper.JILIPoker, balance, betAmount.Abs(), ctx.Time())
	rowID := fmt.Sprintf("jili%s:%s", sessionId, round)
	gm := g.Record{
		"flag":             "2",
		"valid_bet_amount": "0",
		"net_amount":       "0",
		"updated_at":       time.Now().UnixMilli(),
		"bet_time":         time.Now().UnixMilli(),
	}
	query, _, _ := dialect.Update("tbl_game_record").Set(gm).Where(g.Ex{"row_id": rowID}).ToSQL()
	myredis.AddLogToRedis(query)

	return rsp
}
