package model

import (
	"encoding/xml"
)

type BTIReserveBet struct {
	Text                string `xml:",chardata"`
	BetID               string `xml:"BetID,attr"`
	BetTypeID           string `xml:"BetTypeID,attr"`
	BetTypeName         string `xml:"BetTypeName,attr"`
	Stake               string `xml:"Stake,attr"`
	OrgStake            string `xml:"OrgStake,attr"`
	Gain                string `xml:"Gain,attr"`
	IsLive              string `xml:"IsLive,attr"`
	NumberOfBets        string `xml:"NumberOfBets,attr"`
	Status              string `xml:"Status,attr"`
	IsFreeBet           string `xml:"IsFreeBet,attr"`
	FreebetAmount       string `xml:"FreebetAmount,attr"`
	RealAmount          string `xml:"RealAmount,attr"`
	CreationDate        string `xml:"CreationDate,attr"`
	PurchaseBetID       string `xml:"PurchaseBetID,attr"`
	CommomStatusID      string `xml:"CommomStatusID,attr"`
	Odds                string `xml:"Odds,attr"`
	OddsDec             string `xml:"OddsDec,attr"`
	BonusID             string `xml:"BonusID,attr"`
	ComboBetNumersLines string `xml:"ComboBetNumersLines,attr"`
	ReferenceID         string `xml:"ReferenceID,attr"`
	ReserveAmountType   string `xml:"ReserveAmountType,attr"`
	ReserveAmountTypeID string `xml:"ReserveAmountTypeID,attr"`
	HomeTeam            string `xml:"HomeTeam,attr"`
	AwayTeam            string `xml:"AwayTeam,attr"`
	LineID              string `xml:"LineID,attr"`
	LiveScore1          string `xml:"LiveScore1,attr"`
	LiveScore2          string `xml:"LiveScore2,attr"`
	EventState          string `xml:"EventState,attr"`
	LineTypeID          string `xml:"LineTypeID,attr"`
	LineTypeName        string `xml:"LineTypeName,attr"`
	RowTypeID           string `xml:"RowTypeID,attr"`
	BranchName          string `xml:"BranchName,attr"`
	LeagueID            string `xml:"LeagueID,attr"`
	LeagueName          string `xml:"LeagueName,attr"`
	YourBet             string `xml:"YourBet,attr"`
	EventTypeID         string `xml:"EventTypeID,attr"`
	EventDate           string `xml:"EventDate,attr"`
	MasterEventID       string `xml:"MasterEventID,attr"`
	EventID             string `xml:"EventID,attr"`
	NewMasterEventID    string `xml:"NewMasterEventID,attr"`
	NewEventID          string `xml:"NewEventID,attr"`
	NewLeagueID         string `xml:"NewLeagueID,attr"`
	NewLineID           string `xml:"NewLineID,attr"`
	EventName           string `xml:"EventName,attr"`
	TeamMappingID       string `xml:"TeamMappingID,attr"`
	Score               string `xml:"Score,attr"`
	EventTypeName       string `xml:"EventTypeName,attr"`
	Points              string `xml:"Points,attr"`
	BranchId            string `xml:"BranchId,attr"`
	EachWaySetting      string `xml:"EachWaySetting,attr"`
}

type BTIReserveReq struct {
	XMLName      xml.Name      `xml:"Bets"`
	Text         string        `xml:",chardata"`
	CustID       string        `xml:"cust_id,attr"`
	ReserveID    string        `xml:"reserve_id,attr"`
	Amount       string        `xml:"amount,attr"`
	CurrencyCode string        `xml:"currency_code,attr"`
	Platform     string        `xml:"platform,attr"`
	Bet          BTIReserveBet `xml:"Bet"`
}

type BTIDebitReserveBet struct {
	Text                string `xml:",chardata"`
	BetID               string `xml:"BetID,attr"`
	BetTypeID           string `xml:"BetTypeID,attr"`
	BetTypeName         string `xml:"BetTypeName,attr"`
	Stake               string `xml:"Stake,attr"`
	OrgStake            string `xml:"OrgStake,attr"`
	Gain                string `xml:"Gain,attr"`
	IsLive              string `xml:"IsLive,attr"`
	NumberOfBets        string `xml:"NumberOfBets,attr"`
	Status              string `xml:"Status,attr"`
	IsFreeBet           string `xml:"IsFreeBet,attr"`
	FreebetAmount       string `xml:"FreebetAmount,attr"`
	RealAmount          string `xml:"RealAmount,attr"`
	CreationDate        string `xml:"CreationDate,attr"`
	PurchaseBetID       string `xml:"PurchaseBetID,attr"`
	CommomStatusID      string `xml:"CommomStatusID,attr"`
	Odds                string `xml:"Odds,attr"`
	OddsDec             string `xml:"OddsDec,attr"`
	BonusID             string `xml:"BonusID,attr"`
	ComboBetNumersLines string `xml:"ComboBetNumersLines,attr"`
	OddsInUserStyle     string `xml:"OddsInUserStyle,attr"`
	UserOddStyle        string `xml:"UserOddStyle,attr"`
	Combinations        string `xml:"Combinations,attr"`
	WebProviderID       string `xml:"WebProviderID,attr"`
	SOGEIID             string `xml:"SOGEIID,attr"`
	RealStake           string `xml:"RealStake,attr"`
	ReserveAmountType   string `xml:"ReserveAmountType,attr"`
	ReserveAmountTypeID string `xml:"ReserveAmountTypeID,attr"`
	HomeTeam            string `xml:"HomeTeam,attr"`
	AwayTeam            string `xml:"AwayTeam,attr"`
	LineID              string `xml:"LineID,attr"`
	LiveScore1          string `xml:"LiveScore1,attr"`
	LiveScore2          string `xml:"LiveScore2,attr"`
	EventState          string `xml:"EventState,attr"`
	LineTypeID          string `xml:"LineTypeID,attr"`
	LineTypeName        string `xml:"LineTypeName,attr"`
	RowTypeID           string `xml:"RowTypeID,attr"`
	BranchName          string `xml:"BranchName,attr"`
	LeagueID            string `xml:"LeagueID,attr"`
	LeagueName          string `xml:"LeagueName,attr"`
	YourBet             string `xml:"YourBet,attr"`
	EventTypeID         string `xml:"EventTypeID,attr"`
	EventDate           string `xml:"EventDate,attr"`
	MasterEventID       string `xml:"MasterEventID,attr"`
	EventID             string `xml:"EventID,attr"`
	NewMasterEventID    string `xml:"NewMasterEventID,attr"`
	NewEventID          string `xml:"NewEventID,attr"`
	NewLeagueID         string `xml:"NewLeagueID,attr"`
	NewLineID           string `xml:"NewLineID,attr"`
	EventName           string `xml:"EventName,attr"`
	TeamMappingID       string `xml:"TeamMappingID,attr"`
	Score               string `xml:"Score,attr"`
	EventTypeName       string `xml:"EventTypeName,attr"`
	Points              string `xml:"Points,attr"`
	BranchID            string `xml:"BranchID,attr"`
	SRIJbetCode         string `xml:"SRIJbetCode,attr"`
	Country             string `xml:"Country,attr"`
}

type BTIDebitReserveReq struct {
	XMLName   xml.Name           `xml:"Bets"`
	Text      string             `xml:",chardata"`
	CustID    string             `xml:"cust_id,attr"`
	ReserveID string             `xml:"reserve_id,attr"`
	Amount    string             `xml:"amount,attr"`
	Bet       BTIDebitReserveBet `xml:"Bet"`
}

// 结算相关
type BTICustomerBet struct {
	Text            string `xml:",chardata"`
	ID              string `xml:"ID,attr"`
	BetType         string `xml:"BetType,attr"`
	BetTypeID       string `xml:"BetTypeID,attr"`
	PrevBalance     string `xml:"PrevBalance,attr"`
	NewBalance      string `xml:"NewBalance,attr"`
	Amount          string `xml:"Amount,attr"`
	OldStatus       string `xml:"OldStatus,attr"`
	NewStatus       string `xml:"NewStatus,attr"`
	IsFreeBet       string `xml:"IsFreeBet,attr"`
	Odds            string `xml:"Odds,attr"`
	TaxPercentage   string `xml:"TaxPercentage,attr"`
	TaxAmount       string `xml:"TaxAmount,attr"`
	AmountBeforeTax string `xml:"AmountBeforeTax,attr"`
	IsLive          string `xml:"IsLive,attr"`
	TOTax           string `xml:"TOTax,attr"`
	TaxedStake      string `xml:"TaxedStake,attr"`
	BetSettledDate  string `xml:"BetSettledDate,attr"`
	IsResettlement  string `xml:"IsResettlement,attr"`
}

type BTICustomerBets struct {
	Text string         `xml:",chardata"`
	Bet  BTICustomerBet `xml:"Bet"`
}

type BTICustomerChange struct {
	Text            string          `xml:",chardata"`
	ID              string          `xml:"ID,attr"`
	OldStatus       string          `xml:"OldStatus,attr"`
	NewStatus       string          `xml:"NewStatus,attr"`
	Amount          string          `xml:"Amount,attr"`
	PrevBalance     string          `xml:"PrevBalance,attr"`
	NewBalance      string          `xml:"NewBalance,attr"`
	DateUTC         string          `xml:"DateUTC,attr"`
	TriggeredResult string          `xml:"TriggeredResult,attr"`
	Bets            BTICustomerBets `xml:"Bets"`
}

type BTICustomerChanges struct {
	Text   string            `xml:",chardata"`
	Change BTICustomerChange `xml:"Change"`
}

type BTICustomerSelection struct {
	Text             string             `xml:",chardata"`
	LineID           string             `xml:"LineID,attr"`
	DecimalOdds      string             `xml:"DecimalOdds,attr"`
	UserOddStyle     string             `xml:"UserOddStyle,attr"`
	OddsInUserStyle  string             `xml:"OddsInUserStyle,attr"`
	BranchID         string             `xml:"BranchID,attr"`
	BranchName       string             `xml:"BranchName,attr"`
	LineTypeID       string             `xml:"LineTypeID,attr"`
	LineTypeName     string             `xml:"LineTypeName,attr"`
	LeagueID         string             `xml:"LeagueID,attr"`
	LeagueName       string             `xml:"LeagueName,attr"`
	HomeTeam         string             `xml:"HomeTeam,attr"`
	AwayTeam         string             `xml:"AwayTeam,attr"`
	Score            string             `xml:"Score,attr"`
	YourBet          string             `xml:"YourBet,attr"`
	EventTypeID      string             `xml:"EventTypeID,attr"`
	EventTypeName    string             `xml:"EventTypeName,attr"`
	EventDateUTC     string             `xml:"EventDateUTC,attr"`
	MasterEventID    string             `xml:"MasterEventID,attr"`
	EventID          string             `xml:"EventID,attr"`
	NewMasterEventID string             `xml:"NewMasterEventID,attr"`
	NewEventID       string             `xml:"NewEventID,attr"`
	NewLeagueID      string             `xml:"NewLeagueID,attr"`
	NewLineID        string             `xml:"NewLineID,attr"`
	EncodedID        string             `xml:"EncodedID,attr"`
	EventName        string             `xml:"EventName,attr"`
	EventState       string             `xml:"EventState,attr"`
	BestOddsApplied  string             `xml:"BestOddsApplied,attr"`
	SelectionID      string             `xml:"SelectionID,attr"`
	Index            string             `xml:"Index,attr"`
	IsLive           string             `xml:"IsLive,attr"`
	SRIJbetCode      string             `xml:"SRIJbetCode,attr"`
	Country          string             `xml:"Country,attr"`
	CurrentResult    string             `xml:"CurrentResult,attr"`
	Changes          BTICustomerChanges `xml:"Changes"`
}

type BTICustomerSelections struct {
	Text      string               `xml:",chardata"`
	Selection BTICustomerSelection `xml:"Selection"`
}

type BTICustomerPurchase struct {
	Text                  string                `xml:",chardata"`
	ReserveID             string                `xml:"ReserveID,attr"`
	MerchantReserveID     string                `xml:"MerchantReserveID,attr"`
	PurchaseID            string                `xml:"PurchaseID,attr"`
	Amount                string                `xml:"Amount,attr"`
	CreationDateUTC       string                `xml:"CreationDateUTC,attr"`
	CurrentBalance        string                `xml:"CurrentBalance,attr"`
	SeqNum                string                `xml:"seq_num,attr"`
	CurrentStatus         string                `xml:"CurrentStatus,attr"`
	NumberOfLines         string                `xml:"NumberOfLines,attr"`
	NumberOfOpenLines     string                `xml:"NumberOfOpenLines,attr"`
	NumberOfSettledLines  string                `xml:"NumberOfSettledLines,attr"`
	NumberOfLostLines     string                `xml:"NumberOfLostLines,attr"`
	NumberOfWonLines      string                `xml:"NumberOfWonLines,attr"`
	NumberOfCanceledLines string                `xml:"NumberOfCanceledLines,attr"`
	NumberOfDrawLines     string                `xml:"NumberOfDrawLines,attr"`
	NumberOfCashoutLines  string                `xml:"NumberOfCashoutLines,attr"`
	ExtBonusContribution  string                `xml:"ExtBonusContribution,attr"`
	Selections            BTICustomerSelections `xml:"Selections"`
}

type BTICustomerPurchases struct {
	Text     string              `xml:",chardata"`
	Purchase BTICustomerPurchase `xml:"Purchase"`
}

type BTICustomerReq struct {
	XMLName              xml.Name             `xml:"Credit"`
	Text                 string               `xml:",chardata"`
	CustomerID           string               `xml:"CustomerID,attr"`
	CustomerName         string               `xml:"CustomerName,attr"`
	MerchantCustomerCode string               `xml:"MerchantCustomerCode,attr"`
	Amount               string               `xml:"Amount,attr"`
	DomainID             string               `xml:"DomainID,attr"`
	Purchases            BTICustomerPurchases `xml:"Purchases"`
}
