package controller

import (
	"fmt"
	"game/model"
	"github.com/valyala/fasthttp"
)

type BTIController struct{}

// 会员校验接口
func (that *BTIController) Balance(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("access-control-allow-headers", "*")
	ctx.Response.Header.Set("access-control-allow-methods", "GET,POST,OPTIONS")
	ctx.Response.Header.Set("access-control-allow-origin", "*")
	model.JsonResponse("BTI", ctx, model.BTIBalance(ctx))
}

/*
type BTIValidateTokenRsp struct {
	ErrorCode    int
	ErrorMessage string
	CustId       string
	CustLogin    string
	Balance      string
	City         string
	Country      string
	CurrencyCode string
}
*/

// 会员校验接口
func (that *BTIController) ValidateToken(ctx *fasthttp.RequestCtx) {
	rsp := model.BTIValidateToken(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("cust_id=%s\n", rsp.CustId)
	v += fmt.Sprintf("cust_login=%s\n", rsp.CustLogin)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("city=%s\n", rsp.City)
	v += fmt.Sprintf("country=%s\n", rsp.Country)
	v += fmt.Sprintf("currency_code=%s\n", rsp.CurrencyCode)
	model.BTIResponse("BTI", ctx, v)
}

/*
type BTIReserveRsp struct {
	ErrorCode    int
	ErrorMessage string
	Balance      string
	TrxID        string
}
*/

func (that *BTIController) Reserve(ctx *fasthttp.RequestCtx) {
	rsp := model.BTIReserve(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("trx_id=%s\n", rsp.TrxID)
	model.BTIResponse("BTI", ctx, v)
}

func (that *BTIController) DebitReserve(ctx *fasthttp.RequestCtx) {
	rsp := model.BTIDebitReserve(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("trx_id=%s\n", rsp.TrxID)
	model.BTIResponse("BTI", ctx, v)
}

func (that *BTIController) Commit(ctx *fasthttp.RequestCtx) {
	rsp := model.BTICommitReserve(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("trx_id=%s\n", rsp.TrxID)
	model.BTIResponse("BTI", ctx, v)
}

func (that *BTIController) Cancel(ctx *fasthttp.RequestCtx) {
	rsp := model.BTICancelReserve(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	model.BTIResponse("BTI", ctx, v)
}

func (that *BTIController) Credit(ctx *fasthttp.RequestCtx) {
	rsp := model.BTICreditCustomer(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("trx_id=%s\n", rsp.TrxID)
	model.BTIResponse("BTI", ctx, v)
}

func (that *BTIController) DebitCustomer(ctx *fasthttp.RequestCtx) {
	rsp := model.BTIDebitCustomer(ctx)
	v := fmt.Sprintf("error_code=%d\n", rsp.ErrorCode)
	v += fmt.Sprintf("error_message=%s\n", rsp.ErrorMessage)
	v += fmt.Sprintf("balance=%s\n", rsp.Balance)
	v += fmt.Sprintf("trx_id=%s\n", rsp.TrxID)
	model.BTIResponse("BTI", ctx, v)
}
