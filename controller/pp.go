package controller

import (
	"fmt"
	"game/model"
	"github.com/valyala/fasthttp"
)

type PPController struct{}

func (that *PPController) Authenticate(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPAuthenticate(ctx))
}

func (that *PPController) Balance(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPBalance(ctx))
}

func (that *PPController) Bet(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPBet(ctx))
}

func (that *PPController) Result(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPResult(ctx))
}

func (that *PPController) BonusWin(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPBonusWin(ctx))
}

func (that *PPController) JackpotWin(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPJackpotWin(ctx))
}

func (that *PPController) EndRound(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPEndRound(ctx))
}

func (that *PPController) Refund(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPRefund(ctx))
}

func (that *PPController) PromoWin(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPPromoWin(ctx))
}

func (that *PPController) Adjustment(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PP", ctx, model.PPAdjustment(ctx))
}
