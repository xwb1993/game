package controller

import (
	"game/model"
	"github.com/valyala/fasthttp"
)

type OneController struct{}

func (that *OneController) Balance(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.OneBalance(ctx))
}

func (that *OneController) Bet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.OneBet(ctx))
}

func (that *OneController) BetResult(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.OneBetResult(ctx))
}

func (that *OneController) Rollback(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.OneRollback(ctx))
}
