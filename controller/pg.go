package controller

import (
	"fmt"
	"game/model"
	"github.com/valyala/fasthttp"
)

type PGController struct{}

// VerifySession session校验
func (that *PGController) VerifySession(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PG", ctx, model.PGVerifySession(ctx))
}

// CashGet 获取余额
func (that *PGController) CashGet(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PG", ctx, model.PGCashGet(ctx))
}

// CashTransferInOut 投付
func (that *PGController) CashTransferInOut(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PG", ctx, model.PGCashTransferInOut(ctx))
}

// CashAdjustment 余额调整
func (that *PGController) CashAdjustment(ctx *fasthttp.RequestCtx) {
	fmt.Println(ctx.PostArgs().String())
	model.JsonResponse("PG", ctx, model.PGCashAdjustment(ctx))
}
