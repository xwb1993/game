package controller

import (
	"game/model"
	"github.com/valyala/fasthttp"
)

type JILIController struct{}

func (that *JILIController) Auth(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.JLAuth(ctx))
}

func (that *JILIController) Bet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.JLBet(ctx))
}

func (that *JILIController) CancelBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.JLCancelBet(ctx))
}

func (that *JILIController) SessionBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.JLSessionBet(ctx))
}

func (that *JILIController) CancelSessionBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("JILI", ctx, model.JLCancelSessionBet(ctx))
}
