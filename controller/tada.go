package controller

import (
	"game/model"
	"github.com/valyala/fasthttp"
)

type TADAController struct{}

func (that *TADAController) Auth(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("TADA", ctx, model.TDAuth(ctx))
}

func (that *TADAController) Bet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("TADA", ctx, model.TDBet(ctx))
}

func (that *TADAController) CancelBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("TADA", ctx, model.TDCancelBet(ctx))
}

func (that *TADAController) SessionBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("TADA", ctx, model.TDSessionBet(ctx))
}

func (that *TADAController) CancelSessionBet(ctx *fasthttp.RequestCtx) {
	model.JsonResponse("TADA", ctx, model.TDCancelSessionBet(ctx))
}
