package controller

import (
	"game/model"
	"github.com/valyala/fasthttp"
)

type IBCController struct{}

// Balance 沙巴体育-余额
func (that *IBCController) Balance(ctx *fasthttp.RequestCtx) {
	res := model.IBCBalance(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// Bet 沙巴体育-投注
func (that *IBCController) Bet(ctx *fasthttp.RequestCtx) {
	res := model.IBCBet(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// Settle 沙巴体育-结算
func (that *IBCController) Settle(ctx *fasthttp.RequestCtx) {
	res := model.IBCSettle(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// Confirm 沙巴体育-确认下注
func (that *IBCController) Confirm(ctx *fasthttp.RequestCtx) {
	res := model.IBCConfirm(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// CancelBet 沙巴体育-取消下注
func (that *IBCController) CancelBet(ctx *fasthttp.RequestCtx) {
	res := model.IBCCancelBet(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// Resettle 沙巴体育-二次结算
func (that *IBCController) Resettle(ctx *fasthttp.RequestCtx) {
	res := model.IBCResettle(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// UnSettle 沙巴体育-取消结算
func (that *IBCController) UnSettle(ctx *fasthttp.RequestCtx) {
	res := model.IBCUnsettle(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// BetParlay 沙巴体育-下注明细
func (that *IBCController) BetParlay(ctx *fasthttp.RequestCtx) {
	res := model.IBCBetParlay(ctx)
	model.JsonResponse("SABA", ctx, res)
}

// ConfirmParlay 沙巴体育-确认下注明细
func (that *IBCController) ConfirmParlay(ctx *fasthttp.RequestCtx) {
	res := model.IBCConfirmParlay(ctx)
	if res.Status == "0" {
		success := model.IBCConfirmBetParlaySuccess{
			Status:  res.Status,
			Balance: res.Balance,
		}
		model.JsonResponse("SABA", ctx, success)
	} else {
		failed := model.IBCConfirmBetParlayFailed{
			Status: res.Status,
			Msg:    res.Msg,
		}
		model.JsonResponse("SABA", ctx, failed)
	}
}

// CashOut 沙巴体育-交易被接受后，赛事供货商将会通过此方法传输交易
func (that *IBCController) CashOut(ctx *fasthttp.RequestCtx) {

	res := model.IBCResult{
		Status: "101",
		Msg:    "path error",
	}

	model.JsonResponse("SABA", ctx, res)
}

// BetParlay 沙巴体育-下注明细
func (that *IBCController) AdjustBalance(ctx *fasthttp.RequestCtx) {
	res := model.IBCAdjustBalance(ctx)
	model.JsonResponse("SABA", ctx, res)
}
